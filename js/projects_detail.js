function left_nav() {
    if( (document.getElementById("mySideLeftNav").style.width == '0px')  || (document.getElementById("mySideLeftNav").style.width == '') ){
        document.getElementById("mySideLeftNav").style.width = "250px";
        document.getElementById("tabPane").style.marginLeft = "250px";
    } else {
        document.getElementById("mySideLeftNav").style.width = "0px";            
        document.getElementById("tabPane").style.marginLeft = "0px";
    }

}

function close_left_nav() {
    document.getElementById("mySideLeftNav").style.width = "0";
    document.getElementById("tabPane").style.marginLeft = "0px";
}

// Right nav
var sortid = 10000;
function right_nav(G,row,col, val) {
    var val1 = ''; var  val2 = '';   
    var rid = G.FRow.id; console.log(G.FRow.id);
    
    for(var r=G.GetFirstVisible();r!=G.GetLastVisible();r=G.GetNextVisible(r)) {
        if(r.id == rid) {
            console.log(r.A);
            sortid= r.id;
            val1 = (col=='A') ? val :  ( ( typeof(r.A) == "undefined" ) ? r.A : '' );   
            val2 = (col=='B') ? val :  ( ( typeof(r.B) == "undefined" ) ? r.B : '' );  
        }
    }
    var iframe = document.getElementById("FrameDM");
    if (iframe) {
       var iframeContent = (iframe.contentWindow || iframe.contentDocument);
       iframeContent.set_input_val(val1,val2);
       iframeContent.TreeGrid({Data:{Url:"DM.php?val1="+val1+'&val2='+val2}},"DIV_DMApp");
    }
    
    if( (document.getElementById("mySideRightNav").style.width == '0px')  || (document.getElementById("mySideRightNav").style.width == '') ){
        document.getElementById("mySideRightNav").style.width = "540px";
    }
    
}
function right_nav_hard_button() {
    if( (document.getElementById("mySideRightNav").style.width == '0px')  || (document.getElementById("mySideRightNav").style.width == '') ){
        document.getElementById("mySideRightNav").style.width = "540px";
        document.getElementById("show_dm_out").style.display = "none";        
        document.getElementById("show_dm_in").style.display = "block"; 
    } else {
        document.getElementById("mySideRightNav").style.width = "0px";            
        document.getElementById("show_dm_out").style.display = "block";        
        document.getElementById("show_dm_in").style.display = "none";  
        
    }
}

function close_right_nav() {
    document.getElementById("mySideRightNav").style.width = "0";
}
