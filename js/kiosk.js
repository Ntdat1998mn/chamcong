function right_load_pdf(bill_id) {
    document.getElementById("FrameRight").src =  "kiosk_bill_pdf.php?bill_id="+bill_id;
    if( (document.getElementById("idRightPop").style.width == '0px')  || (document.getElementById("idRightPop").style.width == '') ){
        document.getElementById("idRightPop").style.width = "600px";
    } else {
        document.getElementById("idRightPop").style.width = "600px";            
    }
}


function left_pop(kho_id, type=0) {
    close_popbox();
    if(type==1) { 
        document.getElementById("FrameLeft").src = "kiosk_nhap_du_lieu_ban_hang_truc_tiep.php?kho_id="+kho_id;
    } else {
        document.getElementById("FrameLeft").src = "kiosk_nhap_du_lieu_ban_hang.php?kho_id="+kho_id;
    }
    
    if( (document.getElementById("idLeftPop").style.width == '0px')  || (document.getElementById("idLeftPop").style.width == '') ){
        document.getElementById("idLeftPop").style.width = "900px";
    } else {
        document.getElementById("idLeftPop").style.width = "0px";            
    }
}
function close_left_pop() {
    document.getElementById("idLeftPop").style.width = "0";
}

function dinh_danh(name) {
    if(name=='kiosk_danhsachkho') { 
        document.getElementById("FrameTenKho").src =  "kiosk_danhsachkho.php";
    } else if(name=='kiosk_danhsachhang') {     
        document.getElementById("FrameTenHang").src =  "kiosk_danhsachhang.php";  
    } else if(name=='kiosk_dailycungcap') { 
        document.getElementById("FrameDaiLy").src =  "kiosk_danhsachdaily.php";  
    } else if(name=='kiosk_khoitao_mavach') { 
        document.getElementById("FrameMavach").src =  "kiosk_khoitao_mavach.php";  
    }
}

function right_pop(name) {
    close_popbox();
    if(name=='kiosk_tong_xuathang') {     
        document.getElementById("FrameRight").src =  "kiosk_tong_xuathang.php";        
    } else if(name=='kiosk_tong_nhaphang') {     
        document.getElementById("FrameRight").src =  "kiosk_tong_nhaphang.php";        
    }
    if( (document.getElementById("idRightPop").style.width == '0px')  || (document.getElementById("idRightPop").style.width == '') ){
        document.getElementById("idRightPop").style.width = "600px";
    } else {
        document.getElementById("idRightPop").style.width = "0px";            
    }
}

function close_right_pop() {
    document.getElementById("idRightPop").style.width = "0";
}