
var FormValidation = function () {
    // basic validation
    var handleLogin = function() {
        var form = $('#loginform');
        var error = $('.alert-danger', form);
        var success = $('.alert-success', form);
        form.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'message-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",  // validate all fields including form hidden input
            messages: {
                username: {
                    required: jQuery.validator.format("Vui lòng nhập tên đăng nhập."),
                    minlength: jQuery.validator.format("Tên truy nhập lớn hơn 3 ký tự."),
                    maxlength: jQuery.validator.format("Tên truy nhập quá dài."),
                },
                password: {
                    required:jQuery.validator.format("Vui lòng nhập mật mã."),
                    minlength: jQuery.validator.format("Mật mã quá đơn giản."),
                }                
            },
            rules: {
                username: {
                    required: true,
                    minlength: 3,
                    maxlength: 20,
                },
                password: {
                    required: true,
                    minlength: 3,
                },
            },

            invalidHandler: function (event, validator) { //display error alert on form submit              
                success.hide();
                error.show();
                Metronic.scrollTo(error, -200);
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-validate').find('.icon').addClass('fa-times-circle'); // set error class to the control group
                $(element)
                    .closest('.form-validate').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-validate').removeClass('has-error'); // set error class to the control group
                $(element)
                    .closest('.form-validate').find('.icon').removeClass('fa-times-circle');
            },
            
            success: function (label) {
                //$('#login_msg_empty').hide();
                label
                    .closest('.form-validate').removeClass('has-error'); // set success class to the control group
                label
                    .closest('.form-validate').addClass('has-success'); // set success class to the control group
                label
                    .closest('.form-validate').find('.icon').removeClass('fa-times-circle');
                label
                    .closest('.form-validate').find('.icon').addClass('fa-check-circle');
            },

            submitHandler: function (form) {
                success.show();
                error.hide();
                form.submit();
            }
        });
    }
    
    //var handleCompany = function() {
    function handleCompany()  {
        var form = $('#form_company');
        var error = $('.alert-danger', form);
        var success = $('.alert-success', form);
        form.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'message-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",  // validate all fields including form hidden input
            messages: {
                company_name: {
                    required: jQuery.validator.format("Vui lòng nhập tên công ty."),
                    minlength: jQuery.validator.format("Tên công ty không hợp lệ."),
                    maxlength: jQuery.validator.format("Tên công ty không hợp lệ."),
                },
                company_address: {
                    required:jQuery.validator.format("Vui lòng nhập địa chỉ."),
                    minlength: jQuery.validator.format("Địa chỉ không hợp lệ."),
                    maxlength: jQuery.validator.format("Địa chỉ không hợp lệ."),                    
                }                
            },
            rules: {
                company_name: {
                    required: true,
                    minlength: 2,
                    maxlength: 200,                    
                },
                company_address: {
                    required: true,
                    minlength: 2,
                    maxlength: 2000,                    
                },
            },

            invalidHandler: function (event, validator) { //display error alert on form submit              
                success.hide();
                error.show();
                Metronic.scrollTo(error, -200);
            },

            highlight: function (element) { // hightlight error inputs
                $(element)
                    .closest('.form-validate').find('.icon').addClass('fa-times-circle'); // set error class to the control group
                $(element)
                    .closest('.form-validate').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-validate').removeClass('has-error'); // set error class to the control group
                $(element)
                    .closest('.form-validate').find('.icon').removeClass('fa-times-circle');
            },
            
            success: function (label) {
                label
                    .closest('.form-validate').removeClass('has-error'); // set success class to the control group
                label
                    .closest('.form-validate').addClass('has-success'); // set success class to the control group
                label
                    .closest('.form-validate').find('.icon').removeClass('fa-times-circle');
                label
                    .closest('.form-validate').find('.icon').addClass('fa-check-circle');
            },

            submitHandler: function (form) {
                success.show();
                error.hide();
                //form.submit();
                save_company();
            }
        });
    }
  
    return {
        init: function () {
            handleLogin();
            handleCompany();
        }
    };
}();


 