
(function(){
$.fn.vp_popbox = function(options){
  var settings = $.extend({
    selector      : this.selector,
    open          : '.vp_open',
    box           : '.vp_box',
    arrow         : '.vp_arrow',
    arrow_border  : '.vp_arrow-border',
    close         : '.vp_close'
  }, options);

  var methods = {
    open: function(event){
        event.preventDefault();
        
        var iframe = document.getElementById("ns_dinhnghia_phongban_cn");  var page="ns_dinhnghia_phongban.php?page=CN";
        iframe.src = page;
        
        var pop = $(this);
        var box = $(this).parent().find(settings['box']);

        box.find(settings['arrow']).css({'left': box.width()/2 - 10});
        box.find(settings['arrow_border']).css({'left': box.width()/2 - 10});

        if(box.css('display') == 'block'){
            methods.close();
        } else {
            box.css({'display': 'block', 'top': 10, 'left': ((pop.parent().width()/2) -box.width()/2 )});
        }
    },

    close: function(){
        $(settings['box']).fadeOut("fast");
    }
  };

  $(document).bind('keyup', function(event){
      if(event.keyCode == 27){
          methods.close();
      }
  });

  $(document).bind('click', function(event){
    if(!$(event.target).closest(settings['selector']).length ){
          methods.close();
    }
  });

  return this.each(function(){
    $(this).css({'width': $(settings['box']).width()}); // Width needs to be set otherwise popbox will not move when window resized.
      $(settings['open'], this).bind('click', methods.open);
      $(settings['open'], this).parent().find(settings['close']).bind('click', function(event){
      event.preventDefault();
      methods.close();
    });
  });
}

}).call(this);

$(document).ready(function(){
    $('.vp_popbox').vp_popbox({
        'open'          : '.vp_open',
        'box'           : '.vp_box',
        'arrow'         : '.vp_arrow',
        'arrow-border'  : '.vp_arrow-border',
        'close'         : '.vp_close'
    });
});

/*********/

(function(){
$.fn.cn_popbox = function(options){
  var settings = $.extend({
    selector      : this.selector,
    open          : '.cn_open',
    box           : '.cn_box',
    arrow         : '.cn_arrow',
    arrow_border  : '.cn_arrow-border',
    close         : '.cn_close'
  }, options);

  var methods = {
    open: function(event){
        event.preventDefault();
        
        var iframe = document.getElementById("ns_dinhnghia_phongban_cn");  var page="ns_dinhnghia_phongban.php?page=CN";
        iframe.src = page;
        
        var pop = $(this);
        var box = $(this).parent().find(settings['box']);

        box.find(settings['arrow']).css({'left': box.width()/2 - 10});
        box.find(settings['arrow_border']).css({'left': box.width()/2 - 10});

        if(box.css('display') == 'block'){
            methods.close();
        } else {
            box.css({'display': 'block', 'top': 10, 'left': ((pop.parent().width()/2) -box.width()/2 )});
        }
    },

    close: function(){
        $(settings['box']).fadeOut("fast");
    }
  };

  $(document).bind('keyup', function(event){
      if(event.keyCode == 27){
          methods.close();
      }
  });

  $(document).bind('click', function(event){
    if(!$(event.target).closest(settings['selector']).length ){
          methods.close();
    }
  });

  return this.each(function(){
    $(this).css({'width': $(settings['box']).width()}); // Width needs to be set otherwise popbox will not move when window resized.
      $(settings['open'], this).bind('click', methods.open);
      $(settings['open'], this).parent().find(settings['close']).bind('click', function(event){
      event.preventDefault();
      methods.close();
    });
  });
}

}).call(this);

$(document).ready(function(){
  $('.cn_popbox').cn_popbox({
      'open'          : '.cn_open',
      'box'           : '.cn_box',
      'arrow'         : '.cn_arrow',
      'arrow-border'  : '.cn_arrow-border',
      'close'         : '.cn_close'
  });
});