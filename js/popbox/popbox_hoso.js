(function(){
    $.fn.filelylich_popbox = function(options){
      var settings = $.extend({
        selector      : this.selector,
        open          : '.filelylich_open',
        box           : '.filelylich_box',
        arrow         : '.filelylich_arrow',
        arrow_border  : '.filelylich_arrow-border',
        close         : '.filelylich_close'
      }, options);

      var methods = {
        open: function(event){
            event.preventDefault();

            var pop = $(this);
            var box = $(this).parent().find(settings['box']);

            box.find(settings['arrow']).css({'left': box.width()/2 - 10});
            box.find(settings['arrow_border']).css({'left': box.width()/2 - 10});

            if(box.css('display') == 'block'){
                methods.close();
            } else {
                box.css({'display': 'block', 'top': 10, 'left': ((pop.parent().width()/2) -box.width()/2 )});
            }
        },

        close: function(){
            $(settings['box']).fadeOut("fast");
        }
      };

      $(document).bind('keyup', function(event){
          if(event.keyCode == 27){
              methods.close();
          }
      });

      $(document).bind('click', function(event){
        if(!$(event.target).closest(settings['selector']).length ){
              methods.close();
        }
      });

      return this.each(function(){
        $(this).css({'width': $(settings['box']).width()}); // Width needs to be set otherwise popbox will not move when window resized.
          $(settings['open'], this).bind('click', methods.open);
          $(settings['open'], this).parent().find(settings['close']).bind('click', function(event){
          event.preventDefault();
          methods.close();
        });
      });
    }

}).call(this);

$(document).ready(function(){
    $('.filelylich_popbox').filelylich_popbox({
        'open'          : '.filelylich_open',
        'box'           : '.filelylich_box',
        'arrow'         : '.filelylich_arrow',
        'arrow-border'  : '.filelylich_arrow-border',
        'close'         : '.filelylich_close'
    });
});

function my_load_edit_filelylich(nv_id) {
    var iframe = document.getElementById("ns_edit_hoso_filelylich");  var page="ns_edit_hoso_filelylich.php?nv_id="+nv_id;
    iframe.src = page; 
}


(function(){
    $.fn.filehopdong_popbox = function(options){
      var settings = $.extend({
        selector      : this.selector,
        open          : '.filehopdong_open',
        box           : '.filehopdong_box',
        arrow         : '.filehopdong_arrow',
        arrow_border  : '.filehopdong_arrow-border',
        close         : '.filehopdong_close'
      }, options);

      var methods = {
        open: function(event){
            event.preventDefault();

            var pop = $(this);
            var box = $(this).parent().find(settings['box']);

            box.find(settings['arrow']).css({'left': box.width()/2 - 10});
            box.find(settings['arrow_border']).css({'left': box.width()/2 - 10});

            if(box.css('display') == 'block'){
                methods.close();
            } else {
                box.css({'display': 'block', 'top': 10, 'left': ((pop.parent().width()/2) -box.width()/2 )});
            }
        },

        close: function(){
            $(settings['box']).fadeOut("fast");
        }
      };

      $(document).bind('keyup', function(event){
          if(event.keyCode == 27){
              methods.close();
          }
      });

      $(document).bind('click', function(event){
        if(!$(event.target).closest(settings['selector']).length ){
              methods.close();
        }
      });

      return this.each(function(){
        $(this).css({'width': $(settings['box']).width()}); // Width needs to be set otherwise popbox will not move when window resized.
          $(settings['open'], this).bind('click', methods.open);
          $(settings['open'], this).parent().find(settings['close']).bind('click', function(event){
          event.preventDefault();
          methods.close();
        });
      });
    }

}).call(this);

$(document).ready(function(){
    $('.filehopdong_popbox').filehopdong_popbox({
        'open'          : '.filehopdong_open',
        'box'           : '.filehopdong_box',
        'arrow'         : '.filehopdong_arrow',
        'arrow-border'  : '.filehopdong_arrow-border',
        'close'         : '.filehopdong_close'
    });
});

function my_load_edit_filehopdong(nv_id) {
    var iframe = document.getElementById("ns_edit_hoso_filehopdong");  var page="ns_edit_hoso_filehopdong.php?nv_id="+nv_id;
    iframe.src = page; 
}



(function(){
    $.fn.quanhegiadinh_popbox = function(options){
      var settings = $.extend({
        selector      : this.selector,
        open          : '.quanhegiadinh_open',
        box           : '.quanhegiadinh_box',
        arrow         : '.quanhegiadinh_arrow',
        arrow_border  : '.quanhegiadinh_arrow-border',
        close         : '.quanhegiadinh_close'
      }, options);

      var methods = {
        open: function(event){
            event.preventDefault();

            var pop = $(this);
            var box = $(this).parent().find(settings['box']);

            box.find(settings['arrow']).css({'left': box.width()/2 - 10});
            box.find(settings['arrow_border']).css({'left': box.width()/2 - 10});

            if(box.css('display') == 'block'){
                methods.close();
            } else {
                box.css({'display': 'block', 'top': 10, 'left': ((pop.parent().width()/2) -box.width()/2 )});
            }
        },

        close: function(){
            $(settings['box']).fadeOut("fast");
        }
      };

      $(document).bind('keyup', function(event){
          if(event.keyCode == 27){
              methods.close();
          }
      });

      $(document).bind('click', function(event){
        if(!$(event.target).closest(settings['selector']).length ){
              methods.close();
        }
      });

      return this.each(function(){
        $(this).css({'width': $(settings['box']).width()}); // Width needs to be set otherwise popbox will not move when window resized.
          $(settings['open'], this).bind('click', methods.open);
          $(settings['open'], this).parent().find(settings['close']).bind('click', function(event){
          event.preventDefault();
          methods.close();
        });
      });
    }

}).call(this);

$(document).ready(function(){
    $('.quanhegiadinh_popbox').quanhegiadinh_popbox({
        'open'          : '.quanhegiadinh_open',
        'box'           : '.quanhegiadinh_box',
        'arrow'         : '.quanhegiadinh_arrow',
        'arrow-border'  : '.quanhegiadinh_arrow-border',
        'close'         : '.quanhegiadinh_close'
    });
});

function my_load_edit_quanhegiadinh(nv_id) {
    var iframe = document.getElementById("ns_edit_hoso_quanhegiadinh");  var page="ns_edit_hoso_quanhegiadinh.php?nv_id="+nv_id;
    iframe.src = page; 
}

//////////////////////////

(function(){
    $.fn.hoso_popbox = function(options){
      var settings = $.extend({
        selector      : this.selector,
        open          : '.hoso_open',
        box           : '.hoso_box',
        arrow         : '.hoso_arrow',
        arrow_border  : '.hoso_arrow-border',
        close         : '.hoso_close'
      }, options);

      var methods = {
        open: function(event){
            event.preventDefault();

            var pop = $(this);
            var box = $(this).parent().find(settings['box']);

            box.find(settings['arrow']).css({'left': box.width()/2 - 10});
            box.find(settings['arrow_border']).css({'left': box.width()/2 - 10});

            if(box.css('display') == 'block'){
                methods.close();
            } else {
                box.css({'display': 'block', 'top': 10, 'left': ((pop.parent().width()/2) -box.width()/2 )});
            }
        },

        close: function(){
            $(settings['box']).fadeOut("fast");
        }
      };

      $(document).bind('keyup', function(event){
          if(event.keyCode == 27){
              methods.close();
          }
      });

      $(document).bind('click', function(event){
        if(!$(event.target).closest(settings['selector']).length ){
              methods.close();
        }
      });

      return this.each(function(){
        $(this).css({'width': $(settings['box']).width()}); // Width needs to be set otherwise popbox will not move when window resized.
          $(settings['open'], this).bind('click', methods.open);
          $(settings['open'], this).parent().find(settings['close']).bind('click', function(event){
          event.preventDefault();
          methods.close();
        });
      });
    }

}).call(this);

$(document).ready(function(){
    $('.hoso_popbox').hoso_popbox({
        'open'          : '.hoso_open',
        'box'           : '.hoso_box',
        'arrow'         : '.hoso_arrow',
        'arrow-border'  : '.hoso_arrow-border',
        'close'         : '.hoso_close'
    });
});

function my_load_edit_basic(nv_id) {
    var iframe = document.getElementById("ns_edit_hoso_basic");  var page="ns_edit_hoso_basic.php?nv_id="+nv_id;
    iframe.src = page; 
}


