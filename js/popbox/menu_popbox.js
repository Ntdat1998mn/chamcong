(function(){
  $.fn.bselpopbox = function(options){
    var settings = $.extend({
      selector      : this.selector,
      open          : '.bselopen',
      box           : '.bselbox',
      arrow         : '.bselarrow',
      arrow_border  : '.bselarrow-border',
      close         : '.bselclose'
    }, options);

    var methods = {
      open: function(event){
        event.preventDefault();

        var pop = $(this);
        var box = $(this).parent().find(settings['box']);

        box.find(settings['arrow']).css({'left': box.width()/2 - 10});
        box.find(settings['arrow_border']).css({'left': box.width()/2 - 10});

        if(box.css('display') == 'block'){
            methods.close();
        } else {
            box.css({'display': 'block', 'top': 10, 'left': ((pop.parent().width()/2) -box.width()/2 )});
        }
      },

      close: function(){
        $(settings['box']).fadeOut("fast");
      }
    };

    $(document).bind('keyup', function(event){
        if(event.keyCode == 27){
            methods.close();
        }
    });

    $(document).bind('click', function(event){
      if(!$(event.target).closest(settings['selector']).length ){
            methods.close();
      }
    });

    return this.each(function(){
      $(this).css({'width': $(settings['box']).width()}); // Width needs to be set otherwise popbox will not move when window resized.
        $(settings['open'], this).bind('click', methods.open);
        $(settings['open'], this).parent().find(settings['close']).bind('click', function(event){
        event.preventDefault();
        methods.close();
      });
    });
  }

}).call(this);
 

$(document).ready(function(){
    $('.bselpopbox').bselpopbox({
        'open'          : '.bselopen',
        'box'           : '.bselbox',
        'arrow'         : '.bselarrow',
        'arrow-border'  : '.bselarrow-border',
        'close'         : '.bselclose'
    });
});



(function(){
  $.fn.blaxpopbox = function(options){
    var settings = $.extend({
      selector      : this.selector,
      open          : '.blaxopen',
      box           : '.blaxbox',
      arrow         : '.blaxarrow',
      arrow_border  : '.blaxarrow-border',
      close         : '.blaxclose'
    }, options);

    var methods = {
      open: function(event){
        event.preventDefault();

        var pop = $(this);
        var box = $(this).parent().find(settings['box']);

        box.find(settings['arrow']).css({'left': box.width()/2 - 10});
        box.find(settings['arrow_border']).css({'left': box.width()/2 - 10});

        if(box.css('display') == 'block'){
            methods.close();
        } else {
            box.css({'display': 'block', 'top': 10, 'left': ((pop.parent().width()/2) -box.width()/2 )});
        }
      },

      close: function(){
        $(settings['box']).fadeOut("fast");
      }
    };

    $(document).bind('keyup', function(event){
        if(event.keyCode == 27){
            methods.close();
        }
    });

    $(document).bind('click', function(event){
      if(!$(event.target).closest(settings['selector']).length){
            methods.close();
      }
    });

    return this.each(function(){
      $(this).css({'width': $(settings['box']).width()}); // Width needs to be set otherwise popbox will not move when window resized.
        $(settings['open'], this).bind('click', methods.open);
        $(settings['open'], this).parent().find(settings['close']).bind('click', function(event){
        event.preventDefault();
        methods.close();
      });
    });
  }

}).call(this);
 

$(document).ready(function(){
    $('.blaxpopbox').blaxpopbox({
        'open'          : '.blaxopen',
        'box'           : '.blaxbox',
        'arrow'         : '.blaxarrow',
        'arrow-border'  : '.blaxarrow-border',
        'close'         : '.blaxclose'
    });
});





