function close_popbox() {
    $('.nhap_box').fadeOut("fast");
    $('.xuat_box').fadeOut("fast");   
    $('.daily_box').fadeOut("fast");
    close_right_pop();
    close_left_pop();
}


(function(){

$.fn.mv_popbox = function(options){
    var settings = $.extend({
      selector      : this.selector,
      open          : '.mv_open',
      box           : '.mv_box',
      arrow         : '.mv_arrow',
      arrow_border  : '.mv_arrow-border',
      close         : '.mv_close'
    }, options);

    var methods = {
      open: function(event){
        event.preventDefault();
        close_popbox();
        
        var pop = $(this);
        var box = $(this).parent().find(settings['box']);

        box.find(settings['arrow']).css({'left': box.width()/2 - 10});
        box.find(settings['arrow_border']).css({'left': box.width()/2 - 10});

        if(box.css('display') == 'block'){
          methods.close();
        } else {
          box.css({'display': 'block', 'top': 10, 'left': ((pop.parent().width()/2) -box.width()/2 )});
        }
      },

      close: function(){
        $(settings['box']).fadeOut("fast");
      }
    };

    $(document).bind('keyup', function(event){
      if(event.keyCode == 27){
        methods.close();
      }
    });

    $(document).bind('click', function(event){
      if(!$(event.target).closest(settings['selector']).length){
        methods.close();
      }
    });

    return this.each(function(){
      $(this).css({'width': $(settings['box']).width()}); // Width needs to be set otherwise popbox will not move when window resized.
      $(settings['open'], this).bind('click', methods.open);
      $(settings['open'], this).parent().find(settings['close']).bind('click', function(event){
        event.preventDefault();
        methods.close();
      });
    });
  }

}).call(this);

$(document).ready(function(){
    $('.mv_popbox').nhap_popbox({
        'open'          : '.mv_open',
        'box'           : '.mv_box',
        'arrow'         : '.mv_arrow',
        'arrow-border'  : '.mv_arrow-border',
        'close'         : '.mv_close'
    });
});

(function(){

$.fn.daily_popbox = function(options){
    var settings = $.extend({
      selector      : this.selector,
      open          : '.daily_open',
      box           : '.daily_box',
      arrow         : '.daily_arrow',
      arrow_border  : '.daily_arrow-border',
      close         : '.daily_close'
    }, options);

    var methods = {
      open: function(event){
        event.preventDefault();
        close_popbox();
        
        var pop = $(this);
        var box = $(this).parent().find(settings['box']);

        box.find(settings['arrow']).css({'left': box.width()/2 - 10});
        box.find(settings['arrow_border']).css({'left': box.width()/2 - 10});

        if(box.css('display') == 'block'){
          methods.close();
        } else {
          box.css({'display': 'block', 'top': 10, 'left': ((pop.parent().width()/2) -box.width()/2 )});
        }
      },

      close: function(){
        $(settings['box']).fadeOut("fast");
      }
    };

    $(document).bind('keyup', function(event){
      if(event.keyCode == 27){
        methods.close();
      }
    });

    $(document).bind('click', function(event){
      if(!$(event.target).closest(settings['selector']).length){
        methods.close();
      }
    });

    return this.each(function(){
      $(this).css({'width': $(settings['box']).width()}); // Width needs to be set otherwise popbox will not move when window resized.
      $(settings['open'], this).bind('click', methods.open);
      $(settings['open'], this).parent().find(settings['close']).bind('click', function(event){
        event.preventDefault();
        methods.close();
      });
    });
  }

}).call(this);

$(document).ready(function(){
    $('.daily_popbox').nhap_popbox({
        'open'          : '.daily_open',
        'box'           : '.daily_box',
        'arrow'         : '.daily_arrow',
        'arrow-border'  : '.daily_arrow-border',
        'close'         : '.daily_close'
    });
});


(function(){

    $.fn.nhap_popbox = function(options){
    var settings = $.extend({
      selector      : this.selector,
      open          : '.nhap_open',
      box           : '.nhap_box',
      arrow         : '.nhap_arrow',
      arrow_border  : '.nhap_arrow-border',
      close         : '.nhap_close'
    }, options);

    var methods = {
      open: function(event){
        event.preventDefault();
        close_popbox();
        
        var pop = $(this);
        var box = $(this).parent().find(settings['box']);

        box.find(settings['arrow']).css({'left': box.width()/2 - 10});
        box.find(settings['arrow_border']).css({'left': box.width()/2 - 10});

        if(box.css('display') == 'block'){
          methods.close();
        } else {
          box.css({'display': 'block', 'top': 10, 'left': ((pop.parent().width()/2) -box.width()/2 )});
        }
      },

      close: function(){
        $(settings['box']).fadeOut("fast");
      }
    };

    $(document).bind('keyup', function(event){
      if(event.keyCode == 27){
        methods.close();
      }
    });

    $(document).bind('click', function(event){
      if(!$(event.target).closest(settings['selector']).length){
        methods.close();
      }
    });

    return this.each(function(){
      $(this).css({'width': $(settings['box']).width()}); // Width needs to be set otherwise popbox will not move when window resized.
      $(settings['open'], this).bind('click', methods.open);
      $(settings['open'], this).parent().find(settings['close']).bind('click', function(event){
        event.preventDefault();
        methods.close();
      });
    });
  }

}).call(this);

$(document).ready(function(){
     $('.nhap_popbox').nhap_popbox({
         'open'          : '.nhap_open',
         'box'           : '.nhap_box',
         'arrow'         : '.nhap_arrow',
         'arrow-border'  : '.nhap_arrow-border',
         'close'         : '.nhap_close'
     });
});




(function(){

$.fn.xuat_popbox = function(options){
    var settings = $.extend({
      selector      : this.selector,
      open          : '.xuat_open',
      box           : '.xuat_box',
      arrow         : '.xuat_arrow',
      arrow_border  : '.xuat_arrow-border',
      close         : '.xuat_close'
    }, options);

    var methods = {
      open: function(event){
        event.preventDefault();
        close_popbox();
        
        var pop = $(this);
        var box = $(this).parent().find(settings['box']);

        box.find(settings['arrow']).css({'left': box.width()/2 - 10});
        box.find(settings['arrow_border']).css({'left': box.width()/2 - 10});

        if(box.css('display') == 'block'){
          methods.close();
        } else {
          box.css({'display': 'block', 'top': 10, 'left': ((pop.parent().width()/2) -box.width()/2 )});
        }
      },

      close: function(){
        $(settings['box']).fadeOut("fast");
      }
    };

    $(document).bind('keyup', function(event){
      if(event.keyCode == 27){
        methods.close();
      }
    });

    $(document).bind('click', function(event){
      if(!$(event.target).closest(settings['selector']).length){
        methods.close();
      }
    });

    return this.each(function(){
      $(this).css({'width': $(settings['box']).width()}); // Width needs to be set otherwise popbox will not move when window resized.
      $(settings['open'], this).bind('click', methods.open);
      $(settings['open'], this).parent().find(settings['close']).bind('click', function(event){
        event.preventDefault();
        methods.close();
      });
    });
  }

}).call(this);

$(document).ready(function(){
     $('.xuat_popbox').xuat_popbox({
         'open'          : '.xuat_open',
         'box'           : '.xuat_box',
         'arrow'         : '.xuat_arrow',
         'arrow-border'  : '.xuat_arrow-border',
         'close'         : '.xuat_close'
     });
});