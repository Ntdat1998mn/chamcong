<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="http://w2ui.com/src/w2ui-1.4.3.min.css" />
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script type="text/javascript" src="http://w2ui.com/src/w2ui-1.4.3.min.js"></script>

<div id="grid" style="width: 100%; height: 400px;"></div>
<br>
<button class="btn" onclick="showChanged()">Get Changed</button>

<script type="text/javascript">


$(function () {
        $('#grid').w2grid({
        name           : 'grid',
        columns: [                
            { field: 'recid', caption: 'ID', size: '50px' },
            { field: 'lname', caption: 'Last Name', size: '30%' },
            { field: 'fname', caption: 'First Name', size: '30%' },
            { field: 'email', caption: 'Email', size: '40%' },
            { field: 'sdate', caption: 'Start Date', size: '120px' },
            { field: 'sdate', caption: 'End Date', size: '120px' }
        ],
        records: [
            { recid: 1, fname: 'John', lname: 'doe', email: 'vitali@gmail.com', sdate: '1/3/2012' },
            { recid: 2, fname: 'Stuart', lname: 'Motzart', email: 'jdoe@gmail.com', sdate: '2/4/2012' },
            { recid: 3, fname: 'Jin', lname: 'Franson', email: '--', sdate: '4/23/2012' },
            { recid: 4, fname: 'Susan', lname: 'Ottie', email: 'jdoe@gmail.com', sdate: '5/3/2012' },
            { recid: 5, fname: 'Kelly', lname: 'Silver', email: 'jdoe@gmail.com', sdate: '4/3/2012' },
            { recid: 6, fname: 'Francis', lname: 'Gatos', email: 'vitali@gmail.com', sdate: '2/5/2012' }
        ]
    });
   
});
 w2ui['grid'].contextMenu(4);
function showChanged() {
    console.log(w2ui['grid'].getChanges()); 
    w2alert('Changed records are displayed in the console');
}
</script>


