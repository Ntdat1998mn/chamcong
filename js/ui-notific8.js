var UINotific8 = function () {

    return {
        //main function to initiate the module
        init: function () {
            $('#notific8_show_right').click(function(event) {

                var settings = {
                        theme: 'teal',
                        sticky: false,
                        horizontalEdge: 'top',
                        verticalEdge: 'right',
                        heading: 'Notification Heading',
                        life: 5000,
                    },
                    $button = $(this);
                $.notific8('zindex', 11500);
                $.notific8('Message notification', settings);
                
                $button.attr('disabled', 'disabled');
                
                setTimeout(function() {
                    $button.removeAttr('disabled');
                }, 1000);
            });
            $('#notific8_show_left').click(function(event) {

                var settings = {
                        theme: 'teal',
                        sticky: false,
                        horizontalEdge: 'top',
                        verticalEdge: 'left',
                        heading: 'Notification Heading',
                        life: 5000,
                    },
                    $button = $(this);
                $.notific8('zindex', 11500);
                $.notific8('Message notification', settings);
                
                $button.attr('disabled', 'disabled');
                
                setTimeout(function() {
                    $button.removeAttr('disabled');
                }, 1000);
            });
        }
    };
}();

 jQuery(document).ready(function() {   
            // js tast 1
            Metronic.init(); 
            FormValidation.init();
            // js tast 1
            // js tash 2
            UINotific8.init();
            // end js tash 2
            $('.btn-theme-panel .close').click(function(){
               $('.btn-theme-panel').removeClass('open');
            })
         
            // custom modal
            $('.btn-modal').click(function() {
                $('.modal')
                    .prop('class', 'modal fade') // revert to default
                    .addClass( $(this).data('direction') );
                $('.modal').modal('show');
            });
            // end custom modal
         });