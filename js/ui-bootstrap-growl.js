var UIBootstrapGrowl = function() {

    return {
        //main function to initiate the module
        init: function() {
            $('#bs_growl_show_right').click(function(event) {
                $.bootstrapGrowl(' Đã cập nhật', {
                    ele: 'body', // which element to append to
                    type: 'success', // (null, 'info', 'danger', 'success', 'warning')
                    offset: {
                        from: 'top',// 'top', or 'bottom'
                        amount: 20
                    }, 
                    align: 'right', // ('left', 'right', or 'center')
                    width: 'auto', // (integer, or 'auto')
                    delay: 150000, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!
                    allow_dismiss: true, // If true then will display a cross to close the popup.
                    stackup_spacing: 10 // spacing between consecutively stacked growls.
                });

            });
            $('#bs_growl_show_left').click(function(event) {
                $.bootstrapGrowl(' Đã cập nhật', {
                    ele: 'body', // which element to append to
                    type: 'success', // (null, 'info', 'danger', 'success', 'warning')
                    offset: {
                        from: 'top',// 'top', or 'bottom'
                        amount: 20
                    }, 
                    align: 'left', // ('left', 'right', or 'center')
                    width: 'auto', // (integer, or 'auto')
                    delay: 150000, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!
                    allow_dismiss: true, // If true then will display a cross to close the popup.
                    stackup_spacing: 10 // spacing between consecutively stacked growls.
                });

            });
        }

    };

}();