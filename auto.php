<?php error_reporting (E_ALL ^ E_NOTICE); ?>
<?php
    $config = array();
    require_once( "./includes/config.php" );
    require_once( "./classes/ui.class.php" );
    require_once( "./includes/main_functions.php" );
    date_default_timezone_set("Asia/Bangkok");
    session_name( 'QlyChamCong' );
    if (get_cfg_var( 'session.auto_start' ) > 0) {
            session_write_close();
    }
    session_start();
    // check if session has previously been initialised
    if (!isset( $_SESSION['AppUI'] ) || isset($_GET['logout'])) {
        $_SESSION['AppUI'] = new CAppUI();
    }
    $AppUI =& $_SESSION['AppUI'];

    $AppUI->setConfig( $config );
    $AppUI->checkStyle();

    require_once( $AppUI->getSystemClass( 'object' ) );
    require_once( "./includes/db_connect.php" );
    $m = getParam( $_GET, 'm', 'home' );
    include_once( "./modules/home/home.class.php" );
    
    $fYear  = $_POST['fYear'];
    $fMonth = $_POST['fMonth'];
    $fDay   = $_POST['fDay'];
    
    $khuvuc_id = $_POST['khuvuc_id'];
    $sYearMonthDay = $fYear."-".$fMonth."-".$fDay;
    $check_arr = db_loadList( "SELECT count(*) as  num FROM calendar  WHERE cal_khuvuc='".$khuvuc_id."' AND DATE_FORMAT(cal_date,'%Y-%m-%d') = DATE_FORMAT( '".$sYearMonthDay."', '%Y-%m-%d') " );
    if( $check_arr[0]['num'] > 0 ) { echo  'NO'; die; }
    
    if($khuvuc_id =='SG') {

            // 5h-29c-pl 
            $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
                VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 05:00:00', 5, 0, '05:00', 'SG', 'PĐ', 'PL') ";
            db_exec($sql);
            // 6h-29c -pl
            $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
                VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 06:00:00', 6, 0, '06:00', 'SG', 'PĐ', 'PL')";
            db_exec($sql);
            // 6h30-29c -pl
            $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
                VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 06:30:00', 6, 30, '06:30', 'SG', 'PĐ', 'PL')";
            db_exec($sql);
            // 7h-VIP-DX 
            $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
                VALUES ( 'VIP', '". $fYear."-".$fMonth."-".$fDay." 07:00:00', 7, 0, '07:00', 'SG', 'PĐ', 'DX')";
            db_exec($sql);
            // 7h30-VIP-DX 
            $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
                VALUES ( 'VIP', '". $fYear."-".$fMonth."-".$fDay." 07:30:00', 7, 30, '07:30', 'SG', 'PĐ', 'DX')";
            db_exec($sql);
            // 8h-VIP-PL 
            $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
                VALUES ( '9_CHO', '". $fYear."-".$fMonth."-".$fDay." 08:00:00', 8, 0, '08:00', 'SG', 'PĐ', 'PL')";
            db_exec($sql);
            // 8h30-VIP-DX 
            $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
                VALUES ( 'VIP', '". $fYear."-".$fMonth."-".$fDay." 08:30:00', 8, 30, '08:30', 'SG', 'PĐ', 'DX')";
            db_exec($sql);
            // 9h-VIP-PL 
            $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
                VALUES ( 'VIP', '". $fYear."-".$fMonth."-".$fDay." 09:00:00', 9, 0, '09:00', 'SG', 'PĐ', 'PL')";
            db_exec($sql);
            // 9h20-VIP-DX 
            $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
                VALUES ( 'VIP', '". $fYear."-".$fMonth."-".$fDay." 09:20:00', 9, 30, '09:20', 'SG', 'PĐ', 'DX')";
            db_exec($sql);
            // 09h30-29c-pl 
            $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
                VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 09:30:00', 09, 30, '09:30', 'SG', 'PĐ', 'PL')";
            db_exec($sql);
            // 10h-29c -pl
            $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
                VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 10:00:00', 10, 0, '10:00', 'SG', 'PĐ', 'PL')";
            db_exec($sql);
            // 10h30-VIP DX 
            $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
                VALUES ( 'VIP', '". $fYear."-".$fMonth."-".$fDay." 10:30:00', 10, 30, '10:30', 'SG', 'PĐ', 'DX')";
            db_exec($sql);
            // 11h20-VIP DX 
            $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
                VALUES ( 'VIP', '". $fYear."-".$fMonth."-".$fDay." 11:20:00', 11, 20, '11:20', 'SG', 'PĐ', 'DX')";
            db_exec($sql);
            // 11h30-29c -pl
            $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
                VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 11:30:00', 11, 30, '11:30', 'SG', 'PĐ', 'PL')";
            db_exec($sql);
            // 12h-VIP -dx
            $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
                VALUES ( 'VIP', '". $fYear."-".$fMonth."-".$fDay." 12:00:00', 12, 0, '12:00', 'SG', 'PĐ', 'DX')";
            db_exec($sql);
            // 12h20-VIP -dx
            $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
                VALUES ( 'VIP', '". $fYear."-".$fMonth."-".$fDay." 12:20:00', 12, 20, '12:20', 'SG', 'PĐ', 'DX')";
            db_exec($sql);
            // 13h-VIP-PL 
            $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
                VALUES ( '9_CHO', '". $fYear."-".$fMonth."-".$fDay." 13:00:00', 13, 0, '13:00', 'SG', 'PĐ', 'PL')";
            db_exec($sql);
            // 13h30-VIP-PL 
            $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
                VALUES ( 'VIP', '". $fYear."-".$fMonth."-".$fDay." 13:30:00', 13, 30, '13:30', 'SG', 'PĐ', 'PL')";
            db_exec($sql);
            // 14h-29c-BD 
            $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
                VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 14:00:00', 14, 0, '14:00', 'SG', 'PĐ', 'BĐ')";
            db_exec($sql);
            // 14h30-VIP-DX 
            $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
                VALUES ( 'VIP', '". $fYear."-".$fMonth."-".$fDay." 14:30:00', 14, 30, '14:30', 'SG', 'PĐ', 'DX')";
            db_exec($sql);
            // 15h-VIP-DX 
            $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
                VALUES ( 'VIP', '". $fYear."-".$fMonth."-".$fDay." 15:00:00', 15, 0, '15:00', 'SG', 'PĐ', 'DX')";
            db_exec($sql);
            // 15h30-VIP-DX 
            $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
                VALUES ( 'VIP', '". $fYear."-".$fMonth."-".$fDay." 15:30:00', 15, 30, '15:30', 'SG', 'PĐ', 'DX')";
            db_exec($sql);
            // 15h40-VIP-DX 
            $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
                VALUES ( 'VIP', '". $fYear."-".$fMonth."-".$fDay." 15:40:00', 15, 40, '15:40', 'SG', 'PĐ', 'DX')";
            db_exec($sql);

            // 16h-29c-PL 
            $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
                VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 16:00:00', 16, 0, '16:00', 'SG', 'PĐ', 'PL')";
            db_exec($sql);
            // 16h30-VIP-PL 
            $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
                VALUES ( '9_CHO', '". $fYear."-".$fMonth."-".$fDay." 16:30:00', 16, 30, '16:30', 'SG', 'PĐ', 'PL')";
            db_exec($sql);
            // 16h45-VIP-DX 
            $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
                VALUES ( 'VIP', '". $fYear."-".$fMonth."-".$fDay." 16:45:00', 16, 45, '16:45', 'SG', 'PĐ', 'DX')";
            db_exec($sql);
            // 17h-29c-PL 
            $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
                VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 17:00:00', 17, 0, '17:00', 'SG', 'PĐ', 'PL')";
            db_exec($sql);
            // 17h15-VIP-DX 
            $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
                VALUES ( 'VIP', '". $fYear."-".$fMonth."-".$fDay." 17:15:00', 17, 15, '17:15', 'SG', 'PĐ', 'DX')";
            db_exec($sql);
            // 17h35-VIP-PL 
            $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
                VALUES ( 'VIP', '". $fYear."-".$fMonth."-".$fDay." 17:35:00', 17, 35, '17:35', 'SG', 'PĐ', 'PL')";
            db_exec($sql);
            // 17h45-VIP-DX 
            $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
                VALUES ( 'VIP', '". $fYear."-".$fMonth."-".$fDay." 17:45:00', 17, 45, '17:45', 'SG', 'PĐ', 'DX')";
            db_exec($sql);
            // 18h-VIP-DX 
            $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
                VALUES ( 'VIP', '". $fYear."-".$fMonth."-".$fDay." 18:00:00', 18, 0, '18:00', 'SG', 'PĐ', 'DX')";
            db_exec($sql);
            // 18h30-VIP-DX 
            $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
                VALUES ( 'VIP', '". $fYear."-".$fMonth."-".$fDay." 18:30:00', 18, 30, '18:30', 'SG', 'PĐ', 'DX')";
            db_exec($sql);
            // 19-VIP-DX 
            $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
                VALUES ( 'VIP', '". $fYear."-".$fMonth."-".$fDay." 19:00:00', 19, 0, '19:00', 'SG', 'PĐ', 'DX')";
            db_exec($sql);
    }
    if($khuvuc_id =='PL') {
        // 1h40 U 
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( 'U_CHO', '". $fYear."-".$fMonth."-".$fDay." 01:40:00', 1, 40, '01:40', 'PL', 'PL', 'VT') ";
        db_exec($sql);
        // 2h U 
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( 'U_CHO', '". $fYear."-".$fMonth."-".$fDay." 02:00:00', 2, 0, '02:00', 'PL', 'PL', 'VT') ";
        db_exec($sql);
        
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( 'U_CHO', '". $fYear."-".$fMonth."-".$fDay." 02:00:00', 2, 0, '02:00', 'PL', 'PL', 'MD') ";
        db_exec($sql);     // DKIA -MD   
        
        // 2h30 U 
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( 'U_CHO', '". $fYear."-".$fMonth."-".$fDay." 02:30:00', 2, 30, '02:30', 'PL', 'PL', 'VT') ";
        db_exec($sql);             
        
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '9_CHO', '". $fYear."-".$fMonth."-".$fDay." 02:30:00', 2, 30, '02:30', 'PL', 'PL', 'PD') ";
        db_exec($sql);             
        
        // 3h 29c 
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 03:00:00', 3, 0, '03:00', 'PL', 'PL', 'PD') ";
        db_exec($sql);                     

        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '8_CHO', '". $fYear."-".$fMonth."-".$fDay." 03:00:00', 3, 0, '03:00', 'PL', 'PL', 'PD') ";
        db_exec($sql);                     
        
        // 3h30 3c
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 03:30:00', 3, 30, '03:30', 'PL', 'PL', 'PD') ";
        db_exec($sql);                     

        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '9_CHO', '". $fYear."-".$fMonth."-".$fDay." 03:30:00', 3, 30, '03:30', 'PL', 'PL', 'PD') ";
        db_exec($sql);                     

        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '8_CHO', '". $fYear."-".$fMonth."-".$fDay." 03:30:00', 3, 30, '03:30', 'PL', 'PL', 'PD') ";
        db_exec($sql);                     
        
        // 4 29c 
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 04:00:00', 4, 0, '04:00', 'PL', 'PL', 'PD') ";
        db_exec($sql);                     
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '8_CHO', '". $fYear."-".$fMonth."-".$fDay." 04:00:00', 4, 0, '04:00', 'PL', 'PL', 'PD') ";
        db_exec($sql);                     

        // 4h30
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 04:00:00', 4, 30, '04:30', 'PL', 'PL', 'PD') ";
        db_exec($sql);                     
        
        
        // 5h
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 05:00:00', 5, 0, '05:00', 'PL', 'PL', 'PD') ";
        db_exec($sql);                     
        
        // 5h20
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 05:20:00', 5, 20, '05:20', 'PL', 'PL', 'PD') ";
        db_exec($sql);                     
        // 5h30 VT về 
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 05:20:00', 5, 20, '05:20', 'PL', 'VT', 'PL') ";
        db_exec($sql);                     
        
        // 5h40
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 05:40:00', 5, 40, '05:40', 'PL', 'PL', 'PD') ";
        db_exec($sql);                             
        
        // 6h
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 06:00:00', 6, 0, '06:00', 'PL', 'PL', 'PD') ";
        db_exec($sql);                             
        
        // 6h
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 06:00:00', 6, 0, '06:00', 'PL', 'PL', 'VT') ";
        db_exec($sql);                             
        
        // 6h15
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 06:15:00', 6, 15, '06:15', 'PL', 'PL', 'MT') ";
        db_exec($sql);                     
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 06:20:00', 6, 20, '06:20', 'PL', 'PL', 'PD') ";
        db_exec($sql);                     

        
        // 6h40
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 06:40:00', 6, 40, '06:40', 'PL', 'PL', 'PD') ";
        db_exec($sql);                             

        // 7h
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '47_CHO', '". $fYear."-".$fMonth."-".$fDay." 07:00:00', 7, 0, '07:00', 'PL', 'PL', 'PD') ";
        db_exec($sql);                             
        
        // 7h20
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 07:20:00', 7, 20, '07:20', 'PL', 'PL', 'PD') ";
        db_exec($sql);                     
        
        // 7h40
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 07:20:00', 7, 40, '07:40', 'PL', 'PL', 'PD') ";
        db_exec($sql);                             
        
        // 8h
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '47_CHO', '". $fYear."-".$fMonth."-".$fDay." 08:00:00', 8, 0, '08:00', 'PL', 'PL', 'PD') ";
        db_exec($sql);                             
        
        
        // 8h20
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 08:20:00', 8, 20, '08:20', 'PL', 'PL', 'PD') ";
        db_exec($sql);                     
        
        // 8h40
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 08:40:00', 8, 40, '08:40', 'PL', 'PL', 'PD') ";
        db_exec($sql);                             

        // 9h
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 09:00:00', 9, 0, '09:00', 'PL', 'PL', 'PD') ";
        db_exec($sql);                             
        
        
        // 9h20
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 09:20:00', 9, 20, '09:20', 'PL', 'PL', 'PD') ";
        db_exec($sql);                     
        
        // DKIA MD
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 09:20:00', 9, 20, '09:20', 'PL', 'PL', 'MD') ";
        db_exec($sql);                     
        
        // 9h40
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 09:40:00', 9, 40, '09:40', 'PL', 'PL', 'PD') ";
        db_exec($sql);  
        
        // 10h
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 10:00:00', 10, 0, '10:00', 'PL', 'PL', 'PD') ";
        db_exec($sql);                             
        
        // 10h30
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 10:30:00', 10, 30, '10:30', 'PL', 'PL', 'PD') ";
        db_exec($sql);                     

       // 11h
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( 'U_CHO', '". $fYear."-".$fMonth."-".$fDay." 11:00:00', 11, 0, '11:00', 'PL', 'PL', 'PD') ";
        db_exec($sql);                             
        
        // 11h30
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 11:30:00', 11, 30, '11:30', 'PL', 'PL', 'PD') ";
        db_exec($sql);         
        
       // 11h55
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( 'U_CHO', '". $fYear."-".$fMonth."-".$fDay." 11:55:00', 11, 55, '11:55', 'PL', 'PL', 'PD') ";
        db_exec($sql);                             
        
        // 12h 30
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( 'U_CHO', '". $fYear."-".$fMonth."-".$fDay." 12:30:00', 12, 30, '12:30', 'PL', 'PL', 'PD') ";
        db_exec($sql);                 

        // 12h 30
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '9_CHO', '". $fYear."-".$fMonth."-".$fDay." 12:30:00', 12, 30, '12:30', 'PL', 'PL', 'PD') ";
        db_exec($sql);                 
        
        // 12h 30
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 12:30:00', 12, 30, '12:30', 'PL', 'PL', 'PD') ";
        db_exec($sql);                 
        
        // 12h 40
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 12:40:00', 12, 40, '12:40', 'PL', 'PL', 'PD') ";
        db_exec($sql);                         

        // 12h 45
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( 'U_CHO', '". $fYear."-".$fMonth."-".$fDay." 12:45:00', 12, 45, '12:45', 'PL', 'VT', 'PL') ";
        db_exec($sql);                         

        // 13h 
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '47_CHO', '". $fYear."-".$fMonth."-".$fDay." 13:00:00', 13, 0, '13:00', 'PL', 'PL', 'VT') ";
        db_exec($sql);                 
        
        // 13h 30
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 13:30:00', 13, 30, '13:30', 'PL', 'PL', 'PD') ";
        db_exec($sql);                 

        // 13h 30
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '8_CHO', '". $fYear."-".$fMonth."-".$fDay." 13:30:00', 13, 30, '13:30', 'PL', 'PL', 'PD') ";
        db_exec($sql);                 
        
        // 14h 
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '47_CHO', '". $fYear."-".$fMonth."-".$fDay." 14:00:00', 14, 0, '14:00', 'PL', 'PL', 'PD') ";
        db_exec($sql);                 

        // 14h 
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '8_CHO', '". $fYear."-".$fMonth."-".$fDay." 14:00:00', 14, 0, '14:00', 'PL', 'PL', 'PD') ";
        db_exec($sql);                 
        
        // 14h 30
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 14:30:00', 14, 30, '14:30', 'PL', 'PL', 'PD') ";
        db_exec($sql);                 
        
        // 14h 55
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 14:55:00', 14, 55, '14:55', 'PL', 'PL', 'PD') ";
        db_exec($sql);                         
        
        // 15h 10
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 15:10:00', 15, 10, '15:10', 'PL', 'PL', 'PD') ";
        db_exec($sql);                         
        
        // 15h 30
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 15:30:00', 15, 30, '15:30', 'PL', 'PL', 'PD') ";
        db_exec($sql);                                 
        
        // 16h
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 16:00:00', 16, 0, '16:00', 'PL', 'PL', 'PD') ";
        db_exec($sql);   

        // 16h20
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 16:20:00', 16, 20, '16:20', 'PL', 'PL', 'PD') ";
        db_exec($sql);   
        
        // 16h45
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 16:45:00', 16, 45, '16:45', 'PL', 'PL', 'PD') ";
        db_exec($sql);           

        // 17
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 17:00:00', 17, 0, '17:00', 'PL', 'PL', 'PD') ";
        db_exec($sql);           

        // 17h30
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 17:30:00', 17, 30, '17:30', 'PL', 'PL', 'PD') ";
        db_exec($sql);           
        
        // 18
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 18:00:00', 18, 0, '18:00', 'PL', 'PL', 'PD') ";
        db_exec($sql);           
        
    }
    
    if($khuvuc_id =='ĐX') {
            //3h30; 4h; 5h;6h;7h
        // 3h30
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 03:30:00', 3, 30, '03:30', 'ĐX', 'ĐX', 'PĐ') ";
        db_exec($sql);
        
        // 4h0
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 04:00:00', 4, 0, '04:00', 'ĐX', 'ĐX', 'PĐ') ";
        db_exec($sql);        
        // 5h
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 05:00:00', 5, 0, '05:00', 'ĐX', 'ĐX', 'PĐ') ";
        db_exec($sql);                
        // 6h
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 06:00:00', 6, 0, '06:00', 'ĐX', 'ĐX', 'PĐ') ";
        db_exec($sql);                
         // 7h
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 07:00:00', 7, 0, '07:00', 'ĐX', 'ĐX', 'PĐ') ";
        db_exec($sql);                
         // 7h30
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 07:30:00', 7, 30, '07:30', 'ĐX', 'ĐX', 'PĐ') ";
        db_exec($sql);                        
         // 8h
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 08:00:00', 8, 0, '08:00', 'ĐX', 'ĐX', 'PĐ') ";
        db_exec($sql);                                
         // 8h30
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 08:30:00', 8, 30, '08:30', 'ĐX', 'ĐX', 'PĐ') ";
        db_exec($sql);                                        
        
         // 9h
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 09:00:00', 9, 0, '09:00', 'ĐX', 'ĐX', 'PĐ') ";
        db_exec($sql);                                        
        
         // 10h
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 10:00:00', 10, 0, '10:00', 'ĐX', 'ĐX', 'PĐ') ";
        db_exec($sql);                                        
        
         // 11h
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 11:00:00', 11, 0, '11:00', 'ĐX', 'ĐX', 'PĐ') ";
        db_exec($sql);                                        
        
         // 12h
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 12:00:00', 12, 0, '12:00', 'ĐX', 'ĐX', 'PĐ') ";
        db_exec($sql);                                        

         // 13h
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 13:00:00', 13, 0, '13:00', 'ĐX', 'ĐX', 'PĐ') ";
        db_exec($sql);                                        

        // 13h30
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 13:30:00', 13, 30, '13:30', 'ĐX', 'ĐX', 'PĐ') ";
        db_exec($sql);                                        
        
        // 14h
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 14:00:00', 14, 0, '14:00', 'ĐX', 'ĐX', 'PĐ') ";
        db_exec($sql);                                        

        // 15h
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 15:00:00', 15, 0, '15:00', 'ĐX', 'ĐX', 'PĐ') ";
        db_exec($sql);                                        
        
        // 16h
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 16:00:00', 16, 0, '16:00', 'ĐX', 'ĐX', 'PĐ') ";
        db_exec($sql);                                        
        
        // 17h
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 17:00:00', 17, 0, '17:00', 'ĐX', 'ĐX', 'PĐ') ";
        db_exec($sql);                                        
        
        // 17h30
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 17:30:00', 17, 30, '17:30', 'ĐX', 'ĐX', 'PĐ') ";
        db_exec($sql);                                        
        
        // 2h30 -8 chổ
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '8_CHO', '". $fYear."-".$fMonth."-".$fDay." 02:30:00', 02, 30, '02:30', 'ĐX', 'ĐX', 'PĐ') ";
        db_exec($sql);                                        
        
        // 3h -8 chổ
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '8_CHO', '". $fYear."-".$fMonth."-".$fDay." 03:00:00', 03, 0, '03:00', 'ĐX', 'ĐX', 'PĐ') ";
        db_exec($sql);                                        
        // 3h -8 chổ
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '8_CHO', '". $fYear."-".$fMonth."-".$fDay." 03:00:00', 03, 0, '03:00', 'ĐX', 'ĐX', 'PĐ') ";
        db_exec($sql);                                        

        // 4h -8 chổ
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '8_CHO', '". $fYear."-".$fMonth."-".$fDay." 04:00:00', 04, 0, '04:00', 'ĐX', 'ĐX', 'PĐ') ";
        db_exec($sql);                                        
        // 4h -8 chổ
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '8_CHO', '". $fYear."-".$fMonth."-".$fDay." 04:00:00', 04, 0, '04:00', 'ĐX', 'ĐX', 'PĐ') ";
        db_exec($sql);                                        

        // 4h30 -8 chổ
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '8_CHO', '". $fYear."-".$fMonth."-".$fDay." 04:30:00', 04, 30, '04:30', 'ĐX', 'ĐX', 'PĐ') ";
        db_exec($sql);                                                
        // 4h30 -8 chổ
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '8_CHO', '". $fYear."-".$fMonth."-".$fDay." 04:30:00', 04, 30, '04:30', 'ĐX', 'ĐX', 'PĐ') ";
        db_exec($sql);                                                

        // 7h -8 chổ
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '8_CHO', '". $fYear."-".$fMonth."-".$fDay." 07:00:00', 07, 0, '07:00', 'ĐX', 'ĐX', 'PĐ') ";
        db_exec($sql);                                                        
        // 7h -8 chổ
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '8_CHO', '". $fYear."-".$fMonth."-".$fDay." 07:00:00', 07, 0, '07:00', 'ĐX', 'ĐX', 'PĐ') ";
        db_exec($sql);                                                        

        // 8h -8 chổ
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '8_CHO', '". $fYear."-".$fMonth."-".$fDay." 08:00:00', 08, 0, '08:00', 'ĐX', 'ĐX', 'PĐ') ";
        db_exec($sql);                                                        

        // 11h -8 chổ
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '8_CHO', '". $fYear."-".$fMonth."-".$fDay." 11:00:00', 11, 0, '11:00', 'ĐX', 'ĐX', 'PĐ') ";
        db_exec($sql);                                                        
        
        // 12h -8 chổ
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '8_CHO', '". $fYear."-".$fMonth."-".$fDay." 12:00:00', 12, 0, '12:00', 'ĐX', 'ĐX', 'PĐ') ";
        db_exec($sql);                                                        
        
        // 13h -8 chổ
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '8_CHO', '". $fYear."-".$fMonth."-".$fDay." 13:00:00', 13, 0, '13:00', 'ĐX', 'ĐX', 'PĐ') ";
        db_exec($sql);                                                                
        
        // 14h -8 chổ
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '8_CHO', '". $fYear."-".$fMonth."-".$fDay." 14:00:00', 14, 0, '14:00', 'ĐX', 'ĐX', 'PĐ') ";
        db_exec($sql);                                             
        
        // 15h -8 chổ
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '8_CHO', '". $fYear."-".$fMonth."-".$fDay." 15:00:00', 15, 0, '15:00', 'ĐX', 'ĐX', 'PĐ') ";
        db_exec($sql);                                                                
        
        // 16h -8 chổ
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '8_CHO', '". $fYear."-".$fMonth."-".$fDay." 16:00:00', 16, 0, '16:00', 'ĐX', 'ĐX', 'PĐ') ";
        db_exec($sql);                                                                
        
        //  VT về 
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( 'U_CHO', '". $fYear."-".$fMonth."-".$fDay." 05:30:00', 05, 30, '05:30', 'VT', 'VT', 'ĐX') ";
        db_exec($sql);                                                                
        
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( 'U_CHO', '". $fYear."-".$fMonth."-".$fDay." 12:45:00', 12, 45, '12:45', 'VT', 'VT', 'ĐX') ";
        db_exec($sql);                                                                
        
        // đi VT  
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( 'U_CHO', '". $fYear."-".$fMonth."-".$fDay." 07:00:00', 07, 0, '07:00', 'ĐX', 'ĐX', 'VT') ";
        db_exec($sql);                                                                
        
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( 'U_CHO', '". $fYear."-".$fMonth."-".$fDay." 13:30:00', 13, 30, '13:30', 'ĐX', 'ĐX', 'VT') ";
        db_exec($sql);                                                                

        // dv 29 chổ  
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 02:30:00', 02, 30, '02:30', 'ĐX', 'ĐX', 'PĐ') ";
        db_exec($sql);                                                                
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 03:30:00', 03, 30, '03:30', 'ĐX', 'ĐX', 'PĐ') ";
        db_exec($sql);                                                                
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 04:30:00', 04, 30, '04:30', 'ĐX', 'ĐX', 'PĐ') ";
        db_exec($sql);                                                                
        $sql = " INSERT INTO calendar ( cal_loaixe,  cal_date, cal_hour, cal_minute, cal_name, cal_khuvuc, cal_xuatphat, cal_tuyen)
            VALUES ( '29_CHO', '". $fYear."-".$fMonth."-".$fDay." 05:30:00', 05, 30, '05:30', 'ĐX', 'ĐX', 'PĐ') ";
        db_exec($sql);                                                                
        
    }
?>
