<?php error_reporting (E_ALL ^ E_NOTICE); ?>
<?php
    $config = array();
    require_once( "./includes/config.php" );
    require_once( "./classes/ui.class.php" );
    require_once( "./includes/main_functions.php" );
    date_default_timezone_set("Asia/Bangkok");
    session_name( 'QlyChamCong' );
    if (get_cfg_var( 'session.auto_start' ) > 0) {
            session_write_close();
    }
    session_start();
    // check if session has previously been initialised
    if (!isset( $_SESSION['AppUI'] ) || isset($_GET['logout'])) {
        $_SESSION['AppUI'] = new CAppUI();
    }
    $AppUI =& $_SESSION['AppUI'];

    $AppUI->setConfig( $config );
    $AppUI->checkStyle();

    require_once( $AppUI->getSystemClass( 'object' ) );
    require_once( "./includes/db_connect.php" );
    $m = getParam( $_GET, 'm', 'home' );
    include_once( "./modules/home/home.class.php" );
    
    $khuvuc_id = $_GET['khuvuc_id'] ? $_GET['khuvuc_id'] : ($_POST['khuvuc_id'] ? $_POST['khuvuc_id']  : 'SG');
    if($_POST['action'] == 'add_calendar' ) {
        $khuvuc_id = $_POST['khuvuc_id'];
        
        $session_fYear    = $_SESSION['session_fYear'];
        $session_fMonth  = $_SESSION['session_fMonth'];
        $session_fDay    = $_SESSION['session_fDay'];
        
        $sYearMonthDay =  $session_fYear ."-".$session_fMonth ."-". $session_fDay ;
        
        $obj = new CCalendar();  
            $obj->cal_taixe      = '';
            $obj->cal_phone      = '';
            $obj->cal_date       = $sYearMonthDay. ' 19:00:00';
            $obj->cal_hour       = '19';
            $obj->cal_minute     = '0';
            $obj->cal_khuvuc     = $khuvuc_id;
            $obj->cal_xuatphat   = ($khuvuc_id =='SG') ? 'PĐ' : $khuvuc_id ;
            $obj->cal_tuyen      = ($khuvuc_id =='SG') ? 'ĐX' : 'PĐ' ;
            
            $sql = "SELECT max(cal_name) as num FROM calendar WHERE cal_khuvuc = '".$khuvuc_id."' AND DATE_FORMAT(cal_date,'%Y-%m-%d') =  '".date("Y-m-d")."'";
            $ret = db_loadList($sql);
                
            $obj->cal_name       = str_pad( $ret[0]["num"] +1 ,3, '0' ,STR_PAD_LEFT);
            
            $obj->store();       
        die;
    }  
    
    //echo $khuvuc_id;
?>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="js/w2ui/w2ui-1.4.3.css" />
    <script src="js/w2ui/2.1.1.jquery.min.js"></script>
    <script type="text/javascript" src="js/w2ui/w2ui-1.4.3.js"></script>
    <link href="js/toast/toastr.css" rel="stylesheet" type="text/css" />
    <script src="js/toast/toastr.js"></script>  
    
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/buttons.css">
    <link rel="stylesheet" href="css/layout.css">
    
</head>    
<body  style="margin-top:0px;margin-right:3px; background-color: white;  overflow-y: block; overflow-x: hidden; "   >
    <?php 
    
        $session_fYear   = $_SESSION['session_fYear'];
        $session_fMonth  = $_SESSION['session_fMonth'];
        $session_fDay    = $_SESSION['session_fDay'];
        
        $sYearMonthDay =  $session_fYear ."-".$session_fMonth ."-". $session_fDay ;
            
        $cal_id_arr = db_loadList("SELECT * FROM calendar WHERE cal_khuvuc='".$khuvuc_id."' AND DATE_FORMAT(cal_date,'%Y-%m-%d') = DATE_FORMAT( '".$sYearMonthDay."', '%Y-%m-%d') ORDER BY cal_date DESC");
        $sql = " SELECT sum(cal_detail_seat) as tong_so_ve "
                    . " FROM calendar_detail d JOIN calendar c ON d.cal_id = c.cal_id "
                    . " AND c.cal_khuvuc='".$khuvuc_id."' AND DATE_FORMAT(c.cal_date,'%Y-%m-%d') = DATE_FORMAT( '".$sYearMonthDay."', '%Y-%m-%d') ";
        $tong_so_ve_arr= db_loadList($sql  );
        $tong_so_ve = $tong_so_ve_arr[0]['tong_so_ve'] ? $tong_so_ve_arr[0]['tong_so_ve']  : '0';
    ?>
        Ngày: <span  style="color:red;font-size:20px; font-weight: bold"><?php echo $session_fDay."/".$session_fMonth ?></span>
        - Số tài:  <span id="my_tong_so_tai" style="color:red;font-size:20px; font-weight: bold"><?php echo count($cal_id_arr); ?></span> 
        - Tổng số vé: <span id="my_tong_so_ve" style="color:red;font-size:20px; font-weight: bold"><?php echo $tong_so_ve; ?></span> 
    <?php    
        foreach ($cal_id_arr as $row) : 
            $gio = 1*($row["cal_hour"]);
            echo '<a id="hour_'.$gio.'"></a>';
            echo '<iframe  id="calendar_'.$row["cal_id"].'" src="calendar_detail.php?cal_id='.$row["cal_id"].'"   style="margin-top: 0px; width: 100%; height: 350px; border:0; position: relative; z-index: 100;"></iframe>';
        endforeach;
    ?>
</body>

<script>
    
    function scroll(element){   
        var ele = document.getElementById(element);   
        window.scrollTo(ele.offsetLeft,ele.offsetTop); 
    }
    
    function mymove(select_move_hour) {
        //document.getElementById(select_move_hour).scrollIntoView();
        var target = document.getElementById(select_move_hour);
        target.parentNode.scrollTop = target.offsetTop;
//        target.scrollIntoView(); // = target.offsetTop;
        return false;
    }
    
    function myreload(mycal_id) {
        document.getElementById("calendar_"+mycal_id).src="calendar_detail.php?cal_id="+ mycal_id;
    } 
</script>    

