<?php
 // KHU VỰC DIEU PHOI VE
    $task_type_arr = array (	
	1=>'Đăng ký vé',
	2=>'Chuyển khách',
	3=>'Sửa thông tin vé',
	4=>'Huỷ vé',
    );

    class CTasklogs extends CObject {
        var $task_id		= NULL;
        var $cal_detail_id      = NULL;        
        var $task_type		= NULL;
        var $task_spacecode	= NULL;
        var $task_cusphone	= NULL;        
        var $task_date		= NULL;
        var $task_oldvalue	= NULL;
        var $task_newvalue	= NULL;
        var $cal_id		= NULL;
        var $cal_no		= NULL;
        var $user_id		= NULL;

        
        function CTasklogs() {
            $this->CObject( 'tasklogs', 'task_id' );
        }
    }

    class CCalendar extends CObject {
        var $cal_id         = NULL;
        var $cal_taixe      = NULL;
        var $cal_soxe       = NULL;
        var $cal_phone      = NULL;
        var $cal_date       = NULL;
        var $cal_hour       = NULL;
        var $cal_minute     = NULL;

        var $cal_name       = NULL; 
        var $cal_khuvuc     = NULL; 
        var $cal_xuatphat   = NULL;
        var $cal_tuyen      = NULL;
        var $cal_loaixe      = NULL;
        
        function CCalendar() {
            $this->CObject( 'calendar', 'cal_id' );
        }
    }

    class CCalendar_details extends CObject {
        var $cal_detail_name    = NULL;         
        var $cal_detail_phone   = NULL;         
        var $cal_detail_address = NULL;         
        var $cal_detail_note    = NULL;         
        var $cal_detail_order   = NULL;         
        var $cal_detail_date    = NULL;         
        var $cal_detail_status  = NULL;
        var $cal_detail_seat    = NULL;
        var $cal_detail_seat_detail = NULL;       
        var $cal_id             = NULL;        
        var $user_id             = NULL;     
        
        function CCalendar_details() {
            $this->CObject( 'calendar_detail', 'cal_detail_id' );
        }
    }
    // END DIEU PHOI VE

////////////////////////////CCompany////////////////
 class CCompany extends CObject {
    var $company_id             = NULL;
    var $company_name           = NULL;
    var $company_officephone1   = NULL;
    var $company_officephone2   = NULL;
    var $company_fax            = NULL;
    var $company_taxcode        = NULL;
    var $company_address	= NULL;
    var $company_director	= NULL;
    var $company_accountant	= NULL;
    var $company_treasurer	= NULL;
    var $company_invoice_man1	= NULL;
    var $company_invoice_man2	= NULL;

    function CCompany() {
        $this->CObject( 'company', 'company_id' );
    }
}
////////////////////////////Notice////////////////
 class CNotice extends CObject {
    var $notice_id      = NULL;
    var $notice_text	= NULL;

    function CNotice() {
        $this->CObject( 'tbl_notice', 'notice_id' );
    }
}

////////////////////////////Ticket////////////////
$ticket_status_arr=array(
    1 => 'Mới nhận',
    2 => 'Đã được giải quyết',
);

class CForum_ticket extends CObject {
    var $ticket_id	= NULL;
    var $cus_id		= NULL;
    var $ticket_title	= NULL;
    var $ticket_note	= NULL;
    var $ticket_date	= NULL;
    var $user_id	= NULL;
    var $ticket_status	= NULL;

    function CForum_ticket() {
        $this->CObject( 'forum_ticket', 'ticket_id' );
    }
}

class CForum_reply extends CObject {
    var $reply_id		= NULL;
    var $reply_date		= NULL;
    var $reply_note		= NULL;
    var $ticket_id		= NULL;
    var $user_id		= NULL;

    function CForum_reply() {
        $this->CObject( 'forum_reply', 'reply_id' );
    }
}

class CUsers extends CObject {
    var $user_id		= NULL;
    var $user_username		= NULL;
    var $user_password	 	= NULL;
    var $user_fullname		= NULL;
    var $user_longname		= NULL;
    var $user_status		= NULL;
    var $user_level		= NULL;
    var $user_createdate        = NULL;
    var $user_address		= NULL;
    var $user_officephone       = NULL;
    var $user_phone		= NULL;
    var $cp_access		= NULL;
    var $user_type		= NULL;
    var $user_zone_chamcong     = NULL;

    function CUsers() {
        $this->CObject( 'tbl_users', 'user_id' );
    }
}

?>
