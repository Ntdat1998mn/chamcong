<div class="mt-element-ribbon ">
    <div class="ribbon ribbon-right ribbon-clip ribbon-shadow ribbon-round ribbon-border-dash-hor ribbon-color-info uppercase">
        <div class="ribbon-sub ribbon-clip ribbon-right"></div> Hệ thống
    </div>
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
            <?php saveplace_to_session('?m=home&a=sysconf','Hệ thống'); load_saveplace();?>
            </li>
        </ul>
    </div> 
</div>
 
<div class='col-md-6' style="margin-top: -40px;">
    <div class="portlet light portlet-fit portlet-form bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class=" icon-feed font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase">Bảng ghi thông báo</span>
            </div>
            <div class="actions">
            </div>
        </div>

       <div class="portlet-body">
            <div class="form-body form-horizontal pad-top">
                <span id="id_result_msg"></span>  
                <div class="form-group form-md-line-input ">
                    <label class="col-md-4 control-label" for="form_company">Nhập nội dung</label>
                    <div class="col-md-8">
                        <?php  $notice_arr = db_loadList("SELECT * FROM tbl_notice"); ?>
                        <div class="input-icon">
                            <textarea id="notice_text"  rows="4" class="notice_text form-control" placeholder="Nội dung" ><?php echo $notice_arr[0]["notice_text"] ?></textarea>
                            <div class="form-control-focus"> </div>
                            <span class="help-block">Ghi thông báo đến NV.</span>
                            <i class="fa fa-bell-o"></i>
                        </div>
                    </div>
                    <hr><br>
                    <div class="pull-right">
                        <span class='popbox'>
                            <button onclick="javascript:save_notice();" class="btn green">Đồng ý</button>
                            <button onclick="javascript:go_dashboard();" class="btn default">Hủy bỏ</button>
                        </span>
                            
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    
    <div class="portlet light portlet-fit portlet-form bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class=" icon-users font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase">Tài khoản đăng nhập</span>
            </div>
            <div class="actions">
            </div>
        </div>

        <div class="portlet-body">
            <!--<div class="table-scrollable">-->
                <?php  $user_arr = db_loadList("SELECT * FROM tbl_users WHERE user_zone_chamcong = 1 AND user_status=1 AND user_username != 'sys'"); ?>
                <table class="table table-striped table-bordered table-advance table-hover">
                    <thead>
                        <tr>
                            <th style="text-align: center;"><a href="#" onclick="show_phan_quyen();" > <image src="./images/plus.png"></a></th>
                            <th> <i class="fa fa-user"></i> Tài khoản </th>
                            <th> <i class="fa fa-book"></i> Tên nhân viên </th>
                            <th> <i class="fa fa-tasks"></i> Truy cập </th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        <?php $ind=0;  foreach ($user_arr as $row) { $ind++; ?>
                        <tr>
                            <td class="" style="text-align: center;"><?php echo $ind; ?></td>
                            <td class=""><?php echo $row['user_username']; ?></td>
                            <td class=""><?php echo $row['user_fullname']; ?></td>
                            <td> <i class="fa fa-edit"></i> <a href="#" onclick="show_phan_quyen();"  > Chi tiết </a></td>                                                        
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
        </div>
    </div>
    
</div>  
<div class='col-md-6' style="margin-top: -40px;">
    <div class="portlet light portlet-fit portlet-form bordered">
        <div class="portlet-title">
            <div class="caption">
                <i class=" icon-social-dribbble font-dark"></i>
                <span class="caption-subject font-dark sbold uppercase">Thông số chung</span>
            </div>
            <div class="actions">
            </div>
        </div>

        <div class="portlet-body">
            <div class="form-body form-horizontal">
                <div class="form-group form-md-line-input ">
                    <form action="#" class="form-horizontal" id="form_company" novalidate="novalidate">
                            <div class="form-body">
                                 <?php  $company_arr = db_loadList("SELECT * FROM company WHERE company_id=1"); ?>
                                <div class="alert alert-danger display-hide">
                                    <button class="close" data-close="alert"></button> Vui lòng kiểm tra dữ liệu nhập. </div>
                                <span id="id_result_msg_company"></span> 
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="form_company">Tên công ty  <span class="required" >*</span> </label>
                                    <div class="col-md-8 form-validate">
                                        <div class="input-icon right">
                                            <i class="icon fa" id="company_name_fa"></i>
                                            <input type="text" class="form-control" placeholder="" id="company_name" name="company_name" value="<?php echo $company_arr[0]['company_name']; ?>">
                                            <div class="form-control-focus"> </div>
                                        </div>    
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="form_company" id>Địa chỉ <span class="required " >*</span></label>
                                    <div class="col-md-8 form-validate">
                                        <div class="input-icon right">
                                            <i class="icon fa" id="company_address_fa"></i>
                                            <input type="text" class="form-control" placeholder=""  id="company_address" name="company_address" value="<?php echo $company_arr[0]['company_address']; ?>" >
                                         </div>    
                                    </div>
                                </div>
                                <div class="form-group form-md-line-input">
                                    <label class="col-md-4 control-label" for="form_company">Số ĐT  </label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" placeholder="" id="company_officephone1" name="company_officephone1" value="<?php echo $company_arr[0]['company_officephone1']; ?>">
                                    </div>
                                </div>
                            
                            </div>
                    <div class="pull-right">
                        <span class='popbox'>
                            <button type="submit" class="btn green">Lưu</button>
                            <button type="reset" onclick="javascript:go_dashboard();" class="btn default">Huỷ bỏ</button>
                        </span>
                    </div>
                </form>    
                </div>
                <div class="note note-success">
                    <p><u>Lưu ý:</u> Các thông tin trên sẽ được sử dụng trong việc xác lập in hoá đơn và một số nghiệp vụ tài chính.  Vui lòng nhập chính xác và nên Viết Hoa chữ cái đầu của các ô tên công ty và địa chỉ. </p>
                </div>
            </div>
        </div>
    </div>
</div>
<script language="JavaScript">
    $('#notice_text').css('color','orange');
    $('#notice_text').click(function(e) {  
        $('#notice_text').css('color','blue');
    });
    $('#id_result_msg').hide();  

    $('.form-validate').addClass('has-success');  
    $('#company_name_fa').addClass('fa-check-circle');  
    $('#company_address_fa').addClass('fa-check-circle');  
     
    function save_notice() {
        var notice_text = $('#notice_text').val();
        $.ajax({
            type: "POST",
            url: "ajax.php",            
            data: "action=notice"
                +"&notice_text="+notice_text
            ,
            success: function(msg){
                $('#notice_text').css('color','orange');
                toastr.success('<font face="Arial" size="2">Đã cập nhật thông báo mới </font>');  
                //$('#popbox').show();  setTimeout(function(){  $('#popbox').hide(); }, 2000);  
                $('#id_result_msg').html(msg); 
                $('#id_result_msg').show();  
                setTimeout(function(){  $('#id_result_msg').hide(); }, 6000);  
                $('#marqueetxt').html(notice_text);
            }
        });
    } 
    
    function save_company() {
        var company_name = $('#company_name').val();
        var company_address = $('#company_address').val();
        var company_taxcode = $('#company_taxcode').val();
        var company_officephone1 = $('#company_officephone1').val();
        var company_director = $('#company_director').val();
        var company_accountant = $('#company_accountant').val();
        var company_treasurer = $('#company_treasurer').val();

        $.ajax({
            type: "POST",
            url: "ajax.php",            
            data: "action=company"
                +"&company_name="+company_name
                +"&company_address="+company_address
                +"&company_taxcode="+company_taxcode
                +"&company_officephone1="+company_officephone1
                +"&company_director="+company_director        
                +"&company_accountant="+company_accountant        
                +"&company_treasurer="+company_treasurer                
            ,
            success: function(msg){
                $('#id_result_msg_company').html(msg); 
                $('#id_result_msg_company').show(); 
                toastr.success('<font face="Arial" size="2">Đã cập nhật </font>');  
                setTimeout(function(){  $('#id_result_msg_company').hide(); }, 6000);  
            }
        });
    }   
    function show_phan_quyen() {
        document.location="?m=home&a=phanquyen";
    }
</script>



          

