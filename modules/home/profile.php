<div class="mt-element-ribbon ">
   <div class="ribbon ribbon-right ribbon-clip ribbon-shadow ribbon-round ribbon-border-dash-hor ribbon-color-info uppercase">
        <div class="ribbon-sub ribbon-clip ribbon-right"></div> Hồ sơ cá nhân
    </div>      
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
            <?php saveplace_to_session('?m=home&a=profile','Hồ sơ cá nhân'); load_saveplace();?>
            </li>
        </ul>
    </div> 
</div>
 
<link href="./css/profile.css" rel="stylesheet" type="text/css"/>
<div class="row"  style="margin-top:-40px; margin-left:10px;">
    <input type="hidden" id="username" name="username" value="sys">
    <!-- BEGIN PROFILE SIDEBAR -->
    <div class="col-md-3">
        <div class="profile-sidebar">
            <div class="portlet light profile-sidebar-portlet">
                <div class="profile-userpic">
                    <?php
                        $info_arr = db_loadList("SELECT * FROM tbl_users WHERE user_id=".$AppUI->user_id);
                        $filename1 =  file_exists('./uploads/profile_'.$AppUI->user_id.'.jpg') ?  './uploads/profile_'.$AppUI->user_id.'.jpg' : './uploads/profile-default.png';
                    ?>
                    <img id="img_profile" src="<?php echo $filename1; ?>" class="img-responsive" alt="" onclick="change_image(<?php echo $AppUI->user_id; ?>,'<?php echo $AppUI->user_id; ?>')" style="cursor:pointer;">
                </div>
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name">
                       <?php echo $info_arr[0]["user_longname"]; ?>
                    </div>
                    <div class="profile-usertitle-job">
                       <?php echo $info_arr[0]["user_address"]; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-9">
        <div class="profile-content">
            <div class="row">
                <div class="portlet light col-md-6">
                    <div class="portlet-title tabbable-line">
                        <div class="caption caption-md">
                            <i class="icon-globe theme-font hide"></i>
                            <span class="caption-subject font-blue-madison bold uppercase">Thông tin cá nhân </span><font size="1">[<a href="#" onclick="show_edit_user()">Sửa</a>]</font>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="tab-content">
                            <!-- PERSONAL INFO TAB -->
                            <div class="tab-pane active" id="tab_1_1">
                                <div class="form-group">
                                    <label class="control-label">Tên hiển thị: <?php echo $info_arr[0]["user_fullname"]; ?></label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Email: <?php echo $info_arr[0]["user_email"]; ?></label>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Tên đầy đủ: <?php echo $info_arr[0]["user_longname"]; ?></label>
                               </div>
                                <div class="form-group">
                                    <label class="control-label">Điện thoại di động:  <?php echo $info_arr[0]["user_phone"]; ?></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="portlet light col-md-6">
                    <div class="portlet-title tabbable-line">
                        <div class="caption caption-md">
                            <i class="icon-globe theme-font hide"></i>
                            <span class="caption-subject font-blue-madison bold uppercase">Thay đổi mật mã đăng nhập</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="form-group">
                            <div class="form-group form-validate">
                                <label class="col-md-4 control-label" style="padding-left: 0px; ">Mật mã mới</label>                                
                                <div class="bg-input">
                                    <div class="input-icon right">
                                        <i class="icon fa"></i>
                                        <input name="password" type="password" id="user_password" class="form-control"   placeholder="Mật mã">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-group form-validate">
                                <label class="col-md-4 control-label"   style="padding-left: 0px; ">Xác nhận lại</label>
                                <div class="bg-input">
                                    <div class="input-icon right">
                                        <i class="icon fa"></i>
                                        <input name="password" type="password" id="user_cpassword" class="form-control"   placeholder="Xác nhận lại">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <input type="button" onclick="submitIt()" name="Submit" class="btn blue pull-right" value="Thay đổi">
                        </div>
                    </div>
                </div>
                
                
            </div>
        </div>
    </div>
</div>

                
 <Script language="JavaScript">
    function change_image(user_id,image_id) {
        setTimeout(function(){  
             call_camera(user_id, image_id);
        }, 3);
    }
    function submitIt(){
        var user_password  = $("#user_password").val();
        var user_cpassword = $("#user_cpassword").val();
        
        if(user_password.length<6){
            alert("Vui lòng nhập mật mã ít nhất là 6 chữ và số " );
            $("#user_password").focus();
            return false;
        } else if( user_cpassword  !=  user_password ){
            alert("Xác nhận mật mã không đúng. Vui lòng nhập lại." );
            $("#user_cpassword").focus();
            return false;            
        }else{
            $.ajax({
                type: "POST",
                url: "ajax.php",
                data: "action=edit_password&pass="+user_password,
                success: function(msg){
                    toastr.info('<font face="Arial" size="2">Đã thay đổi mật mã. Vui lòng đăng nhập hệ thống</font>'); 
                    setTimeout(function(){  document.location='?logout=-1'; }, 1000);
                }
            }); 
        }
    }
    function save_profile_info() {
        var user_fullname   = $("#user_fullname").val();
        var user_email      = $("#user_email").val();
        var user_longname   = $("#user_longname").val();
        var user_phone      = $("#user_phone").val();
        var user_address    = $("#user_address").val();
        
        $.ajax({
            type: "POST",
            url: "ajax.php",
            data: "action=save_profile&user_fullname="+user_fullname+"&user_email="+user_email+"&user_longname="+user_longname+"&user_phone="+user_phone+"&user_address="+user_address,
            success: function(msg){
               location.reload();
            }
        }); 
    }
    function show_edit_user() {
        $.ajax({
            type: "POST",
            url: "ajax.php",
            data: "action=load_profile",
            success: function(msg){
                $('#mypop_ajax_show').html(msg);
            }
        }); 
        document.getElementById("popup_layer_edit").style.display = 'block';
        document.getElementById("thewindowbackground_edit").style.display = 'block';
    }
    function closepopup() {
        document.getElementById("popup_layer_edit").style.display = 'none';
        document.getElementById("thewindowbackground_edit").style.display = 'none';
    } 
</Script>

<!-- POPUP EDIT-->
    <div id="thewindowbackground_camera" style="display:none; width:100%; height:100%; background-color:#ffffff; position:absolute; left:0px; top: 0px;opacity: .1; filter: alpha(opacity=90); z-index:99999;"  onclick="CloseCamera();"></div>
    <div id="popup_layer_camera" style="background-color:#ffffff; display:none; position: absolute;left: 30%;top: 10px;width: 752px; height: 358px; margin-left: -100px;margin-top: 30px; z-index:999999;">
        <div class = "mytarget">
            <style>
                .progress {
                    margin-left:10px;
                    width: 320px;
                    height:25px;
                }
                .progress-bar-success {
                    background-color: yellow;
                }
                .modal-footer {
                     border-top: 0px solid #e5e5e5 !important; 
                }
                button, input, select, textarea {
                    font-family: inherit;
                    font-size: 10px;
                    line-height: inherit;
                }
           
            </style>
            <table  bgcolor="#FFFFFF" border="1" cellspacing="0" cellpadding="0" width="100%"  style="border-width: thin;  border-spacing: 2px;   border-style: none; border-color: black; ">
            <tr>
                <td style="vertical-align: top">    
                    <div style="margin-top: -10px; width: 100%; height: 10px; border-bottom: 1px solid ORANGE; text-align: center"> </div>
                    
                    <div class="pageTitle" id="pageT" style="background-color:#238dc1">
                        <table width="100%"  cellspacing="0" cellpadding="0" border=0 class="goUp"><tr>
                            <td align="left" style="padding:0px;margin:0px; width:100%;"><div class="titleBox" style="color:white; height: 25px; font-size: 18px;">
                                <i class="fa fa-bars menu-i" onclick="CloseCamera()" style="cursor:hand; color:white; "></i> 
                                &nbsp;&nbsp; Ảnh đại diện của bạn</div></td>
                            <td class="goUp" valign="bottom" align="right"></td></tr>
                        </table> 
                    </div> 
                    <br> <br>  
                    <form class="cmxform form-horizontal tasi-form" role="form"  id="frmUpload" method="post" action="" name="frmUpload" enctype="multipart/form-data">
                        
                    <div class="form-group ">
                        <div class="col-lg-8">
                            <span class="btn btn-success fileinput-button" style="background-color:#238dc1">
                                <i class="glyphicon glyphicon-plus"></i> <span >Chọn file hình ảnh</span>
                                <input id="fileupload" type="file" name="files[]">
                            </span>
                            <span id="files" class="files"></span>
                            <input id="filename" name="filename" type="hidden" style="width:0px; border:0px"><br>
                            <br>
                            <div id="progress" class="progress">
                                <div class="progress-bar progress-bar-success"></div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="modal-footer" style="margin-top:-20px">
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button type="submit"  class="btn btn-danger" name="btsubmit" id="btsubmit"> <i class="fa fa-upload"></i> Đồng ý thay hình </button>
                                <button type="button" class="btn " name="btcancel"  id="btdelete" onclick="mybtdelete()">  <i class="fa fa-trash-o"></i> Xoá  </button>
                            </div>
                            <input type="hidden" name="dosql" value="kiosk_uploadfile.php">
                            <input type="hidden" name="action" value="">
                        </div>
                    </div>
                      </form>
                    <br><br >
                    <font size="2"><i style="margin-left:2px;">
                        &nbsp;<u>Hướng dẫn:</u> Để thay đổi ảnh đại diện:
                        <br>&nbsp;- Chụp hình ở hình bên 
                        <br>&nbsp;- Tải tập tin hình ảnh từ máy tính, đt (.gif, .jpg, .png)  </i> 
                    </font>
                    
                </td>
                <td>
                    <hr>
                    <div style="width:420px;height:420px;border:6px inset #238dc1;margin-top: -42px">
                        <input type="hidden" value="0" id="danhmuc_id">
                        <input type="hidden" value="0" id="image_id">
                        <!-- CAMERA CODE-->
                            <script type="text/javascript" src="webcam.js"></script>
                            <script language="JavaScript">
                                webcam.set_quality( 90 ); // JPEG quality (1 - 100)
                                webcam.set_shutter_sound( true ); // play shutter click sound
                            </script>
                            <script language="JavaScript">
                                document.write( webcam.get_html(410, 410) ); //Next, write the movie to the page at 320x240
                            </script>
                            <br/>
                                <input type=button value="Chụp hình" onClick="take_snapshot()">
                            <!-- Code to handle the server response (see webcam.php) -->
                            <script language="JavaScript">
                                webcam.set_hook( 'onComplete', 'my_completion_handler' );
                                function take_snapshot() {
                                    var tenhinhchup= document.getElementById('image_id').value ;
                                    webcam.set_api_url( 'webcam.php?phieu_code='+tenhinhchup );
                                    webcam.snap();	
                                }
                                function my_completion_handler(msg) {
                                    // extract URL out of PHP output
                                    if (msg.match(/(http\:\/\/\S+)/)) {
                                        var image_url = RegExp.$1;
                                        // show JPEG image in page
                                        //document.getElementById('upload_results').innerHTML =  '<a>Đã thay ảnh đại diện!</a> '; //  + image_url; 
                                        // reset camera for another shot
                                        // Reload frane kiosk_image
                                        toastr.info('<font face="Arial" size="2">Đã thay ảnh đại diện</font>'); 
                                        webcam.reset();
                                        setTimeout(function(){ location.reload(); }, 1000);
                                        
                                        //document.getElementById("frame_second").src="kiosk_image.php?danhmuc_id="+danhmuc_id;
                                    }
                                    else alert("PHP Error: " + msg);
                                }
                            </script>
                            <!--<div id="upload_results" style="background-color:#eee; "></div>-->
                            <!-- END CAMERA -->
                    </div>
                    <br> <br>
                </td>
            
            </tr>
        </table>
        </div>    
    </div>
<!-- END POPUP EDIT-->

<script type="text/javascript">
    function call_camera(danhmuc_id, image_id) {
        document.getElementById("danhmuc_id").value = danhmuc_id;                
        document.getElementById("image_id").value = 'profile_'+ image_id;        
        document.getElementById("popup_layer_camera").style.display = 'block';
        document.getElementById("thewindowbackground_camera").style.display = 'block';
    }
    
    function CloseCamera() { 
        document.getElementById("popup_layer_camera").style.display = 'none';
        document.getElementById("thewindowbackground_camera").style.display = 'none';
    }
</script>

<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.validate.min.js"></script>
<script src="js/jquery.ui.widget.js"></script>
<script src="js/jquery.fileupload.js"></script>
<link rel="stylesheet" href="css/jquery.fileupload.css">
<!-- Modal script -->
<Script language="JavaScript">

    function mybtdelete() { 
        var form_data = new FormData();  
        var tenhinhchup= document.getElementById('image_id').value ;
        form_data.append('action', 'delete_image');
        form_data.append('image_id', tenhinhchup);   
        $.ajax({
                url: 'kiosk_image.php', // point to server-side PHP script 
                dataType: 'text',  // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,                         
                type: 'post',
                success: function(msg){
                    toastr.error('Đã xoá hình ảnh.'); 
                    location.reload();
                }
            });
    }
    
    $('#btsubmit').click(function(){
        $("#frmUpload").validate( {
            rules: {
                fileupload: {
                    required: true,
                    minlength: 0
                }
            },
            messages: {
                fileupload : "Vui lòng chọn file."
            },
            submitHandler: function (form) {
                var tenhinhchup= document.getElementById('image_id').value ;
                var file_data = $('#fileupload').prop('files')[0];   
                var form_data = new FormData();                  
                form_data.append('file', file_data);
                form_data.append('action', 'upload_image');
                form_data.append('image_id', tenhinhchup);                
                $.ajax({
                    url: 'kiosk_image.php', // point to server-side PHP script 
                    dataType: 'text',  // what to expect back from the PHP script, if anything
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,                         
                    type: 'post',
                    success: function(msg){
                        if(msg=='NOTOK') {
                            alert('File không hợp lệ. vui lòng chọn file khác');
                        } else {
                            toastr.info('<font face="Arial" size="2">Đã thay ảnh đại diện</font>'); 
                            location.reload();
                        }
                    }
                });
                 
            }
        } );
    });

    $('#fileupload').bind('change', function() {
        for(i =0 ;i <= 100; i++){
            var delay = Math.floor((Math.random() * 10) + 1) * 100;

            $('#progress .progress-bar').css(
                'width',
                i + '%'
            ).delay( delay );
        }
    });
</Script>


                

          

