<?php
error_reporting (E_ALL ^ E_NOTICE);
class PDF extends FPDF
{

function Footer()
{
	global $lang;
	global $language;
    //Go to 1.5 cm from bottom
    $this->SetY(-15);
    //Select Arial italic 8
    $this->SetFont('Arial','I',8);
    //Print centered page number
    $this->Cell(0,10,$lang["$language"]['page'].' '.$this->PageNo(),0,0,'C');
}

function Cover($cover)
{
    $this->SetFont('Arial','',15);
	$this->MultiCell(150,9,$cover);
	$this->Ln();
}

function Header()
{
	global $title;
    //Select Arial bold 15
    $this->SetFont('Arial','B',15);
    //Move to the right
    $this->Cell(85);
    //Framed title
    $this->Cell(30,10,$title,0,0,'C');
    //Line break
    $this->Ln(10);
}

function TableHeader($header,$w)
{
    $this->SetFillColor(255,0,0);
    $this->SetTextColor(255);
    $this->SetDrawColor(128,0,0);
    $this->SetLineWidth(.3);
    $this->SetFont('','B',11);

    for($i=0;$i<count($header);$i++)
        $this->Cell($w[$i],10,$header[$i],1,0,'C',1);
    $this->Ln();
}

//Colored table
function FancyTable($header,$data,$w)
{

	$this->TableHeader($header,$w);

    //Color and font restoration
    $this->SetFillColor(224,235,255);
    $this->SetTextColor(0);
    $this->SetFont('');
    //Data
    $fill=0;
	$supercont=1;
    foreach($data as $row)
    {
		$contador=0;
		foreach($row as $valor) {
        	$this->Cell($w[$contador],6,$valor,'LR',0,'C',$fill);
			$contador++;
		}
        $this->Ln();
        $fill=!$fill;
		if($supercont%40 == 0) {
    		$this->Cell(array_sum($w),0,'','T');
			$this->AddPage();
			$this->TableHeader($header,$w);
    		$this->SetFillColor(224,235,255);
		    $this->SetTextColor(0);
		    $this->SetFont('');
		}
		$supercont++;
    }
    //CANH BO $this->Cell(array_sum($w),0,'','T');
    
}
}

function export_csv($header,$data)
{
//header("Content-Type: application/csv-tab-delimited-table");
header("Content-type: application/x-msdownload");
header("Content-disposition: filename=".date(d)."_".date(m)."_".date(Y).".csv");

$linea="";
foreach($header as $valor) {
//	$linea.="\"$valor\";";
}
//$linea=substr($linea,0,-1);

//print $linea."\r\n";

foreach($data as $valor) {
	$linea="";
	foreach($valor as $subvalor) {
		$linea.="\"$subvalor\",";
	}
	$linea=substr($linea,0,-1);
	print $linea."\r\n";
}
}


?>