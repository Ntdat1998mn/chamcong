<?php error_reporting (E_ALL ^ E_NOTICE); ?>
<?php

require('fpdf.php');
require('pdf_class.php');

$filename = $_POST['filename'];
 
$header = unserialize(rawurldecode($_POST['head']));
$data   = unserialize(rawurldecode($_POST['rawdata']));
$width  = unserialize(rawurldecode($_POST['width']));
$title  = unserialize(rawurldecode($_POST['title']));
$cover  = unserialize(rawurldecode($_POST['cover']));

if(isset($_POST['pdf']) || isset($_POST['pdf_x'])) {

	//	echo "<pre>DATA"; print_r($data); echo "</pre>"; 
	//	echo "<pre>HEADER"; print_r($header); echo "</pre>"; 
	//	echo "<pre>FILENAME"; print_r($filename); echo "</pre>"; 
	//	echo "<pre>TITLE"; print_r($title); echo "</pre>"; 
	//	echo "<pre>COVER"; print_r($cover); echo "</pre>"; 
	//exit;

	$pdf=new PDF();
	$pdf->SetFont('Arial','',12);
	$pdf->SetAutoPageBreak(true);
	$pdf->SetLeftMargin(1);
	$pdf->SetRightMargin(1);
	$pdf->AddPage();
	if($cover<>"") {
		$pdf->Cover($cover);
	}
	//$pdf->AddPage();
	$pdf->FancyTable($header,$data,$width);
	//echo "HERE";exit;
	//$pdf->FancyTable($header,$data,$width);
	$pdf->Output( $filename.'.pdf','D');
} else {
	export_csv($header,$data);
}
	// print "EXPORT FILE PDF FINISH";
?>
