<?php
$config = array();
require_once( "./includes/config.php" );
require_once( "./classes/ui.class.php" );
require_once( "./includes/main_functions.php" );

session_name( 'QlyChamCong' );
if (get_cfg_var( 'session.auto_start' ) > 0) {
    session_write_close();
}
session_start();
if (!isset( $_SESSION['AppUI'] ) || isset($_GET['logout'])) {
    $_SESSION['AppUI'] = new CAppUI();
}
$AppUI =& $_SESSION['AppUI'];
$AppUI->setConfig( $config );
$AppUI->checkStyle();
require_once( "./includes/db_connect.php" );
$AppUI->doLogin();
//echo $_POST['username']); exit;
if (isset($_POST['username'])) {
    $username 	= getParam( $_POST, 'username', '' );
    $password 	= getParam( $_POST, 'password', '' );
    $txtPassSec     = getParam( $_POST, 'txtPassSec', '' );
    $redirect       = getParam( $_REQUEST, 'redirect', '' );

    $ok = $AppUI->login( $username, $password, $txtPassSec );
    if (!$ok) {
        $AppUI->setMsg( ' truy cập không đúng' );
    }
    $AppUI->redirect( "$redirect" ,"", "index");
}

$m = getParam( $_GET, 'm', 'home' );
$a = '';
$u = '';

// check if we are logged in
if ($AppUI->doLogin()) {
    $redirect = @$_SERVER['QUERY_STRING'];
    if (strpos( $redirect, 'logout' ) !== false) {
        $redirect = '';
    }
    require "./style/login.php";
    session_unset();
    session_destroy();
    exit;
}

$m = getParam( $_GET, 'm', 'home' );
$u = getParam( $_GET, 'u', '' );
$a = getParam( $_GET, 'a', 'dashboard' );

    require_once( $AppUI->getSystemClass( 'object' ) );
    require_once( "./includes/db_connect.php" );
    @include_once( "./modules/home/home.class.php" );
  
    $action  =   $_POST['action'];
     
    if($action=='notice') {
        $obj = new CNotice(); 
        $obj->notice_id=1;
        $obj->notice_text = $_POST['notice_text'];
        $obj->store();
        //echo '<div  class="alert alert-success"> <i class="fa fa-check" style="font-size:20px"></i>Hệ thống đã cập nhật thông báo mới </div>';
        echo '    <div class="note note-success">
                    <p><u>Thông báo:</u> Hệ thống đã cập nhật. </p>
                </div>
                ';
    } 
    if($action=='company') {
        $obj = new CCompany(); 
        $obj->company_id             = 1;
        $obj->company_name           = $_POST['company_name'];
        $obj->company_officephone1   = $_POST['company_officephone1'];
        //$obj->company_officephone2   = $_POST['company_officephone2'];
        //$obj->company_fax            = $_POST['company_fax'];
        $obj->company_taxcode        = $_POST['company_taxcode'];
        $obj->company_address        = $_POST['company_address'];
        $obj->company_director       = $_POST['company_director'];
        $obj->company_accountant     = $_POST['company_accountant'];
        $obj->company_treasurer      = $_POST['company_treasurer'];
        //$obj->company_invoice_man1   = $_POST['company_invoice_man1'];
        //$obj->company_invoice_man2   = $_POST['company_invoice_man2'];
        $obj->store();
        //echo '<div class="alert alert-success"> <i class="fa fa-check" style="font-size:20px"></i>Hệ thống đã cập nhật thông tin </div>';
            echo '    <div class="note note-success">
                <p><u>Thông báo:</u> Hệ thống đã cập nhật. </p>
                </div>
            ';
    } 
    if($_POST['action']=='load_profile') {
          $info_arr = db_loadList("SELECT * FROM tbl_users WHERE user_id=".$AppUI->user_id);
        ?>
            <div class="col-md-12">
                <div class="profile-content">
                    <div class="row">
                        <div class="portlet light col-md-12">
                            <div class="portlet-title tabbable-line">
                                <div class="caption caption-md">
                                    <i class="icon-globe theme-font hide"></i>
                                    <span class="caption-subject font-blue-madison bold uppercase">Sửa thông tin cá nhân </span>
                                </div>
                            </div>
                            <div class=" col-md-6">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_1_1">
                                        <div class="form-group">
                                            <label class="control-label">Tên hiển thị:</label>
                                            <input name="user_fullname" type="text" id="user_fullname" class="form-control"  value="<?php echo $info_arr[0]['user_fullname']; ?>">
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Email: </label>
                                            <input name="user_email" type="text" id="user_email" class="form-control"   value="<?php echo $info_arr[0]['user_email']; ?>">
                                            
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Tên đầy đủ: </label>
                                            <input name="user_longname" type="text" id="user_longname" class="form-control"   value="<?php echo $info_arr[0]['user_longname']; ?>">
                                       </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="  col-md-6">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_1_1">
                                        <div class="form-group">
                                            <label class="control-label">Điện thoại di động:  </label>
                                            <input name="user_phone" type="text" id="user_phone" class="form-control"   value="<?php echo $info_arr[0]['user_phone']; ?>">
                                            
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Chức vụ:  </label>
                                            <input name="user_address" type="text" id="user_address" class="form-control"  value="<?php echo $info_arr[0]['user_address']; ?>">
                                            
                                        </div>
                                    </div>
                                    <div class="pull-right">
                                        <span class='popbox'>
                                            <button onclick="javascript:save_profile_info();" class="btn green">Đồng ý</button>
                                            <button onclick="javascript:closepopup();" class="btn default">Hủy bỏ</button>
                                        </span>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        
        <?php
        die;
    }
    if($_POST['action']=='edit_password') {
        $pass= $_POST['pass'];
        db_exec(  "UPDATE tbl_users SET user_password = '".$pass."' WHERE user_id=".$AppUI->user_id );
    }
    if($_POST['action']=='save_profile') {
        $user_fullname   = $_POST['user_fullname'];
        $user_email      = $_POST['user_email'];
        $user_longname   = $_POST['user_longname'];
        $user_phone      = $_POST['user_phone'];
        $user_address    = $_POST['user_address'];
        
        db_exec(  "UPDATE tbl_users SET "
                . " user_fullname = '".$user_fullname."',"
                . " user_email = '".$user_email."',"
                . " user_longname = '".$user_longname."',"
                . " user_phone = '".$user_phone."',"
                . " user_address = '".$user_address."'"                
                . " WHERE user_id=".$AppUI->user_id );
    }

    if($_POST['action']=='load_2grids') {
        $cal_id1 = $_POST["calid1"];
        $cal_id2 = $_POST["calid2"];        
        ?>
            <div class="col-md-12">
                <div class="profile-content">
                    <div class="row">
                        <div class="portlet light col-md-12">
                            <div class="portlet-title tabbable-line">
                                <div class="caption caption-md">
                                    <i class="icon-globe theme-font hide"></i>
                                    <span class="caption-subject font-blue-madison bold uppercase">Chuyển khách</span>
                                </div>
                            </div>
                            <iframe  id="calendar_transfer" src="calendar_transfer.php?cal_id1=<?php echo $cal_id1; ?>&cal_id2=<?php echo $cal_id2; ?>"   style="margin-left: -20px; width: 800px; height: 300px; border:0; position: relative; z-index: 100;"></iframe>
                            
                            <div class="pull-right">
                                <span class='popbox'>
                                    <button onclick="javascript:closepopup();" class="btn green">Đóng cửa sổ</button>
                                    <!--<button onclick="javascript:closepopup();" class="btn default">Hủy bỏ</button>-->
                                </span>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        
        <?php
        die;
    }    
    
    
    if($_POST['action']=='ask_why_delete') {
        $input_check_arr = $_POST['input_check_arr'];
          //echo "<pre>"; echo('--'.$input_check_arr); echo "</pre>";
        $my_arr = explode(',', $input_check_arr);
        //echo "<pre>"; print_r($my_arr);
        $_text='';
        $ind=0;
        foreach($my_arr as $row) {
            $ind++;
            if($row > 0) {  
                $my_arr_value[$ind] = $row; 
                $_text .= $row.',';
            } 
        }
        $list_arr = db_loadList( "SELECT * FROM calendar_detail WHERE cal_detail_id IN (".$_text." 0)" );
        ?>
            <div class="col-md-12">
                <div class="profile-content">
                    <div class="row">
                        <div class="portlet light col-md-12">
                            <div class="portlet-title tabbable-line">
                                <div class="caption caption-md">
                                    <i class="icon-globe theme-font hide"></i>
                                    <span class="caption-subject font-blue-madison bold uppercase">Lý do huỷ vé</span>
                                </div>
                            </div>
                            Danh sách vé chuẩn bị huỷ:
                            <br>
                            <br><font color="red" size="3">
                            <?php
                                foreach($list_arr as $row):
                                    echo $row['cal_detail_address'] . "-" . $row['cal_detail_phone']."- Số vé:". $row['cal_detail_seat'] ."-ghế:". $row['cal_detail_seat_detail'].'<br>';
                                endforeach;
                            ?>
                            </font>
                            <br>
                            
                            <div class="form-group">
                                <label for="comment">Lý do hủy vé:</label>
                                <input type="hidden" id="mycal_id" value="<?php echo $list_arr[0]['cal_id']; ?>">
                                <textarea class="form-control" rows="5" id="ly_do_huy_ve"></textarea>
                            </div>
                            
                            <div class="pull-right">
                                <span class='popbox' nowrap style="inline-box-align: initial">
                                    <button id="dong_y_huy_ve" onclick="javascript:dong_y_huy_ve();" class="btn green">Đồng ý huỷ vé</button>    
                                    <button onclick="javascript:closepopup();" class="btn default">Đóng cửa sổ</button>
                                </span>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <script>
                function dong_y_huy_ve() {
                    if( $('#ly_do_huy_ve').val().length < 4 ) {
                        alert('Vui lòng nhập lý do huỷ vé. Lý do phải tối thiểu từ 2 chữ trở lên');
                    } else {
                        $("#ly_do_huy_ve").attr("disabled", "disabled"); 
                    <?php foreach($list_arr as $row): ?>
                        $.ajax({
                            type: "POST",
                            url: "calendar_detail.php",
                            data: "action=delete_excel&cal_detail_id=<?php echo $row['cal_detail_id']; ?>&ly_do_huy_ve="+$('#ly_do_huy_ve').val(),
                            success: function(msg){
                            }
                        });  
                    <?php endforeach; ?>   
                        toastr.error('<font face="Arial" size="2">Đã huỷ vé</font>'); 
                        setTimeout(function(){ closepopup(); }, 2000);
                    }
                }
            </script>    
        <?php
        die;
    }    
    
?>

 