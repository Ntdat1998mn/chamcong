<?php
        $phieu_code = $_GET['phieu_code'];
        
        $filename = 'uploads/'.$phieu_code. '.jpg';
        @unlink($filename);
        //$filename = 'img/'.date('YmdHis') . '.jpg';
        $result = file_put_contents( $filename, file_get_contents('php://input') );
        if (!$result) {
                print "ERROR: Failed to write data to $filename, check permissions\n";
                exit();
        }

	function Watermark($image_path, $watermark_path)
	{
		if(!$watermark_path) 
		{
			return false;
		}
		$watermark_id = imagecreatefrompng($watermark_path);
		imagealphablending($watermark_id, false);
		imagesavealpha($watermark_id, true);
		
		$info_img = getimagesize($image_path);
		$info_wtm = getimagesize($watermark_path);
		$fileType = strtolower($info_img['mime']);
		
		$image_w 		= $info_img[0];
		$image_h 		= $info_img[1];
		$watermark_w	= $info_wtm[0];
		$watermark_h	= $info_wtm[1];
			
		switch($fileType)
		{
			case	'image/gif':	return false;									break;
			case	'image/png': 	$image_id = imagecreatefrompng($image_path);	break;
			default: $image_id = imagecreatefromjpeg($image_path);	break;
		}
		/* Watermark in the bottom right of image*/
		$dest_x  = ($image_w - $watermark_w); 
		$dest_y  = ($image_h  - $watermark_h);
		
		/* Watermark in the middle of image 
		$dest_x = round(( $image_height / 2 ) - ( $logo_h / 2 ));
		$dest_y = round(( $image_w / 2 ) - ( $logo_w / 2 ));
		*/
		imagecopy($image_id, $watermark_id, $dest_x, $dest_y, 0, 0, $watermark_w, $watermark_h);
		
		//override to image
		switch($fileType)
		{
                    case	'image/png': 	imagepng ($image_id, $image_path); break;
                    default: imagejpeg($image_id, $image_path); 		break;
		}       		 
		imagedestroy($image_id);
		imagedestroy($watermark_id);
		return true;
	}

Watermark($filename, 'logo1.png');
$url = 'http://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['REQUEST_URI']) . '/' . $filename;
print "$url\n";

?>
