<?php 
error_reporting (E_ALL ^ E_NOTICE); 
$config = array();
require_once( "./includes/config.php" );
require_once( "./classes/ui.class.php" );
require_once( "./includes/main_functions.php" );
date_default_timezone_set("Asia/Bangkok");
session_name( 'QlyChamCong' );
if (get_cfg_var( 'session.auto_start' ) > 0) {
	session_write_close();
}
session_start();
if (!isset( $_SESSION['AppUI'] ) || isset($_GET['logout'])) {
    $_SESSION['AppUI'] = new CAppUI();
}
$AppUI =& $_SESSION['AppUI'];
$AppUI->setConfig( $config );
$AppUI->checkStyle();
require_once( $AppUI->getSystemClass( 'object' ) );
require_once( "./includes/db_connect.php" );

include_once( "./modules/home/home.class.php" );
    
    if($_POST['action'] == 'add_user' ) {
        $obj = new CUsers();  
            $obj->user_username      = 'cc_default';
            $obj->user_status        = '1';
            $obj->user_level         = '100';    
            $obj->user_type          = 'nhanvien';
            $obj->cp_access          = '1';  
            $obj->user_zone_chamcong = 1;
            $obj->store();       
        die;
    }
    
    if($_POST['action'] == 'delete_user' ) {
        $user_id = $_POST['user_id'];
        $sql = " DELETE FROM tbl_users  WHERE user_id = ".$user_id;
        db_exec($sql);    
        die;
    }

    if($_POST['action'] == 'save_user' ) {
        $user_id = $_POST['user_id'];
        $field    = $_POST['field'];
        $val      = $_POST['val'];        
        $sql = " UPDATE tbl_users SET ".$field." = '".$val."' WHERE user_id = ".$user_id;
        db_exec($sql);    
        die;
    }
    
    if($_GET['action'] == 'thiet_lap_quyen_phong_ban' ) {
        $user_id = $_GET['user_id'];
        $danhmuc_arr_1 = db_loadList("SELECT danhmuc_id,danhmuc_name FROM ns_danhmuc_phongban WHERE danhmuc_status =1 ORDER BY danhmuc_order");  
        foreach ($danhmuc_arr_1 as $row) : 
            $row['checked'] =0;
            $danhmuc_arr[] = $row;
        endforeach;
            if($user_id >  0 ) {
                $list_perms_arr = db_loadHashList( "SELECT perms_name,perms_id FROM kiosk_user_permissions WHERE perms_type='PHONGBAN' AND user_id=".$user_id );
                foreach ($danhmuc_arr as $k=>$row) :
                    if( $list_perms_arr[ $row['danhmuc_id'] ] > 0  ) { $danhmuc_arr[$k]['checked'] = '1'; }
                endforeach;
                echo '{
                    "total": '.count($danhmuc_arr).',
                    "records": [
                   ';
                    if(count($danhmuc_arr > 0) ) {
                        $idx=0;
                        foreach ($danhmuc_arr as $row) :
                            $idx++; $mark = ' style: {';
                                $mark .="2 :'background-color: white;  color:blue; '";
                            $mark .= ' }';
                            echo "
                            {    recid: ".$idx.",  danhmuc_id: ".$row['danhmuc_id'].", 
                                'danhmuc_order':".$idx.",                                    
                                'danhmuc_name':'".$row['danhmuc_name']."', 
                                'check': '<input type=\"checkbox\" ". (($row['checked'] =='1')  ?   'checked'  : '' ) ." onclick=\"mycheck_quyenphongban(this.checked,".$user_id.",".$row['danhmuc_id'].")\" >', 
                                ".$mark."
                            },
                            ";
                        endforeach;
                    }    
                echo '
                        ]
                    }
                ';
            } else {
               echo '{
                   "total": 0,
                   "records": [
                       ]
                   }
               ';
            }
        die;
    }
    
    if($_POST['action'] == 'save_quyen' ) {
        $perms_type =   $_POST['perms_type'];
        $checked    =   $_POST['checked'];
        $user_id    =   $_POST['user_id'];
        $name       =   $_POST['name'];        
        if($checked== 'false') {
            db_exec( "DELETE FROM kiosk_user_permissions WHERE user_id= ".$user_id." AND perms_name= '".$name."' ") ;
            die;
        }        
        $sql ="INSERT kiosk_user_permissions(perms_name,perms_type,user_id) VALUES ('".$name."','".$perms_type."',".$user_id." ) ";
        echo $sql;
        db_exec($sql);
        die;
         
    }
    
?>    
<head>
    <link rel="stylesheet" type="text/css" href="js/w2ui/w2ui-1.4.3.css" />
    <script src="js/1.12.1_jquery.js"></script>
    <script type="text/javascript" src="js/w2ui/w2ui-1.4.3.js"></script>
    <link href="js/toast/toastr.css" rel="stylesheet" type="text/css" />
    <script src="js/toast/toastr.js"></script>  
    
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/buttons.css">
    <link rel="stylesheet" href="css/layout.css">
</head>
<body style=" overflow-x: hidden;">
    <?php 
        $sql = "SELECT * FROM tbl_users WHERE   user_zone_chamcong = 1 AND user_status=1 AND user_username NOT IN ('sys','admin') ";
        $list_user_arr = db_loadList($sql); 
    ?>
    <script type="text/javascript">
        var mycol_user = [
            "user_order",
            "" ,
            "user_username",
            "user_password",
            "user_fullname",            
        ];
        $(function () {
            $('#grid1').w2grid({ 
                name: 'grid1', 
                //selectType: 'cell',
                recordHeight : 25,                        
                show: { 
                    toolbar: false,
                    footer: false,
                    toolbarSave: false,
                    selectColumn: false,
                    lineNumbers    : false,
                },
                multiSelect: false,
                columns: [                
                    { field: 'user_order', caption: '<img src="./images/plus.png" style="cursor:pointer;" onclick="ExcelAddRow_TaiKhoan()">', size: '25px', sortable: false, resizable: false,style:'text-align: center;background-color:#CFCFCF;',   editable: { type: 'text' } },  
                    { field: 'check', caption: '<img src="./images/Red.gif"  style="cursor:pointer;" onclick="delete_check_user();" >', size: '25px', sortable: true, resizable: true, style:'text-align: center;' },            

                    { field: 'user_username', caption:  '<div style="text-align: center;">TÀI KHOẢN</div>', size: '65px', sortable: true, resizable: true,style:'text-align: left;',  
                        editable: { type: 'text' } ,                               
                    },                         
                    { field: 'user_password', info: true, caption:  '<div style="text-align: center;">MẬT MÃ</div>', size: '60px', sortable: true, resizable: true,style:'text-align: center;',  
                        editable: { type: 'text' } ,            
                    },
                    { field: 'user_fullname', info: true, caption:  '<div style="text-align: center;">TÊN NV</div>', size: '80px', sortable: true, resizable: true,style:'text-align: center;',  
                        editable: { type: 'text' } ,            
                    },                            
                            
                ],
                onSelect: function (event) {
                    var recid  = (event.recid);                 
                    var record= w2ui.grid1.get(recid);        // id for select     
                    $('#ten_hien_thi2').html(record.user_username); 
                    w2ui['grid4'].load('kiosk_phanquyen.php?action=thiet_lap_quyen_phong_ban&user_id='+record.user_id );
                },
                onChange: function (event) {
                    // For create ajx edit DB  console.log(event);
                    var input_str  = (event.input_id);
                    var arr = input_str.split('_'); // grid1_grid1_edit_1_4

                    var record= w2ui.grid1.get(arr[3]);   
                    var user_id = record.user_id;                 //id for edit             
                    var value_new = (event.value_new);              // new value
                    var field = mycol_user[arr[4]];                      // field     
                    //console.log(field);
                    myExcelChange_TaiKhoan(field,value_new,user_id ); 

                },
                onClick: function(event) {
                    var recid  = (event.recid);                 
                    var record= w2ui.grid1.get(recid);        // id for select     
                },
                records: [
                    <?php
                        if(count($list_user_arr) > 0 ) {
                            $idx=0;  
                            foreach ($list_user_arr as $row) :
                                $idx++; $mark = ' style: {';
                                    $mark .="14 :'background-color: lightgreen;  color:red; '";
                                $mark .= ' }';
                                echo "
                                {   recid: ".$idx.",user_order: ".$idx.", user_id: ".$row['user_id'].", 
                                    'user_username': '".$row['user_username']."', 
                                    'user_password': '****',    
                                    'user_fullname': '".$row['user_fullname']."',                                            
                                    'check': '<input type=\"checkbox\" onclick=\"mycheck_user(".$idx.",".$row['user_id'].")\" >', 
                                    ".$mark."
                                },
                                ";
                            endforeach;

                        }    
                    ?>
                ]
            }); 

        });

        function deletecheckuserfunc(item, index) {
            w2ui.grid1.select(index, index);
            w2ui.grid1.delete(true);
            $.ajax({
                type: "POST",
                url: "kiosk_phanquyen.php",
                data: "action=delete_user&user_id="+item,
                success: function(msg){
                }
            });  
        }

        var input_check_user_arr = []; 
        function mycheck_user(idx,user_id) {
            if(input_check_user_arr[idx] == user_id ) {
                input_check_user_arr[idx] = 0;        
            } else {
                input_check_user_arr[idx] = user_id;
            }
        }

        function delete_check_user() {
            if(input_check_user_arr.length == 0 ) {
                alert('Vui lòng chọn ít nhất 1 tài khoản này.');
            } else {
                if(confirm('Bạn có muốn xoá tài khoản này?')) { 
                    input_check_user_arr.forEach(deletecheckuserfunc);
                    toastr.error('<font face="Arial" size="2">Đã xóa</font>');     
                    setTimeout(function(){ location.reload(); }, 2000);
                }
            }
        }

        function ExcelAddRow_TaiKhoan() {
            $.ajax({
                type: "POST",
                url: "kiosk_phanquyen.php",
                data: "action=add_user",
                success: function(msg) {
                    location.reload();
                }
            });
        }

        function myExcelChange_TaiKhoan(field, val,user_id) {
            $.ajax({
                type: "POST",
                url: "kiosk_phanquyen.php",
                data: "action=save_user&user_id="+user_id+'&field='+field+'&val='+val ,
                success: function(msg){
                }
            });  
        }
        
    $(function () {
        $('#grid4').w2grid({ 
            name: 'grid4', 
            url  : 'kiosk_phanquyen.php?action=thiet_lap_quyen_phong_ban',                     
            show: { 
                toolbar: false,
                footer: false,
                toolbarSave: false,
                selectColumn: false,
                lineNumbers    : false,
            },
            multiSelect: false,
            columns: [   
                { field: 'danhmuc_order', caption: '', size: '25px', sortable: false, resizable: false,style:'text-align: center;background-color:#CFCFCF;',   editable: { type: 'text' } },  
                { field: 'danhmuc_name', info: true, caption:  '<div style="text-align: left;">TÊN PHÒNG BAN </div>', size: '193px', sortable: true, resizable: true,style:'text-align: left;',  
                    render: function (record, index, col_index) {
                        var html = this.getCellValue(index, col_index);
                        return  html; 
                    } 
                },
                { field: 'check', caption: '', size: '25px', sortable: true, resizable: true, style:'text-align: center;' },            
            ],
            onSelect: function (event) {
                var recid  = (event.recid);                 
            },
            records: [
            ]
        }); 
    });
    
    function mycheck_quyenphongban(val, user_id ,danhmuc_id) {
   
        $.ajax({
            type: "POST",
            url: "kiosk_phanquyen.php",
            data: "action=save_quyen&perms_type=PHONGBAN&checked="+val+"&user_id="+user_id+'&name='+danhmuc_id ,
            success: function(msg){
                
            }
        });  
    }
   
    </script>
    <div style="background-color: white; height:1200px; text-align: left;padding-left: 10px; padding-top: -10px;">
        <table style="width:80%; text-align: center; ">
            <tr>
                <td style="width:25%; vertical-align: top; align:left;">
                    <u>TÀI KHOẢN </u>&nbsp;&nbsp;
                    <div id="grid1" style="width: 257px; height: 200px;margin-left: 0px;margin-top: 0px;margin-right: 5px !important"></div>
                    
                </td>            
                <td style="width:100%; vertical-align: top;text-align: left;">
                    <u style="color:blue">QUYỀN CHẤM CÔNG</u> <small>[<span id="ten_hien_thi2" style="color:red"></span>]</small>
                    <div id="grid4" style="width: 250px;height: 900px;"></div>
                </td>             

            </tr>    
        </table>
    </div>
    <br><br><br><br><br><br><br>
    
    