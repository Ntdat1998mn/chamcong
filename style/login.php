<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>[:: Quản lý chấm công ::]</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <link href="css/simple-line-icons.min.css" rel="stylesheet" type="text/css">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="css/bootstrap-switch.min.css" rel="stylesheet" type="text/css">
        <link href="css/morris.css" rel="stylesheet" type="text/css">
        <link href="css/components.min.css" rel="stylesheet" id="style_components" type="text/css">
        <link href="css/layout.min.css" rel="stylesheet" type="text/css">
        <link href="css/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color">
        <link href="fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="css/jquery.notific8.min.css"/>
        <link href="css/style.css" rel="stylesheet" type="text/css" id="style_color">
        <link href="css/mystyle.css" rel="stylesheet" type="text/css" id="style_color">
    </head>
    <!-- END HEAD -->
    <body>
    <div class="wallpaper-login">
        <div class="background-fff">
        <div class="logo"><a href=""><img src="images/logo.jpg"></a></div>
            <div class="form-login">
                <form action="./index.php" method="post" name="loginform"  id="loginform">
                <input type="hidden" name="login" value="" />
                <input type="hidden" name="redirect" value="<?php echo $redirect;?>" />	  
                    <?php
                        if ($AppUI->msg) {
                            echo '                             
                                <div class="note note-danger"  id="login_msg_empty">
                                    <p>'.$AppUI->getMsg().'</p>                                
                                </div> 
                            ';                                 
                            $AppUI->msg='';
                        } else {
                            echo '
                                <div class="note note-success"  id="login_msg_empty">
                                    <p>Vui lòng nhập tên truy cập</p>
                                </div>                            
                            ';
                        } 
                    ?>
                    <div class="form-body">
                        <div class="form-group">
                            <div class="bg-input">
                                <div class="first-name form-validate">
                                    <div class="input-icon right">
                                        <i class="icon fa"></i>
                                        <input name="username" type="text" id="username"   class="form-control" placeholder="Tên truy cập">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="form-group form-validate">
                            <div class="bg-input">
                                <div class="input-icon right">
                                    <i class="icon fa"></i>
                                    <input name="password" type="password" id="password" class="form-control"   placeholder="Mật mã">
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="form-actions">
                            <input type="submit" name="Submit" class="btn blue pull-right" value="Đăng nhập">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="page-footer" style="position:fixed; bottom:0; width:100%">
        <div class="page-footer-inner">
            <p>Sản phẩm của <a target="_blank" href="">IntelERP&#8482;</a> </p>
        </div>
    </div>
    <script src="js/jquery.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
    <script src="js/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="js/morris.min.js" type="text/javascript"></script>
    <script src="js/app.min.js" type="text/javascript"></script>
    <script src="js/dashboard.min.js" type="text/javascript"></script>
    <script src="js/layout.min.js" type="text/javascript"></script>
    <script src="js/demo.min.js" type="text/javascript"></script>
    <script src="js/quick-sidebar.min.js" type="text/javascript"></script>
    <!-- js tash 1 -->
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script src="js/metronic.js" type="text/javascript"></script>
    <script src="js/form-validation.js"></script>
    <!-- end js tash 1 -->
    <!-- js tash 2 -->
    <script src="js/jquery.notific8.min.js"></script>
    <script src="js/ui-notific8.js"></script>
    <!-- end js tash 2 -->
    </body>
</html>
<script>
    document.loginform.username.focus();
 </script>