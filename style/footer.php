                <!-- END CONTENT -->
            </div>
        </div>
   </div>
</div>    
    
    </div>

      <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <div class="page-footer">
           <div class="page-footer-inner">
              2023 © IntelERP&#8482; 
              <p></p>
           </div>
        </div>
        
            <!-- POPUP EDIT-->
            <div id="thewindowbackground_edit" class="thewindowbackground_edit"   onclick="closepopup();"></div> 
            <div id="popup_layer_edit" class="popup_layer_edit"  style="width:900px" >
                <span id="mypop_ajax_show">
                    <iframe  id="frame_transfer" src=""   style="margin-top: 0px; width: 100%; height: 420px; border:0; overflow-y: hidden; "></iframe>
                </span>
                <input type="hidden" id="popup_khuvuc" value='SG' > 
            </div>
            <!-- END POPUP EDIT-->
            
            <style>
                .thewindowbackground_edit_report {
                    display:none; width:100%; height:2500px;
                    background-color:#000000; position:absolute; left:0px; 
                    top: -100px; bottom: -100px;opacity: .6; filter: alpha(opacity=70); z-index:99999;
                }
                @media (max-width: 600px) {
                    .thewindowbackground_edit_report {
                        height:2500px;
                    }
                }
                .popup_layer_edit_report {
                    background-color:#fff; display:none; position: absolute;left: 0px;
                    top: 0px;width: 1200px; height:800px;  margin-left: 120px;margin-top: 10px; z-index:9999999;
                    border-radius:6px;
                }

                @media (max-width: 800px) {
                    .thewindowbackground_edit_report {
                        height:1700px;
                    }
                    .popup_layer_edit_report {
                        top: 150px;width: 400px; height:400px;  
                        margin-left: 0px;margin-top: 100px; z-index:9999999;
                    }
                }
                
            </style>    
            <style>
                .close-button {
                    cursor:pointer;
                    display: inline-block;
                    -webkit-box-sizing: content-box;
                    -moz-box-sizing: content-box;
                    box-sizing: content-box;
                    width: 0.75em;
                    height: 0.75em;
                    position: relative;
                    border: none;
                    -webkit-border-radius: 1em;
                    border-radius: 1em;
                    font: normal 4em/normal Arial, Helvetica, sans-serif;
                    color: rgba(0,0,0,1);
                    -o-text-overflow: clip;
                    text-overflow: clip;
                    background: #1abc9c;
                }

                .close-button::before {
                  display: inline-block;
                  -webkit-box-sizing: content-box;
                  -moz-box-sizing: content-box;
                  box-sizing: content-box;
                  width: 0.45em;
                  height: 0.1em;
                  position: absolute;
                  content: "";
                  top: 0.33em;
                  left: 0.155em;
                  border: none;
                  font: normal 100%/normal Arial, Helvetica, sans-serif;
                  color: rgba(0,0,0,1);
                  -o-text-overflow: clip;
                  text-overflow: clip;
                  background: #ffffff;
                  text-shadow: none;
                  -webkit-transform: rotateZ(45deg)   ;
                  transform: rotateZ(45deg)   ;
                }

                .close-button::after {
                  display: inline-block;
                  -webkit-box-sizing: content-box;
                  -moz-box-sizing: content-box;
                  box-sizing: content-box;
                  width: 0.45em;
                  height: 0.1em;
                  position: absolute;
                  content: "";
                  top: 0.33em;
                  left: 0.155em;
                  border: none;
                  font: normal 100%/normal Arial, Helvetica, sans-serif;
                  color: rgba(0,0,0,1);
                  -o-text-overflow: clip;
                  text-overflow: clip;
                  background: #ffffff;
                  text-shadow: none;
                  -webkit-transform: rotateZ(-45deg)   ;
                  transform: rotateZ(-45deg)   ;
                }
                </style> 
            
            <!-- POPUP EDIT-->
            <div id="thewindowbackground_edit_report" class="thewindowbackground_edit_report"   onclick="closepopup_report();"></div> 
            <div id="popup_layer_edit_report" class="popup_layer_edit_report"  style="width:1100px" >
                <div style="width:100%; margin-right:0px; text-align: right;margin-top:-20px;  " onclick="closepopup_report();">
                    <span  class="close-button" style="margin-right:-15px" ></span>
                </div>
                <iframe  id="frame_report" src=""   style="margin-top: 0px; width: 100%; height: 620px; border:0; overflow-y: hidden; "></iframe>
            </div>
            <!-- END POPUP EDIT-->
            
            
        <!-- END FOOTER -->
    </body>
</html>

<script>
    function closepopup_report() {
        document.getElementById("popup_layer_edit_report").style.display = 'none';
        document.getElementById("thewindowbackground_edit_report").style.display = 'none';
    } 
    $("body").addClass("page-sidebar-closed");
    $(".page-sidebar-menu").addClass("page-sidebar-menu-closed");
    
    var page_logo_count_click= 0;
    $('.page-logo').click(function() {
        page_logo_count_click = page_logo_count_click + 1;
        var num = (page_logo_count_click % 2) >> 0;
        if( num == 1  ) { 
            $('#pull-left').css ('left', '200px');
        } else {
            $('#pull-left').css ('left', '20px');            
        }   
    });
    
    
</script>

 