<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>[:: Quản lý chấm công  ::]</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <link href="css/simple-line-icons.min.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="css/bootstrap-switch.min.css" rel="stylesheet" type="text/css">
    <link href="css/morris.css" rel="stylesheet" type="text/css">
    <link href="css/components.min.css" rel="stylesheet" id="style_components" type="text/css">
    <link href="css/layout.min.css" rel="stylesheet" type="text/css">
    <link href="css/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color">
    <link href="fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="css/jquery.notific8.min.css"/>
    <link href="css/style.css" rel="stylesheet" type="text/css" id="style_color">
    <link href="css/mystyle.css" rel="stylesheet" type="text/css" id="style_color">    
</head>
 <!-- END FOOTER -->
    <script src="js/jquery.min.js" type="text/javascript"></script>
    
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
    <script src="js/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="js/morris.min.js" type="text/javascript"></script>
    <script src="js/app.min.js" type="text/javascript"></script>
    <script src="js/dashboard.min.js" type="text/javascript"></script>
    <script src="js/layout.min.js" type="text/javascript"></script>
    <script src="js/demo.min.js" type="text/javascript"></script>
    <script src="js/quick-sidebar.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script src="js/metronic.js" type="text/javascript"></script>
    <script src="js/form-validation.js"></script>
    <script src="js/jquery.notific8.min.js"></script>
    <script src="js/ui-notific8.js"></script>
        
<!--TOAST-->
<link href="js/toast/toastr.css" rel="stylesheet" type="text/css" />
<script src="js/toast/toastr.js"></script> 
<!-- POBOX-->
<link rel='stylesheet' href='js/popbox/popbox.css' type='text/css' media='screen' charset='utf-8'>
<script type='text/javascript' charset='utf-8' src='js/popbox/popbox.js'></script>
<script type='text/javascript'>
    $(document).ready(function(){
        $('.popbox').popbox({
            'open'          : '.popbox-open',
            'box'           : '.popbox-box',
            'arrow'         : '.popbox-arrow',
            'arrow-border'  : '.popbox-arrow-border',
            'close'         : '.popbox-close'
         });
    });
    var use_cookie_tab = 0;
</script>
<script type="text/javascript" src="js/tabpane.js"></script>
<link type="text/css" rel="stylesheet" href="css/tab.css" />
<script src="js/myjs.js"></script>   

<!--page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-content-white page-sidebar-closed-->
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
    <div class="page-header navbar navbar-fixed-top">
        <div class="page-header-inner ">
            <div class="page-logo">
                <img src="images/logo.png" alt="logo" class="logo-default"> 
                <div class="menu-toggler sidebar-toggler">
                    <span></span>
                </div>
            </div>
            <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
            <?php require "./style/top_menu.php"; ?>
         </div>
    </div>
    
           
    <!-- BEGIN HEADER & CONTENT DIVIDER -->
      <div class="clearfix"> </div>
      <!-- END HEADER & CONTENT DIVIDER -->
      <!-- BEGIN CONTAINER -->
      <div class="page-container">
        <?php require "./style/left_menu.php"; ?>      
    