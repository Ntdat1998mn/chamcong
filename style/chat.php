        <!-- BEGIN QUICK SIDEBAR -->
         <a href="javascript:;" class="page-quick-sidebar-toggler">
         <i class="icon-login"></i>
         </a>
         <div class="page-quick-sidebar-wrapper" data-close-on-body-click="false">
            <div class="page-quick-sidebar">
               <ul class="nav nav-tabs">
                  <li class="active">
                     <a href="javascript:;" data-target="#quick_sidebar_tab_1" data-toggle="tab"> Chat với nhau
                     </a>
                  </li>
               </ul>
               <div class="tab-content">
                  <div class="tab-pane active page-quick-sidebar-chat" id="quick_sidebar_tab_1">
                     <div class="page-quick-sidebar-list" style="position: relative; overflow: hidden; width: auto; height: 548px;">
                        <div class="page-quick-sidebar-chat-users" data-rail-color="#ddd" data-wrapper-class="page-quick-sidebar-list" data-height="548" data-initialized="1" style="overflow: hidden; width: auto; height: 548px;">
                           <h3 class="list-heading">Thành viên</h3>
                           <ul class="media-list list-items">
                              <li class="media">
                                 <div class="media-status">
                                    <span class="badge badge-success">8</span>
                                 </div>
                                 <img class="media-object" src="./uploads/profile-support.png" alt="...">
                                 <div class="media-body">
                                    <h4 class="media-heading">Support</h4>
                                    <div class="media-heading-sub"> Support Phần mềm bán hàng </div>
                                 </div>
                              </li>
                               
                           </ul>
                           
                        </div>
                        <div class="slimScrollBar" style="width: 7px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px; height: 343.991px; background: rgb(187, 187, 187);"></div>
                        <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; opacity: 0.2; z-index: 90; right: 1px; background: rgb(221, 221, 221);"></div>
                     </div>
                     <div class="page-quick-sidebar-item">
                        <div class="page-quick-sidebar-chat-user">
                           <div class="page-quick-sidebar-nav">
                              <a href="javascript:;" class="page-quick-sidebar-back-to-list">
                              <i class="icon-arrow-left"></i>Back</a>
                           </div>
                           <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 443px;">
                              <div class="page-quick-sidebar-chat-user-messages" data-height="443" data-initialized="1" style="overflow: hidden; width: auto; height: 443px;">
                                 <div class="post out">
                                    <img class="avatar" alt="" src="./uploads/profile-support.png">
                                    <div class="message">
                                       <span class="arrow"></span>
                                       <a href="javascript:;" class="name">Support</a>
                                       <span class="datetime">20:15</span>
                                       <span class="body"> When could you send me the report ? </span>
                                    </div>
                                 </div>
                                 <div class="post in">
                                    <img class="avatar" alt="" src="./uploads/profile-default.png">
                                    <div class="message">
                                       <span class="arrow"></span>
                                       <a href="javascript:;" class="name">Quản lý cửa hàng  </a>
                                       <span class="datetime">20:15</span>
                                       <span class="body"> Its almost done. I will be sending it shortly </span>
                                    </div>
                                 </div>
                                 <div class="post out">
                                    <img class="avatar" alt="" src="./uploads/profile-support.png">
                                    <div class="message">
                                       <span class="arrow"></span>
                                       <a href="javascript:;" class="name">Support</a>
                                       <span class="datetime">20:15</span>
                                       <span class="body"> Alright. Thanks! :) </span>
                                    </div>
                                 </div>
                                 <div class="post in">
                                    <img class="avatar" alt="" src="./uploads/profile-default.png">
                                    <div class="message">
                                       <span class="arrow"></span>
                                       <a href="javascript:;" class="name">Quản lý cửa hàng  </a>
                                       <span class="datetime">20:16</span>
                                       <span class="body"> You are most welcome. Sorry for the delay. </span>
                                    </div>
                                 </div>
                                 <div class="post out">
                                    <img class="avatar" alt="" src="./uploads/profile-support.png">
                                    <div class="message">
                                       <span class="arrow"></span>
                                       <a href="javascript:;" class="name">Support</a>
                                       <span class="datetime">20:17</span>
                                       <span class="body"> No probs. Just take your time :) </span>
                                    </div>
                                 </div>
                                 <div class="post in">
                                    <img class="avatar" alt="" src="./uploads/profile-default.png">
                                    <div class="message">
                                       <span class="arrow"></span>
                                       <a href="javascript:;" class="name">Quản lý cửa hàng  </a>
                                       <span class="datetime">20:40</span>
                                       <span class="body"> Alright. I just emailed it to you. </span>
                                    </div>
                                 </div>
                                 <div class="post out">
                                    <img class="avatar" alt="" src="./uploads/profile-support.png">
                                    <div class="message">
                                       <span class="arrow"></span>
                                       <a href="javascript:;" class="name">Support</a>
                                       <span class="datetime">20:17</span>
                                       <span class="body"> Great! Thanks. Will check it right away. </span>
                                    </div>
                                 </div>
                                 <div class="post in">
                                    <img class="avatar" alt="" src="./uploads/profile-default.png">
                                    <div class="message">
                                       <span class="arrow"></span>
                                       <a href="javascript:;" class="name">Quản lý cửa hàng  </a>
                                       <span class="datetime">20:40</span>
                                       <span class="body"> Please let me know if you have any comment. </span>
                                    </div>
                                 </div>
                                 <div class="post out">
                                    <img class="avatar" alt="" src="./uploads/profile-support.png">
                                    <div class="message">
                                       <span class="arrow"></span>
                                       <a href="javascript:;" class="name">Support</a>
                                       <span class="datetime">20:17</span>
                                       <span class="body"> Sure. I will check and buzz you if anything needs to be corrected. </span>
                                    </div>
                                 </div>
                              </div>
                              <div class="slimScrollBar" style="width: 7px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px; height: 290.309px; background: rgb(187, 187, 187);"></div>
                              <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; opacity: 0.2; z-index: 90; right: 1px; background: rgb(234, 234, 234);"></div>
                           </div>
                           <div class="page-quick-sidebar-chat-user-form">
                              <div class="input-group">
                                 <input type="text" class="form-control" placeholder="Type a message here...">
                                 <div class="input-group-btn">
                                    <button type="button" class="btn green">
                                    <i class="icon-paper-clip"></i>
                                    </button>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- END QUICK SIDEBAR -->
         <div class="scroll-to-top" style="display: block;">
            <i class="icon-arrow-up"></i>
         </div>
    