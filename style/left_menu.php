<div class="page-sidebar-wrapper" >
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
            <li class="sidebar-toggler-wrapper hide">
                <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                <div class="sidebar-toggler">
                    <span></span>
                </div>
            </li>
            <li class="nav-item">
                <a href="?m=home&a=dashboard" class="nav-link nav-toggle">
                    <i class="icon-layers"></i>
                    <span class="title">Bàn làm việc</span>
                    <span class=""></span>
                </a>

            </li>
            
            
        <?php 
            if( ($AppUI->user_username == 'sys' ) || ($AppUI->user_username == 'Nhân sự 2' ) ||  ($AppUI->user_username == 'admin' ) ) {
        ?>
            <li class="nav-item start active open" >
                <a href="#" class="nav-link nav-toggle">
                    <i class="icon-settings"></i>
                    <span class="title">Cấu hình hệ thống</span>
                    <span class="arrow open"></span>
                </a>
                <ul class="sub-menu">
                    
                    <li class="nav-item start active open">
                        <a href="?m=home&a=sysconf" class="nav-link ">
                            <i class="icon-social-dribbble"></i>
                            <span class="title">Cấu hình hệ thống</span>
                            <span class="selected"></span>
                        </a>
                    </li>
                                              
                </ul>
            </li>
        <?php } ?>    
        </ul>
    </div>
</div>

<div class="page-content-wrapper">
    <div class="page-content" style="min-height:1112px">
        <div class="row">
            <div class="content-vdl">
                <!-- CONTENT-->