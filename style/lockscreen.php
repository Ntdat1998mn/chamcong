<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>[:: Khoá màn hình ứng dụng ::]</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <link href="css/simple-line-icons.min.css" rel="stylesheet" type="text/css">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="css/bootstrap-switch.min.css" rel="stylesheet" type="text/css">
        <link href="css/morris.css" rel="stylesheet" type="text/css">
        <link href="css/components.min.css" rel="stylesheet" id="style_components" type="text/css">
        <link href="css/layout.min.css" rel="stylesheet" type="text/css">
        <link href="css/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color">
        <link href="fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="css/jquery.notific8.min.css"/>
        <link href="css/style.css" rel="stylesheet" type="text/css" id="style_color">
        <link href="css/mystyle.css" rel="stylesheet" type="text/css" id="style_color">
        <link href="css/lock.min.css" rel="stylesheet" type="text/css" id="style_color">
    </head>
    <!-- END HEAD -->
<?php
    $info_arr = db_loadList("SELECT * FROM tbl_users WHERE user_id=".$AppUI->user_id);
    $filename1 =  file_exists('./uploads/profile_'.$AppUI->user_id.'.jpg') ?  './uploads/profile_'.$AppUI->user_id.'.jpg' : './uploads/profile-default.png';
?>
 <body class="" style="background-color: rgba(35, 141, 193, 0.66) !important;">
    <div class="page-lock">
        <div class="page-logo"> </div>
        <div class="page-body">
            <div class="lock-head"> Locked </div>
            <div class="lock-body">
                <div class="lock-cont">
                    <div class="lock-item col-md-3">
                        <div class=" lock-avatar-block">
                            <img src="<?php echo $filename1; ?>" class="lock-avatar"> 
                        </div>
                    </div>
                    <div class="lock-item lock-item-full col-md-9"  >
                        <form class="lock-form " action="./index.php" method="post" name="loginform"  id="loginform">
                                <input type="hidden" name="login" value="" />
                                <input type="hidden" name="redirect" value="m=home&a=dashboard" />	  
                                 <input type="hidden" name="username" type="text" id="username"   value="<?php echo $AppUI->user_username; ?>" >
                                <?php
                                    if ($AppUI->msg) {
                                        echo ' <p>'.$AppUI->getMsg().'</p>   ';                                 
                                        $AppUI->msg='';
                                    } 
                                ?>        
                            <h4 style=" "><?php echo $info_arr[0]["user_longname"]; ?></h4>
                            <div class="form-group">
                                <input id="password" class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Mật mã" name="password" /> </div>
                            <div class="form-actions">
                                <div class="form-actions">
                                    <input type="submit" name="Submit" class="btn red uppercase" value="Đăng nhập">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="lock-bottom">
                <a href="?logout=-1">Không phải <?php echo $info_arr[0]["user_fullname"]; ?>?</a>
            </div>
        </div>
        <div class="page-footer-custom"> <?php echo date('Y'); ?> &copy; IntelERP&#8482;. </div>
    </div>
</body>   
<?php
    //session_unset();
    //session_destroy();
?>

    <script src="js/jquery.min.js" type="text/javascript"></script>
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
    <script src="js/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="js/morris.min.js" type="text/javascript"></script>
    <script src="js/app.min.js" type="text/javascript"></script>
    <script src="js/dashboard.min.js" type="text/javascript"></script>
    <script src="js/layout.min.js" type="text/javascript"></script>
    <script src="js/demo.min.js" type="text/javascript"></script>
    <script src="js/quick-sidebar.min.js" type="text/javascript"></script>
    <!-- js tash 1 -->
    <script type="text/javascript" src="js/jquery.validate.min.js"></script>
    <script src="js/metronic.js" type="text/javascript"></script>
    <script src="js/form-validation.js"></script>
    <!-- end js tash 1 -->
    <!-- js tash 2 -->
    <script src="js/jquery.notific8.min.js"></script>
    <script src="js/ui-notific8.js"></script>
    <!-- end js tash 2 -->
    </body>
</html>
<script>
    document.loginform.username.focus();
 </script>