<div class="top-menu">
        <?php	show_notice($a);?>
    <ul class="nav navbar-nav pull-right">
         
        <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                <i class="icon-bell"></i>
                <span class="badge badge-default"> 7 </span>
            </a>
            <ul class="dropdown-menu">
               <li class="external">
                  <h3>
                     <span class="bold">7 </span> tin nhắn
                  </h3>
                  <a href=" ">Xem hết</a>
               </li>
               <li>
                  <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 250px;">
                     <ul class="dropdown-menu-list scroller" style="height: 250px; overflow: hidden; width: auto;" data-handle-color="#637283" data-initialized="1">
                        <li>
                           <a href="javascript:;">
                           <span class="time">just now</span>
                           <span class="details">
                           <span class="label label-sm label-icon label-success">
                           <i class="fa fa-plus"></i>
                           </span> Support nhắn ... </span>
                           </a>
                        </li>
                     </ul>
                     <div class="slimScrollBar" style="width: 7px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px; background: rgb(99, 114, 131);"></div>
                     <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; opacity: 0.2; z-index: 90; right: 1px; background: rgb(234, 234, 234);"></div>
                  </div>
               </li>
            </ul>
        </li>
        <!--        <li class="dropdown dropdown-extended dropdown-inbox" id="header_inbox_bar">
                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <i class="icon-envelope-open"></i>
                        <span class="badge badge-default"> 4 </span>
                    </a>
                  <ul class="dropdown-menu">
                     <li class="external">
                        <h3>You have
                           <span class="bold">7 New</span> Messages
                        </h3>
                        <a href=" ">view all</a>
                     </li>
                     <li>
                        <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 275px;">
                           <ul class="dropdown-menu-list scroller" style="height: 275px; overflow: hidden; width: auto;" data-handle-color="#637283" data-initialized="1">
                              <li>
                                 <a href=" ">
                                 <span class="photo">
                                 <img src="./uploads/profile-default.png" class="img-circle" alt=""> </span>
                                 <span class="subject">
                                 <span class="from"> Lisa Wong </span>
                                 <span class="time">Just Now </span>
                                 </span>
                                 <span class="message"> Vivamus sed auctor nibh congue nibh. auctor nibh auctor nibh... </span>
                                 </a>
                              </li>
                           </ul>
                           <div class="slimScrollBar" style="width: 7px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px; background: rgb(99, 114, 131);"></div>
                           <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; opacity: 0.2; z-index: 90; right: 1px; background: rgb(234, 234, 234);">
                           </div>
                        </div>
                     </li>
                  </ul>
               </li>-->
        <!--        <li class="dropdown dropdown-extended dropdown-tasks" id="header_task_bar">
                  <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                  <i class="icon-calendar"></i>
                  <span class="badge badge-default"> 3 </span>
                  </a>
                  <ul class="dropdown-menu extended tasks">
                     <li class="external">
                        <h3>You have
                           <span class="bold">12 pending</span> tasks
                        </h3>
                        <a href="">view all</a>
                     </li>
                     <li>
                        <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 275px;">
                           <ul class="dropdown-menu-list scroller" style="height: 275px; overflow: hidden; width: auto;" data-handle-color="#637283" data-initialized="1">
                              <li>
                                 <a href="javascript:;">
                                 <span class="task">
                                 <span class="desc">New UI release</span>
                                 <span class="percent">38%</span>
                                 </span>
                                 <span class="progress progress-striped">
                                 <span style="width: 38%;" class="progress-bar progress-bar-important" aria-valuenow="18" aria-valuemin="0" aria-valuemax="100">
                                 <span class="sr-only">38% Complete</span>
                                 </span>
                                 </span>
                                 </a>
                              </li>
                           </ul>
                           <div class="slimScrollBar" style="width: 7px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px; background: rgb(99, 114, 131);"></div>
                           <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; opacity: 0.2; z-index: 90; right: 1px; background: rgb(234, 234, 234);"></div>
                        </div>
                     </li>
                  </ul>
               </li>-->

       <li class="dropdown dropdown-user">
          <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
            <?php
                $profile_filename =  file_exists('./uploads/profile_'.$AppUI->user_id.'.jpg') ?  './uploads/profile_'.$AppUI->user_id.'.jpg' : './uploads/profile-default.png';
            ?>
              
          <img alt="" class="img-circle" src="<?php echo $profile_filename; ?>">
          <span class="username username-hide-on-mobile"> Tài khoản </span>
          <i class="fa fa-angle-down"></i>
          </a>
          <ul class="dropdown-menu dropdown-menu-default">
             <li>
                <a href="?m=home&a=profile">
                <i class="icon-user"></i> Hồ sơ cá nhân </a>
             </li>
             <li>
                 <a href="#">
                <i class="icon-rocket"></i> Tác vụ
                <span class="badge badge-success"> 7 </span>
                </a>
             </li>
             <li class="divider"> </li>
             <li>
                <a href="?lockscreen=1">
                <i class="icon-lock"></i> Khoá màn hình </a>
             </li>
             <li>
                <a href="?logout=-1">
                <i class="icon-key"></i> Thoát chương trình </a>
             </li>
          </ul>
       </li>
       <li class="dropdown dropdown-quick-sidebar-toggler">
          <a href="javascript:;" class="dropdown-toggle">
          <i class="icon-logout"></i>
          </a>
       </li>
       <!-- END QUICK SIDEBAR TOGGLER -->
    </ul>
</div>