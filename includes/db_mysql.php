<?php /* $Id: db_mysql.php,v 1.10 2003/05/21 23:16:09 eddieajau Exp $ */
/*
	Based on Leo West's (west_leo@yahooREMOVEME.com):
	lib.DB
	Database abstract layer
	-----------------------
	MYSQL VERSION
	-----------------------
	A generic database layer providing a set of low to middle level functions
	originally written for WEBO project, see webo source for "real life" usages
*/
function db_connect( $host='localhost', $dbname, $user='root', $passwd='', $port='3306', $persist=false ) {
    $con=   @mysql_connect( "$host:$port", $user, $passwd );
            mysql_select_db( $dbname );
            mysql_set_charset('utf8',$con);
    return $con;
}



function db_error() {
  global $con;
	return mysql_error($con);
}

function db_errno() {
  global $con;
	return mysql_errno($con);
}

function db_insert_id() {
  global $con;
	return mysql_insert_id($con);
}

function db_exec( $sql ) {
  global $con;
	$cur = mysql_query( $sql, $con );
//	echo $sql."<BR>";
	if( !$cur ) {
		return false;
	}
	return $cur;
}

function db_free_result( $cur ) {
	mysql_free_result( $cur );
}

function db_num_rows( $qid ) {
	return mysql_num_rows( $qid );
}

function db_fetch_row( $cur ) {
	return mysql_fetch_row( $cur );
}

function db_fetch_assoc( $cur ) {
	return mysql_fetch_assoc( $cur );
}

function db_fetch_array( $cur  ) {
	return mysql_fetch_array( $cur );
}

function db_fetch_object( $cur  ) {
	return mysql_fetch_object( $cur );
}

function db_escape( $str ) {
	return mysql_real_escape_string( $str );
}

function db_version() {
  global $con;
	if( ($cur = mysql_query( "SELECT VERSION()", $con )) ) {
		$row =  mysql_fetch_row( $cur );
		mysql_free_result( $cur );
		return $row[0];
	} else {
		return 0;
	}
}

function db_unix2dateTime( $time ) {
	// converts a unix time stamp to the default date format
	return $time > 0 ? date("Y-m-d H:i:s", $time) : null;
}

function db_dateTime2unix( $time ) {
	if ($time == '0000-00-00 00:00:00') {
		return -1;
	}
	if( ! preg_match( "/^(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})(.?)$/", $time, $a ) ) {
		return -1;
	} else {
		return mktime( $a[4], $a[5], $a[6], $a[2], $a[3], $a[1] );
	}
}
?>