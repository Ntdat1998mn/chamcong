<?php /* INCLUDES $Id: main_functions.php,v 1.53 2003/09/09 22:58:55 ajdonnison Exp $ */
##
## Global General Purpose Functions
##

$CR = "\n";
define('SECONDS_PER_DAY', 60 * 60 * 24);

##
## Returns the best color based on a background color (x is cross-over)
##
function bestColor( $bg, $lt='#ffffff', $dk='#000000' ) {
// cross-over color = x
	$x = 128;
	$r = hexdec( substr( $bg, 0, 2 ) );
	$g = hexdec( substr( $bg, 2, 2 ) );
	$b = hexdec( substr( $bg, 4, 2 ) );

	if ($r < $x && $g < $x || $r < $x && $b < $x || $b < $x && $g < $x) {
		return $lt;
	} else {
		return $dk;
	}
}

##
## returns a select box based on an key,value array where selected is based on key
##
function arraySelect( &$arr, $select_name, $select_attribs, $selected, $translate=true ) {
	GLOBAL $AppUI;
	reset( $arr );
	$s = "\n<select name=\"$select_name\" $select_attribs >";
	foreach ($arr as $k => $v ) {
		if ($translate) {
			$v = @$AppUI->_( $v );
		}
		$s .= "\n\t<option value=\"".$k."\"".($k == $selected ? " selected=\"selected\"" : '').">" . dPformSafe( $v ) . "</option>";
	}
	$s .= "\n</select>\n";
	return $s;
}

function arraySelectColor( &$arr, $select_name, $select_attribs, $selected, $translate=true ) {
	GLOBAL $AppUI;
	reset( $arr );
	$s = "\n<select name=\"$select_name\" $select_attribs >";
	foreach ($arr as $k => $v ) {
		if ($translate) {
			$v = @$AppUI->_( $v );
		}
                $data_color = strtolower(substr(dPformSafe( $v ),0,2));
		$s .= "\n\t<option class=\"".$data_color."Text\" value=\"".$k."\"".($k == $selected ? " selected=\"selected\"" : '').">" . dPformSafe( $v ) . "</option>";
	}
	$s .= "\n</select>\n";
	return $s;
}


##
## returns a select box based on an key,value array where selected is based on key
##
function arraySelectTree( &$arr, $select_name, $select_attribs, $selected, $translate=true ) {
	GLOBAL $AppUI;
	reset( $arr );

	$children = array();
	// first pass - collect children
	foreach ($arr as $k => $v ) {
		$id = $v[0];
		$pt = $v[2];
		$list = @$children[$pt] ? $children[$pt] : array();
		array_push($list, $v);
	    $children[$pt] = $list;
	}
	$list = tree_recurse($arr[0][2], '', array(), $children);
	return arraySelect( $list, $select_name, $select_attribs, $selected, $translate );
}

function tree_recurse($id, $indent, $list, $children) {
	if (@$children[$id]) {
		foreach ($children[$id] as $v) {
			$id = $v[0];
			$txt = $v[1];
			$pt = $v[2];
			$list[$id] = "$indent $txt";
			$list = tree_recurse($id, "$indent--", $list, $children);
		}
	}
	return $list;
}

##
## Merges arrays maintaining/overwriting shared numeric indicees
##
function arrayMerge( $a1, $a2 ) {
	foreach ($a2 as $k => $v) {
		$a1[$k] = $v;
	}
	return $a1;
}

##
## breadCrumbs - show a colon separated list of bread crumbs
## array is in the form url => title
##
function breadCrumbs( &$arr ) {
	GLOBAL $AppUI;
	$crumbs = array();
	foreach ($arr as $k => $v) {
		$crumbs[] = "<a href=\"$k\">".$AppUI->_( $v )."</a>";
	}
	return implode( ' <strong>:</strong> ', $crumbs );
}
##
## generate link for context help -- old version
##
function contextHelp( $title, $link='' ) {
	return dPcontextHelp( $title, $link );
}

function dPcontextHelp( $title, $link='' ) {
	global $AppUI;
	return "<a href=\"#$link\" onClick=\"javascript:window.open('?m=help&dialog=1&hid=$link', 'contexthelp', 'width=400, height=400, left=50, top=50, scrollbars=yes, resizable=yes')\">".$AppUI->_($title)."</a>";
}

##
## displays the configuration array of a module for informational purposes
##
function dPshowModuleConfig( $config ) {
	GLOBAL $AppUI;
	$s = '<table cellspacing="2" cellpadding="2" border="0" class="std" width="50%">';
	$s .= '<tr><th colspan="2">'.$AppUI->_( 'Module Configuration' ).'</th></tr>';
	foreach ($config as $k => $v) {
		$s .= '<tr><td width="50%">'.$AppUI->_( $k ).'</td><td width="50%" class="hilite">'.$AppUI->_( $v ).'</td></tr>';
	}
	$s .= '</table>';
	return ($s);
}

/**
 *	Function to recussively find an image in a number of places
 *	@param string The name of the image
 *	@param string Optional name of the current module
 */
function findImage( $name, $module=null ) {
// uistyle must be declared globally
	global $AppUI, $uistyle;

	if (file_exists( "{$AppUI->cfg['root_dir']}/style/$uistyle/images/$name" )) {
		return "./style/$uistyle/images/$name";
	} else if ($module && file_exists( "{$AppUI->cfg['root_dir']}/modules/$module/images/$name" )) {
		return "./modules/$module/images/$name";
	} else if (file_exists( "{$AppUI->cfg['root_dir']}/images/icons/$name" )) {
		return "./images/icons/$name";
	} else if (file_exists( "{$AppUI->cfg['root_dir']}/images/obj/$name" )) {
		return "./images/obj/$name";
	} else {
		return "./images/$name";
	}
}

/**
 *	Workaround removed due to problems in Opera and other issues
 *	with IE6.
 *	Workaround to display png images with alpha-transparency in IE6.0
 *	@param string The name of the image
 *	@param string The image width
 *	@param string The image height
 *	@param string The alt text for the image
 */
function showImage( $src, $wid='', $hgt='', $alt='' ) {
	/*
	if (strpos( $src, '.png' ) > 0 && strpos( $_SERVER['HTTP_USER_AGENT'], 'MSIE 6.0' ) !== false) {
		return "<div style=\"height:{$hgt}px; width:{$wid}px; filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='$src', sizingMethod='scale');\" ></div>";
	} else {
	*/
		return "<img src=\"$src\" width=\"$wid\" height=\"$hgt\" alt=\"$alt\" border=\"0\" />";
	// }
}

#
# function to return a default value if a variable is not set
#

function defVal($var, $def) {
	return isset($var) ? $var : $def;
}

/**
* Utility function to return a value from a named array or a specified default
*/
function getParam( &$arr, $name, $def=null ) {
	return isset( $arr[$name] ) ? $arr[$name] : $def;
}

#
# add history entries for tracking changes
#

function addHistory( $description, $project_id = 0, $module_id = 0) {
	global $AppUI;
	/*
	 * TODO:
	 * 1) description should be something like:
	 * 		command(arg1, arg2...)
	 *  for example:
	 * 		new_forum('Forum Name', 'URL')
	 *
	 * This way, the history module will be able to display descriptions
	 * using locale definitions:
	 * 		"new_forum" -> "New forum '%s' was created" -> "Se ha creado un nuevo foro llamado '%s'"
	 *
	 * 2) project_id and module_id should be provided in order to filter history entries
	 *
	 */
	if(!$AppUI->cfg['log_changes']) return;
	$description = str_replace("'", "\'", $description);
	$hsql = "select * from modules where mod_name = 'History' and mod_active = 1";
	$qid = db_exec($hsql);

	if (! $qid || db_num_rows($qid) == 0) {
	  $AppUI->setMsg("History module is not loaded, but your config file has requested that changes be logged.  You must either change the config file or install and activate the history module to log changes.", UI_MSG_ALERT);
	  return;
	}

	$psql =	"INSERT INTO history " .
			"( history_description, history_user, history_date ) " .
	  		" VALUES ( '$description', " . $AppUI->user_id . ", now() )";
	db_exec($psql);
	echo db_error();
}

##
## Looks up a value from the SYSVALS table
##
/*function getSysVal( $title ) {
	$sql = "
	SELECT id, name
	FROM references
	WHERE type = '$title'
	ORDER by id
	";
	$result = db_loadHashList( $sql);
	return $result;
}*/
function getSysVal( $title ) {
	$sql = "
	SELECT syskey_type, syskey_sep1, syskey_sep2, sysval_value
	FROM sysvals,syskeys
	WHERE sysval_title = '$title'
		AND syskey_id = sysval_key_id
	";
	db_loadHash( $sql, $row );
// type 0 = list
	$sep1 = $row['syskey_sep1'];	// item separator
	$sep2 = $row['syskey_sep2'];	// alias separator

	// A bit of magic to handle newlines and returns as separators
	// Missing sep1 is treated as a newline.
	if (!isset($sep1))
	  $sep1 = "\n";
	if ($sep1 == "\\n")
	  $sep1 = "\n";
	if ($sep1 == "\\r")
	  $sep1 = "\r";

	$temp = explode( $sep1, $row['sysval_value'] );
	$arr = array();
	// We use trim() to make sure a numeric that has spaces
	// is properly treated as a numeric
	foreach ($temp as $item) {
		if($item) {
			$temp2 = explode( $sep2, $item );
			if (isset( $temp2[1] )) {
				$arr[trim($temp2[0])] = $temp2[1];
			} else {
				$arr[trim($temp2[0])] = $temp2[0];
			}
		}
	}
	return $arr;
}
function dPformatDuration($x) {
    global $dPconfig;
    global $AppUI;
    $dur_day = floor($x / $dPconfig['daily_working_hours']);
    //$dur_hour = fmod($x, $dPconfig['daily_working_hours']);
    $dur_hour = $x - $dur_day*$dPconfig['daily_working_hours'];
    $str = '';
    if ($dur_day > 1) {
        $str .= $dur_day .' '. $AppUI->_('days'). ' ';
    } elseif ($dur_day == 1) {
        $str .= $dur_day .' '. $AppUI->_('day'). ' ';
    }

    if ($dur_hour > 1 ) {
        $str .= $dur_hour .' '. $AppUI->_('hours');
    } elseif ($dur_hour > 0 and $dur_hour <= 1) {
        $str .= $dur_hour .' '. $AppUI->_('hour');
    }

    if ($str == '') {
        $str = $AppUI->_("n/a");
    }

    return $str;

}

/**
*/
function dPsetMicroTime() {
	global $microTimeSet;
	list($usec, $sec) = explode(" ",microtime());
	$microTimeSet = (float)$usec + (float)$sec;
}

/**
*/
function dPgetMicroDiff() {
	global $microTimeSet;
	$mt = $microTimeSet;
	dPsetMicroTime();
	return sprintf( "%.3f", $microTimeSet - $mt );
}

/**
* Make text safe to output into double-quote enclosed attirbutes of an HTML tag
*/
function dPformSafe( $txt, $deslash=false ) {
	if (is_object( $txt )) {
		foreach (get_object_vars($txt) as $k => $v) {
			if ($deslash) {
				$obj->$k = htmlspecialchars( stripslashes( $v ) );
			} else {
				$obj->$k = htmlspecialchars( $v );
			}
		}
	} else if (is_array( $txt )) {
		foreach ($txt as $k=>$v) {
			if ($deslash) {
				$txt[$k] = htmlspecialchars( stripslashes( $v ) );
			} else {
				$txt[$k] = htmlspecialchars( $v );
			}
		}
	} else {
		if ($deslash) {
			$txt = htmlspecialchars( stripslashes( $txt ) );
		} else {
			$txt = htmlspecialchars( $txt );
		}
	}
	return $txt;
}
function get_param($ParamName){
  global $HTTP_POST_VARS;
  global $HTTP_GET_VARS;

  $ParamValue = "";
  if(isset($_POST[$ParamName]))
    $ParamValue = $_POST[$ParamName];
  else if(isset($_GET[$ParamName]))
    $ParamValue = $_GET[$ParamName];

  return $ParamValue;
}

function convert2days( $durn, $units ) {
	global $AppUI;
	switch ($units) {
	case 0:
		return $durn / $AppUI->cfg['daily_working_hours'];
		break;
	case 24:
		return $durn;
	}
}

function formatTime($uts) {
	global $AppUI;
	$date = new CDate();
	$date->setDate($uts, DATE_FORMAT_UNIXTIME);	
	return $date->format( $AppUI->getPref('SHDATEFORMAT') );
}

// Modify by Huy Canh
function tosql($value, $type="Text")
{
  if($value == "")
  {
    return "";
  }
  else
  {
    if($type == "Number")
      return doubleval($value);
    else
    {
      if(get_magic_quotes_gpc() == 0)
      {
        $value = str_replace("'","''",$value);
        $value = str_replace("\\","\\\\",$value);
      }
      else
        {
          $value = str_replace("\\'","''",$value);
          $value = str_replace("\\\"","\"",$value);
        }
      return  $value;
     }
   }
} 

//showdate
function show_dates($sdate,$my_format,$day_name,$month_name,$year_name, $select_attribs=null)
	{
            if($my_format == "dd-mm-yyyy"){
                $selected_day = substr($sdate,0,2);
                $selected_month = substr($sdate,3,2);
                $selected_year = substr($sdate,6,4);
            }
            if($my_format == "yyyy-mm-dd"){
                    $selected_day = substr($sdate,8,2);
                    $selected_month = substr($sdate,5,2);
                    $selected_year = substr($sdate,0,4);
            }
            if($sdate == "first"){
                    $selected_day = "01";
                    $selected_month = date(n);
                    $selected_year = date(Y);
            }

            $date_str = "<select id=\"" .$day_name. "\" $select_attribs name=\"" .$day_name. "\">";
            for ($i=1;$i<=31;$i++){
                    $values = "";
                    if($i < 10) $values = "0" . $i;
                    else $values = $i;
                    $selected="";
                    if ($values == $selected_day){
                            $selected = "SELECTED";
                    }
                    $date_str .= "<option value='" .$values. "' " .$selected. ">" .$values. "</option>";
            }
            $date_str .= "</select>";

            $month_str = "<select id=\"" .$month_name. "\" $select_attribs name=\"" .$month_name. "\">";
            for ($i=1;$i<=12;$i++){
                    $values = "";
                    if($i < 10) $values = "0" . $i;
                    else $values = $i;
                    $selected="";
                    if ($values == $selected_month){
                            $selected = "SELECTED";
                    }
                    $month_str .= "<option value='" .$values. "' " .$selected. ">" .$values. "</option>";
            }
            $month_str .= "</select>";

            $year_str = "<select id=\"" .$year_name. "\" $select_attribs name=\"" .$year_name. "\">";
            for ($i=2016;$i<=2030;$i++){
                    $selected="";
                    if ($i == $selected_year){
                            $selected = "SELECTED";
                    }
                    $year_str .= "<option value='" .$i. "' " .$selected. ">" .$i. "</option>";
            }
            $year_str .= "</select>";

            return $date_str ."/". $month_str ."/". $year_str;
	}
function show_months($sdate,$my_format,$month_name,$year_name, $select_attribs=null)
	{
		$selected_month = get_month($sdate);
		$selected_year = get_year($sdate);
		if($my_format == "mm-yyyy"){
			$selected_month = substr($sdate,0,2);
			$selected_year = substr($sdate,3,4);
		}
	$month_str = "\n<font face=\" arial\"><select id=\"$month_name\" name=\"$month_name\" $select_attribs style=\"font-family: Arial, Helvetica, sans-serif; font-size: 10pt;\">";
//		$month_str = "<select name=\"" .$month_name. "\">";
		for ($i=1;$i<=12;$i++){
			$values = "";
			if($i < 10) $values = "0" . $i;
			else $values = $i;
			$selected="";
			if ($values == $selected_month){
				$selected = "SELECTED";
			}
			$month_str .= "<option value='" .$values. "' " .$selected. ">" .$values. "</option>";
		}
		$month_str .= "\n</select></font>\n";

	$year_str = "\n<font face=\" arial\"><select id=\"$year_name\" name=\"$year_name\" $select_attribs style=\"font-family: Arial, Helvetica, sans-serif; font-size: 10pt;\">";
	//	$year_str = "<select name=\"" .$year_name. "\">";
		for ($i=2016;$i<=2030;$i++){
			$selected="";
			if ($i == $selected_year){
				$selected = "SELECTED";
			}
			$year_str .= "<option value='" .$i. "' " .$selected. ">" .$i. "</option>";
		}
		$year_str .= "\n</select></font>\n";
//	$s .= "\n</select></font>\n";

		return $month_str ."-". $year_str;
	}

function show_years( $year_name, $selected_year, $select_attribs) {
    $year_str = "\n<font face=\" arial\"><select name=\"$year_name\" id=\"$year_name\"  $select_attribs style=\"font-family: Arial, Helvetica, sans-serif; font-size: 10pt;\">";
        for ($i=2017;$i<=2030;$i++){
            $selected="";
            if ($i == $selected_year){
                $selected = "SELECTED";
            }
            $year_str .= "<option value='" .$i. "' " .$selected. ">" .$i. "</option>";
        }
        $year_str .= "\n</select></font>\n";
        return $year_str;
}       

function get_day($string_time) {
	$date_converted_string = strtotime($string_time); 
	$today = getdate($date_converted_string); 
	$mday = $today['mday']; return $mday; 
}
function get_month($string_time) {
	$date_converted_string = strtotime($string_time); 
	$today = getdate($date_converted_string); 
	$month = $today['mon']; return $month; 
}
function get_year($string_time) {
	$date_converted_string = strtotime($string_time); 
	$today = getdate($date_converted_string); 
	$year = $today['year']; return $year; 
}



	function show_msg( $msg ) { 
  	
	  	print'<style type="text/css">
	  	.resultmsg { 
		    background-color:#f4ebc8;
			border:1px solid #cbaf81;
			} 
		</style>
	  		<table cellpadding="2" cellspacing="0" border="0" width="100%"  class="resultmsg">
				<tr>
					<td height="20" bgcolor="#A7C3F7" class="chuthuong" ><b>K&#7871;t qu&#7843;</b></td>
				</tr><tr>
					<tr><td>
						<table width="100%" border="0" cellpadding="2" cellspacing="0">
								<tr>
								    <td width="15%"   align="left"  class="redsmall">
		 								'.$msg.'
									</td>
							    </tr>
						</table>
					</td></tr>
				</table>';
		
		}	

	function  reset_saveplace() {
		global $AppUI;
		$AppUI->storeplace= array(
			);
	}	
	function delete_saveplace_linkid($nlink) {
		global $AppUI;
		$AppUI->storeplace[$nlink]= '';
	}
	function load_saveplace() {
		global $AppUI;
		//echo '<div class="quickLink">';
		//	echo '<a href="?m=home&a=search">Tìm</a>&gt;';				 	
		//	foreach ($AppUI->storeplace as $nlink=>$npage): 
		//		if($nlink=='') {continue;}
		//		echo '<a href="'.$nlink.'" name="">'.$npage.'</a>&gt;';
		//	endforeach;
		//echo '</div>';
//                 echo '<a href="?m=home&a=search">Tìm</a>&gt;';
                echo '<a href="?m=home&a=dashboard">Bàn làm việc</a>&gt;';
                if( isset($AppUI->storeplace) ) { 
                    foreach ($AppUI->storeplace as $nlink=>$npage): 
                        if($nlink=='') {continue;}
                        echo '<a href="'.$nlink.'" name="">'.$npage.'</a>&gt;';
                    endforeach;
                }
	}	

	function  saveplace_to_session($nlink,$npage) {
		global $AppUI;
		$AppUI->storeplace[$nlink]= $npage;
	}	
	
 	function date_vn_show($sdate, $format='abc'){
 		if($format=='veryshort') {
	 		return  substr($sdate,8,2).'/'.substr($sdate,5,2); 
 		} else  if($format=='short') {
	 		return  substr($sdate,8,2).'/'.substr($sdate,5,2).'/'.substr($sdate,0,4); 
 		} else if($format=='giophut') {
	 		return  substr($sdate,11,5); 
		} else if($format=='newline') {
	 		return  substr($sdate,8,2).'/'.substr($sdate,5,2) . "<br>" . substr($sdate,11,5) ; 	 		
	 	} else if($format=='giophuttruoc') {
	 		return  substr($sdate,11,5) . " " . substr($sdate,8,2).'/'.substr($sdate,5,2) ; 	 		
	 	} else {
	 	//	2012-07-16 12:21:35
	 		return  substr($sdate,8,2).'/'.substr($sdate,5,2) . " " . substr($sdate,11,5) ; 
	 	}	
 	}	
 	
 	
 	function show_hours($sdate,$hour_name,$minute_name, $select_attribs='')
	{
            if($sdate=='') {
                $selected_hour   	= '17';
                $selected_minute 	= '00';
            } else {
                $selected_minute    = substr($sdate,-5,2);
                $selected_hour      = substr($sdate,-8,2);
            }
 	 
	 
	$hour_str = "\n<select id=\"$hour_name\"  name=\"$hour_name\" $select_attribs  >";
		for ($i=0;$i<=23;$i++){
			$values = "";
			if($i < 10) $values = "0" . $i;
			else $values = $i;
			$selected="";
			if ($values == $selected_hour){
				$selected = "SELECTED";
			}
			$hour_str .= "<option value='" .$values. "' " .$selected. ">" .$values. " </option>";
		}
		$hour_str .= "\n</select></font>\n";

 	$min_str = "\n<select id=\"$minute_name\" name=\"$minute_name\" $select_attribs  >";
		$min_arr = array(0,5,10,15,20,25,30,35,40,45,50,55);
		foreach ($min_arr as $values){
			$selected="";
			if ($values == $selected_minute){
				$selected = "SELECTED";
			}
			$min_str .= "<option value='" .$values. "' " .$selected. ">" .$values. " </option>";
		}
		$min_str .= "\n</select></font>\n";

		return $hour_str .":". $min_str;
	}
	
	
 
	
	function online_user($show='notshow') {
		global $AppUI;	
		$s_username = 	$AppUI->user_username;  // Bien s_id         
		$time 		= 	time();            		// Lay thoi gian hien tai  
		$time_secs 	=	1800;        			// Thoi gian tinh bang seconds test tren localhost thi cho no bang 3 seconds de nhanh thay ket qua, chạy trên host thì để 900 = 15 phút là vừa  
		$time_out 	= 	$time - $time_secs;    	// Lay thoi gian hien tai     

		db_exec("DELETE FROM onlines WHERE s_time < '$time_out'");                			 
		db_exec("DELETE FROM onlines WHERE s_username = '$s_username'");              		 
		db_exec("INSERT INTO onlines (s_username, s_time) VALUES ('$s_username', '$time')");   
		db_exec("DELETE FROM onlines WHERE s_username = 'sys'");                			 
                
		if($show=='show') {
			$user_online = db_loadList("SELECT * FROM onlines");
			echo "<b><u>Online:</u><font color='blue' size='1'> ";
			$ind=0;
			foreach ($user_online as $row):
				$ind++;
				echo $row['s_username'].';'; 
				if( $ind%15 == 0 ) echo "<br>";
			endforeach;
			echo "</b></font>";
		}	
	}
        
      
?>
<?php function show_notice($a='') { 
    $notice_arr = db_loadlist("SELECT notice_text FROM tbl_notice");
    if($notice_arr[0]['notice_text']) {
?>      
    <ul  class="pull-left" id="pull-left">
        <span id="date_time"></span><script type="text/javascript">window.onload = date_time('date_time');</script>
        <label class="btn_task" onclick="fresh_all();"> Làm mới</label>
        <label class="marquee-title"> 
            <marquee onmouseover="this.stop();" onmouseout="this.start();" id="marqueetxt" >
            <?=$notice_arr[0]["notice_text"]?>      </marquee>
        </label>            
        <label class="btn_task"> Tác vụ</label>
        <?php if($a=='projects') { ?> 
            <label class="btn_task left_5px" onclick="AddIt()">Lập báo giá mới</label>
        <?php } ?>
    </ul>       
<?php }  
}

 function vn_str_filter ($str){ 
    $unicode = array( 
    'a'=>'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ', 
    'd'=>'đ', 
    'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ', 
    'i'=>'í|ì|ỉ|ĩ|ị', 
    'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ', 
    'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự', 
    'y'=>'ý|ỳ|ỷ|ỹ|ỵ', 
    'A'=>'Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ', 
    'D'=>'Đ', 
    'E'=>'É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ', 
    'I'=>'Í|Ì|Ỉ|Ĩ|Ị', 
    'O'=>'Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ', 
    'U'=>'Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự', 
    'Y'=>'Ý|Ỳ|Ỷ|Ỹ|Ỵ', ); 

    foreach($unicode as $nonUnicode=>$uni){ 
            $str = preg_replace("/($uni)/i", $nonUnicode, $str); 
    } 
    return $str; 
}       
?>