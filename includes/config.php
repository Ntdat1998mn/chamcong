<?php
$config['dbtype'] = "mysql";

$config['dbhost'] = "localhost";
$config['dbname'] = "namanh";
$config['dbuser'] = "root";
$config['dbpass'] = "1234";

$config['dbpersist'] = false;
$config['check_legacy_password'] = false;
$config['host_locale'] = "en";
$config['currency_symbol'] = "$";
$config['host_style'] = "default";

// local settings [DEFAULT example WINDOWS]
$config['root_dir'] = "www/html/namanh/chamcong/";  // No trailing slash
$config['base_url'] = "http://localhost/namanh/chamcong/";

// enable if you want to log changes using the history module
$config['log_changes'] = false;

// disable if you want to be able to leave start or end dates empty
$config['check_stat_dates'] = true;

// warn when a translation is not found (for developers and tranlators)
$config['locale_warn'] = false;
$config['locale_alert'] = '^';

// set debug = true to help analyse errors
$config['debug'] = true;
