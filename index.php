<?php
$config = array();
require_once( "./includes/config.php" );
require_once( "./classes/ui.class.php" );
require_once( "./includes/main_functions.php" );

session_name( 'QlyChamCong' );
if (get_cfg_var( 'session.auto_start' ) > 0) {
    session_write_close();
}
session_start();
if (!isset( $_SESSION['AppUI'] ) || isset($_GET['logout'])   ) {
    $_SESSION['AppUI'] = new CAppUI();
}
$AppUI =& $_SESSION['AppUI'];
$AppUI->setConfig( $config );
$AppUI->checkStyle();
require_once( "./includes/db_connect.php" );
$AppUI->doLogin();
//echo $_POST['username']); exit;
if (isset($_POST['username'])) {
    $username 	= getParam( $_POST, 'username', '' );
    $password 	= getParam( $_POST, 'password', '' );
    $txtPassSec     = getParam( $_POST, 'txtPassSec', '' );
    $redirect       = getParam( $_REQUEST, 'redirect', '' );

    $ok = $AppUI->login( $username, $password, $txtPassSec );
    if (!$ok) {
        $AppUI->setMsg( 'Mật mã truy cập không đúng' );
    }
  
    $AppUI->redirect( "$redirect" ,"", "index");
}

$m = getParam( $_GET, 'm', 'home' );
$a = '';
$u = '';

// check if we are logged in
if ($AppUI->doLogin()) {
    $redirect = @$_SERVER['QUERY_STRING'];
    if (strpos( $redirect, 'logout' ) !== false) {
        $redirect = '';
    }
    require "./style/login.php";
    session_unset();
    session_destroy();
    exit;
}

    $lockscreen = getParam( $_GET, 'lockscreen', '0' );
    if (@$lockscreen == 1) {
        $_SESSION['lockscreen'] = 1;
    }
    if(@$_SESSION['lockscreen'] == 1) {
        require "./style/lockscreen.php";
        exit;
    }

$m = getParam( $_GET, 'm', 'home' );
$u = getParam( $_GET, 'u', '' );
$a = getParam( $_GET, 'a', 'dashboard' );



ob_start();

@include_once( "./locales/$AppUI->user_locale/locales.php" );
@include_once( "./locales/core.php" );
setlocale( LC_TIME, $AppUI->user_locale );

// @include_once( "./modules/$m/$m.class.php" );

if (isset( $_REQUEST["dosql"]) ) {
    require ("./modules/$m/". $_REQUEST["dosql"] . ".php");
}

require "./style/header.php";
require "./modules/$m/"   . "$a.php"; 
require "./style/chat.php";
require "./style/footer.php";

ob_end_flush();
?>
