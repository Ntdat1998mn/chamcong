<?php error_reporting (E_ALL ^ E_NOTICE); ?>
<?php
    $config = array();
    require_once( "./includes/config.php" );
    require_once( "./classes/ui.class.php" );
    require_once( "./includes/main_functions.php" );
    date_default_timezone_set("Asia/Bangkok");
    session_name( 'QlyChamCong' );
    if (get_cfg_var( 'session.auto_start' ) > 0) {
            session_write_close();
    }
    session_start();
    // check if session has previously been initialised
    if (!isset( $_SESSION['AppUI'] ) || isset($_GET['logout'])) {
        $_SESSION['AppUI'] = new CAppUI();
    }
    $AppUI =& $_SESSION['AppUI'];

    $AppUI->setConfig( $config );
    $AppUI->checkStyle();

    require_once( $AppUI->getSystemClass( 'object' ) );
    require_once( "./includes/db_connect.php" );
    $m = getParam( $_GET, 'm', 'home' );
    include_once( "./modules/home/home.class.php" );
    $action = $_POST["action"];
    
    if( $_POST['action'] == 'save_NoteIt') {
        $luong_id  = $_POST['luong_id'];
        $field          = $_POST['field'];
        $val            = $_POST['val']; 
        if( ($val==' ') || ($val=='')  ) { 
            $sql = " UPDATE ns_nhanvien_bangluongchinh SET note_".$field." = '' WHERE luong_id = ".$luong_id;
            db_exec($sql);                
        } else {
            $sql = " UPDATE ns_nhanvien_bangluongchinh SET note_".$field." = '".$val."' WHERE luong_id = ".$luong_id;
            db_exec($sql);    
        }
        die;
    }
    
    $fMonth = $_POST['fMonth'] ? $_POST['fMonth'] : (  $_GET['fMonth'] ? $_GET['fMonth'] : date('m') );
    $fYear  = $_POST['fYear'] ? $_POST['fYear'] : (  $_GET['fYear'] ? $_GET['fYear'] : date('Y') );
    $sMonthYear =  $fMonth."-".$fYear ;
    $my_format  = 'mm-yyyy';
    
    
    
    if($_GET['action'] == 'danh_sach_phong_ban' ) {
        $danhmuc_arr = db_loadList("SELECT * FROM ns_danhmuc_phongban WHERE danhmuc_status = 1 ORDER BY danhmuc_name ASC");
        
            //            $list_perms_arr = db_loadHashList( "SELECT perms_name,perms_id FROM kiosk_user_permissions WHERE perms_type='PHONGBAN' AND user_id=".$AppUI->user_id );
            foreach ($danhmuc_arr as $k=>$row) :
                //if( $list_perms_arr[ $row['danhmuc_id'] ] > 0  ) { $danhsachphongban_arr[$k]  = $row; }
                $danhsachphongban_arr[$k]  = $row;
            endforeach;
        
        echo '{
                "total": "'.count($danhsachphongban_arr).'",
                "records": [
                ';  
                    $idx=0;
                    foreach ($danhsachphongban_arr as $row) :
                        $idx++;  
                        $mark = ' style: {';   $mark .="3 :' color:black;  '";  $mark .= ' }'; 
                        echo "
                        {    'recid': '".$idx."',"
                            . "'danhmuc_id': '".$row['danhmuc_id']."',"
                            . "'danhmuc_order': '".$idx."',"                                   
                            . "'danhmuc_name': '".$row['danhmuc_name']."', "
                            . ( ( $idx > 0 ) ?  "'check': '<input type=\"checkbox\" onclick=\"my_PhongBancheck(".$idx.",".$row['danhmuc_id'].")\" >'," : "" )    
                            .$mark."
                        },
                        ";
                    endforeach;
            echo '            
                ]
            }';            
        die;
    }
    
    if($_GET['action'] == 'danh_sach_luong_nv' ) {
        $danhmuc_id = $_GET['danhmuc_id']; 
        $sql = "SELECT nv_id,nv_name  FROM ns_nhanvien";
        $nv_nhanvien_arr = db_loadHashList($sql);
        
        //$kq_arr = db_loadList("SELECT * FROM ns_nhanvien WHERE danhmuc_id = ".$danhmuc_id." AND nv_status = 1 ORDER BY nv_name ASC");
        // Cái này không đúng - lỡ có nhân viên nghỉ việc - mà mình vẫn phải tính lương thì làm sao 
        //////////////////////////////////////////////////////////////////////////////////////////
        $fMonth = $_GET['fMonth']; 
        $fYear  = $_GET['fYear']; 
        $sql = "SELECT * FROM ns_nhanvien_bangluongchinh WHERE 1=1 AND fMonth ='".$fMonth."' AND fYear='".$fYear."'  AND danhmuc_id = ".$danhmuc_id." ORDER BY nv_name ASC";
        $kq_arr = db_loadList($sql);
        //////////////////////////////////////////////////////////////////////
        
        echo '{
                "total": "'.count($kq_arr).'",
                "records": [
                ';  
                    $idx=0;
                    foreach ($kq_arr as $row) :
                        $idx++;  
                        $mark = ' style: {';   $mark .="3 :' color:blue;',4 :' color:blue;' ";  $mark .= ' }'; 
                        echo "
                        {    'recid': '".$idx."',"
                            . "'nv_id': { id: '".$row['nv_id']."', text: '".$nv_nhanvien_arr[ $row['nv_id'] ]."' }, " 
                            . "'nv_name': '".$row['nv_name']."', "                                
                            . "'fMonth': '".$row['fMonth']."',"                                   
                            . "'fYear': '".$row['fYear']."',"                                                                   
                            . "'danhmuc_id': '".$row['danhmuc_id']."', "
                            . "'luong_co_dinh': '".$row['luong_co_dinh']."', "
                            . "'phu_cap_trach_nhiem': '".$row['phu_cap_trach_nhiem']."', "                                
                            . "'phu_cap_tien_xang': '".$row['phu_cap_tien_xang']."', "                                                            
                            . "'phu_cap_tien_com': '".$row['phu_cap_tien_com']."', "                                                                                        
                            . "'phu_cap_dien_thoai': '".$row['phu_cap_dien_thoai']."', "  
                            . "'phu_cap_cong_tac_phi': '".$row['phu_cap_cong_tac_phi']."', "                                  
                            . "'luong_id': '".$row['luong_id']."',"
                            . "'nv_order': '".$idx."',"                                
                            . "'d01': '".$row['d01']."',"
                            . "'d02': '".$row['d02']."',"
                            . "'d03': '".$row['d03']."',"
                            . "'d04': '".$row['d04']."',"
                            . "'d05': '".$row['d05']."',"
                            . "'d06': '".$row['d06']."',"
                            . "'d07': '".$row['d07']."',"
                            . "'d08': '".$row['d08']."',"
                            . "'d09': '".$row['d09']."',"
                            . "'d10': '".$row['d10']."',"
                            . "'d11': '".$row['d11']."',"
                            . "'d12': '".$row['d12']."',"
                            . "'d13': '".$row['d13']."',"
                            . "'d14': '".$row['d14']."',"
                            . "'d15': '".$row['d15']."',"
                            . "'d16': '".$row['d16']."',"
                            . "'d17': '".$row['d17']."',"
                            . "'d18': '".$row['d18']."',"
                            . "'d19': '".$row['d19']."',"
                            . "'d20': '".$row['d20']."',"
                            . "'d21': '".$row['d21']."',"
                            . "'d22': '".$row['d22']."',"
                            . "'d23': '".$row['d23']."',"
                            . "'d24': '".$row['d24']."',"
                            . "'d25': '".$row['d25']."',"
                            . "'d26': '".$row['d26']."',"
                            . "'d27': '".$row['d27']."',"
                            . "'d28': '".$row['d28']."',"
                            . "'d29': '".$row['d29']."',"
                            . "'d30': '".$row['d30']."',"
                            . "'d31': '".$row['d31']."',"                                
                            
                                
                             . "'note_d01': '".str_replace("'","",preg_replace('/\s+/', ' ', trim($row['note_d01']) ) ) ."'," 
                            . "'note_d02': '".str_replace("'","",preg_replace('/\s+/', ' ', trim($row['note_d02']) ) ) ."'," 
                            . "'note_d03': '".str_replace("'","",preg_replace('/\s+/', ' ', trim($row['note_d03']) ) ) ."'," 
                            . "'note_d04': '".str_replace("'","",preg_replace('/\s+/', ' ', trim($row['note_d04']) ) ) ."'," 
                            . "'note_d05': '".str_replace("'","",preg_replace('/\s+/', ' ', trim($row['note_d05']) ) ) ."'," 
                            . "'note_d06': '".str_replace("'","",preg_replace('/\s+/', ' ', trim($row['note_d06']) ) ) ."'," 
                            . "'note_d07': '".str_replace("'","",preg_replace('/\s+/', ' ', trim($row['note_d07']) ) ) ."'," 
                            . "'note_d08': '".str_replace("'","",preg_replace('/\s+/', ' ', trim($row['note_d08']) ) ) ."'," 
                            . "'note_d09': '".str_replace("'","",preg_replace('/\s+/', ' ', trim($row['note_d09']) ) ) ."',"   
                            . "'note_d10': '".str_replace("'","",preg_replace('/\s+/', ' ', trim($row['note_d10']) ) ) ."'," 
                            . "'note_d11': '".str_replace("'","",preg_replace('/\s+/', ' ', trim($row['note_d11']) ) ) ."'," 
                            . "'note_d12': '".str_replace("'","",preg_replace('/\s+/', ' ', trim($row['note_d12']) ) ) ."'," 
                            . "'note_d13': '".str_replace("'","",preg_replace('/\s+/', ' ', trim($row['note_d13']) ) ) ."'," 
                            . "'note_d14': '".str_replace("'","",preg_replace('/\s+/', ' ', trim($row['note_d14']) ) ) ."'," 
                            . "'note_d15': '".str_replace("'","",preg_replace('/\s+/', ' ', trim($row['note_d15']) ) ) ."'," 
                            . "'note_d16': '".str_replace("'","",preg_replace('/\s+/', ' ', trim($row['note_d16']) ) ) ."'," 
                            . "'note_d17': '".str_replace("'","",preg_replace('/\s+/', ' ', trim($row['note_d17']) ) ) ."'," 
                            . "'note_d18': '".str_replace("'","",preg_replace('/\s+/', ' ', trim($row['note_d18']) ) ) ."'," 
                            . "'note_d19': '".str_replace("'","",preg_replace('/\s+/', ' ', trim($row['note_d19']) ) ) ."'," 
                            . "'note_d20': '".str_replace("'","",preg_replace('/\s+/', ' ', trim($row['note_d20']) ) ) ."'," 
                            . "'note_d21': '".str_replace("'","",preg_replace('/\s+/', ' ', trim($row['note_d21']) ) ) ."'," 
                            . "'note_d22': '".str_replace("'","",preg_replace('/\s+/', ' ', trim($row['note_d22']) ) ) ."'," 
                            . "'note_d23': '".str_replace("'","",preg_replace('/\s+/', ' ', trim($row['note_d23']) ) ) ."'," 
                            . "'note_d24': '".str_replace("'","",preg_replace('/\s+/', ' ', trim($row['note_d24']) ) ) ."'," 
                            . "'note_d25': '".str_replace("'","",preg_replace('/\s+/', ' ', trim($row['note_d25']) ) ) ."'," 
                            . "'note_d26': '".str_replace("'","",preg_replace('/\s+/', ' ', trim($row['note_d26']) ) ) ."'," 
                            . "'note_d27': '".str_replace("'","",preg_replace('/\s+/', ' ', trim($row['note_d27']) ) ) ."'," 
                            . "'note_d28': '".str_replace("'","",preg_replace('/\s+/', ' ', trim($row['note_d28']) ) ) ."'," 
                            . "'note_d29': '".str_replace("'","",preg_replace('/\s+/', ' ', trim($row['note_d29']) ) ) ."'," 
                            . "'note_d30': '".str_replace("'","",preg_replace('/\s+/', ' ', trim($row['note_d30']) ) ) ."'," 
                            . "'note_d31': '".str_replace("'","",preg_replace('/\s+/', ' ', trim($row['note_d31']) ) ) ."'," 
                                 
                                
                            . "'tong_cong': '".$row['tong_cong']."',"                                
                            . "'check': '<input type=\"checkbox\" onclick=\"my_CHAMCONGcheck(".$idx.",".$row['luong_id'].")\" >',"                                                             
                            .$mark."
                        },
                        ";
                    endforeach;
            echo '            
                ]
            }';            
        die;
    }
    
    if($_POST['action'] == 'save_CHAMCONG' ) {
        $luong_id  = $_POST['luong_id'];
        $field          = $_POST['field'];
        $val            = $_POST['val']; 
        if( ($val==' ') || ($val=='')  ) { 
            $sql = " UPDATE ns_nhanvien_bangluongchinh SET ".$field." = '' WHERE luong_id = ".$luong_id;
            db_exec($sql);                
        } else {
            $sql = " UPDATE ns_nhanvien_bangluongchinh SET ".$field." = '".$val."' WHERE luong_id = ".$luong_id;
            db_exec($sql);    
        }
        
        $kq_arr = db_loadList("SELECT * FROM ns_nhanvien_bangluongchinh WHERE  luong_id = ".$luong_id);
        $total = 0; 
        for ($i = 1; $i <= 31; $i++) {   
            $my_ind = ($i < 10) ? '0'.$i : $i;  
            switch ( $kq_arr[0]['d'.$my_ind] ) {
                case '0.5':
                    $total = $total +0.5;  
                    break;
                case '1':
                    $total = $total +1; 
                    break;
                case 'L':
                    $total = $total + 1; 
                    break;
                case 'P':
                    $total = $total;
                    //$total = $total + 1; 
                    break;
                case '':
                    $total = $total; 
                    break;                            
                case 'O':
                    $total = $total; 
                    break;                    
                case '1.5':
                    $total = $total + 1.5;                     
                    break;                    
                case '2':
                    $total = $total + 2;                     
                    break;
                case '3':
                    $total = $total + 3;                     
                    break;
                
                default:
                    $total = $total;
            }
        };
        $sql = " UPDATE ns_nhanvien_bangluongchinh SET tong_cong = '".$total."' WHERE luong_id = ".$luong_id;
        db_exec($sql); 
        
        echo  $total;
        die;
    }
    
?> 
<head>
    <link rel="stylesheet" type="text/css" href="js/w2ui/w2ui-1.4.3.css" />
    <script src="js/w2ui/2.1.1.jquery.min.js"></script>
    <script type="text/javascript" src="js/w2ui/w2ui-1.4.3.js"></script>
    <link href="js/toast/toastr.css" rel="stylesheet" type="text/css" />
    <script src="js/toast/toastr.js"></script>  
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/buttons.css">
    <link rel="stylesheet" href="css/layout.css">
</head>
<script type="text/javascript">
//////////////////////////////////////////////
function addCommas(nStr)
{
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
} 

$(function () {
    $('#grid1').w2grid({ 
        name: 'grid1', 
        selectType: 'cell',
        url  : 'ns_qlychamcong.php?action=danh_sach_phong_ban',
        show: { 
            toolbar: false,
            footer: false,
            toolbarSave: false,
            selectColumn: false,
            lineNumbers    : false,
        },
        multiSelect: false,
        columns: [    
            { field: 'danhmuc_order', caption: '', size: '25px', sortable: false, resizable: false,style:'text-align: center;background-color:#CFCFCF;',   editable: { type: 'text' } }, 
            { field: 'danhmuc_name', info: true, caption:  '<div style="text-align: left;">TÊN VĂN PHÒNG</div>', size: '170px', sortable: true, resizable: true,style:'text-align: left;',    editable: { type: 'text' }             },
        ],
        onSelect: function (event) {
            var recid  = (event.recid);                 
            var record= w2ui.grid1.get(recid);        // id for select 
            $('#danhmuc_id').val(record.danhmuc_id );  
            w2ui['grid2'].load('ns_qlychamcong.php?action=danh_sach_luong_nv&fMonth=<?php echo $fMonth; ?>&fYear=<?php echo $fYear; ?>&danhmuc_id='+record.danhmuc_id);                    
        },
        records: [
        ]
    }); 
    
});

//////////////////////////////////////////////

function myExcelChangeCHAMCONG(field, val, luong_id) {
    $.ajax({
        type: "POST",
        url: "ns_qlychamcong.php",
        data: "action=save_CHAMCONG&luong_id="+luong_id+'&field='+field+'&val='+val ,
        success: function(msg){
            console.log(msg);
            
        }
    });  
}    

var nv_id_all_chamcong_arr =  [
    <?php 
        $nv_id_all_arr = db_loadHashList("SELECT nv_id,  nv_name   FROM ns_nhanvien n WHERE nv_status IN (1,3) ");
        foreach ($nv_id_all_arr as $k=>$row) :
            echo "  { id: '".$k."', text: '".$row ."' },";
        endforeach;
    ?>
]; 

<?php
    $daysOfWeek = array('Sunday'=>'CN','Monday'=>'T2','Tuesday'=>'T3','Wednesday'=>'T4','Thursday'=>'T5','Friday'=>'T6','Saturday'=>'T7');
    $tDay = date('t',strtotime("01-". $fMonth ."-".$fYear));

?>
var my_chamcong_arr = [
    "",
    "nv_id",
    <?php   for ($i = 1; $i <= $tDay; $i++) {   $my_ind = ($i < 10) ? '0'.$i : $i;  echo  '"d'.$my_ind.'",';   };  ?>
    "tong_cong",            
];

function writeIt(luong_id, day, val) {
    //alert(luong_id   ); 
    $.ajax({
        type: "POST",
        url: "ns_qlychamcong.php",
        data: "action=save_NoteIt&luong_id="+luong_id+'&field='+day+'&val='+val ,
        success: function(msg){
            toastr.success("Đã lưu");  
            //console.log(msg);
            setTimeout(function(){location.reload()}, 1000); 

        }
    });  
}


$(function () {
    $('#grid2').w2grid({ 
        name: 'grid2', 
        selectType: 'cell',
        url  : 'ns_qlychamcong.php?action=danh_sach_luong_nv&danhmuc_id=0',
        show: { 
            toolbar: false,
            footer: false,
            toolbarSave: false,
            selectColumn: false,
            lineNumbers    : false,
        },
        multiSelect: false,
        columns: [    
            { field: 'nv_order', caption: '', size: '25px', sortable: false, resizable: false,style:'text-align: center;background-color:#FFFFFF;',   editable: { type: 'text' } }, 
            { field: 'nv_id', info: true, caption:  '<div style="text-align: left;">TÊN NHÂN VIÊN</div>', size: '160px', sortable: true, resizable: true,style:'text-align: left;', 
                //editable: { type: 'list', items: nv_id_all_chamcong_arr, showAll: true },
                render: function (record, index, col_index) {
                    var html = this.getCellValue(index, col_index);
                    return html.text || '';
                }                          
            },
            <?php 
                for ($i = 1; $i <= $tDay; $i++) { 
                    $my_ind = ($i < 10) ? '0'.$i : $i;
                    $_datename = date('l',strtotime($my_ind."-". $fMonth ."-".$fYear));
                    if($daysOfWeek[$_datename] =='T7') { $_color ='  color:blue'; $_background = 'background:lightyellow; ';} 
                    else if($daysOfWeek[$_datename] =='CN') { $_color ='background:yellow; color:red'; $_background = 'background:lightgreen; '; }
                    else {  $_color ='color:black'; $_background ='';}                    
                    ;                    
            ?>           
                    { field: 'd<?php echo $my_ind; ?>', info: true, caption:  '<div style="text-align: center;<?php echo $_color; ?>"> <?php echo $my_ind; ?><br> <?php echo $daysOfWeek[$_datename]; ?></div>', size: '25px', sortable: true, resizable: true,style:'text-align: center;<?php echo $_background; ?>', 
                        <?php  if(  
                                ($fYear."-". $fMonth ."-".$my_ind == date('Y-m-d') ) 
                                ||  ( $fYear."-". $fMonth ."-".$my_ind == date('Y-m-d', strtotime("-1 days"))  ) 
                                ||  ( $fYear."-". $fMonth ."-".$my_ind == date('Y-m-d', strtotime("-2 days"))  ) 
                                ||  ( $fYear."-". $fMonth ."-".$my_ind == date('Y-m-d', strtotime("-3 days"))  ) 
                                ||  ( $fYear."-". $fMonth ."-".$my_ind == date('Y-m-d', strtotime("-4 days"))  )                                         
                                ||  ( $fYear."-". $fMonth ."-".$my_ind == date('Y-m-d', strtotime("-5 days"))  ) 
                                ||  ( $fYear."-". $fMonth ."-".$my_ind == date('Y-m-d', strtotime("-6 days"))  )                                                                                 
                                )  { ?>
                            editable: { type: 'text' },
                        <?php  } ?>
                            editable: { type: 'text' },
                            render: function (record,index, col_index) {
                                var recd = this.getCellValue(index, col_index);
                                var html = '<div style="padding: 5px; ">'+
                                    ' Chú thích:'+
                                    ' <textarea rows="2" style="background-color:lightblue;width:150px !important;" id="'+ record.luong_id +'_d<?php echo $my_ind; ?>" onchange="writeIt('+ record.luong_id +', \'d<?php echo $my_ind; ?>\', this.value);">' + record.note_d<?php echo $my_ind; ?> +'</textarea>'+
                                    '</div>';
                                if(recd != '') {     
                                    if(record.note_d<?php echo $my_ind; ?> != '') { 
                                        return  '<div onmouseover="$(this).w2overlay( w2utils.base64decode(\''+ w2utils.base64encode(html) +'\'), { left: -15 });" '+
                                               '  style="background-color:lightblue;"   >' + recd + '</div>';                               
                                    } else {
                                        return  '<div onmouseover="$(this).w2overlay( w2utils.base64decode(\''+ w2utils.base64encode(html) +'\'), { left: -15 });" '+
                                               '   onmouseout_nouse="$(this).w2overlay(\'\');"  >' + recd + '</div>';                                                                   
                                    }   
                               } else {
                                    return  '';                               
                               }
                            }
                    },
            <?php  
                } 
            ?> 
            { field: 'tong_cong', info: true, caption:  '<div style="text-align: center;">TỔNG CỘNG</div>', size: '80px', sortable: true, resizable: true,style:'text-align: left; color:blue; style:bold; font-size:15px', 
            },
        ],
        onChange: function (event) {
            var input_str  = (event.input_id);
            var arr = input_str.split('_'); // grid_grid_edit_1_4
            var record= w2ui.grid2.get(arr[3]);   
            var value_new = (event.value_new);          // new value
            var field = my_chamcong_arr[arr[4]];  
            
            if ( (value_new ==' ') || (value_new =='') || (value_new =='0') || (value_new =='0.5') ||  (value_new =='1') ||  (value_new =='1.5') || (value_new =='2') || (value_new =='3') || (value_new =='L') || (value_new =='O') || (value_new =='P')  ) {
                $.ajax({
                    type: "POST",
                    url: "ns_qlychamcong.php",
                    data: "action=save_CHAMCONG&luong_id="+record.luong_id+'&field='+field+'&val='+value_new ,
                    success: function(msg){
                        w2ui['grid2'].set(record.recid, { tong_cong: msg  });
                    }
                });
            } else {
                toastr.error('Vui lòng nhập đúng các ký tự : khoảng trắng, 0, 0.5, 1; 1.5; 2; 3 ; O, P, L ');  
                event.value_new = '1';
                value_new = 1;
            }
        },
        onSelect: function (event) {
            
            
        },
        records: [
        ]
    }); 
    
    
});

//////////////////////////////////////////////

</script>
<body style="margin-right:-10px; background-color: white; border: 0px;  "    >

<table style="width:100%">
    <tr>
        <td width="250px" style="border-right: thin double orange; vertical-align: top">
            <div id="grid1" style="width: 180px;height: 1000px;"></div>
        </td>
        <td width="100%" style="border-right: thin double orange; vertical-align: top">
            <u>BẢNG CHẤM CÔNG THÁNG(<b style="color:red"><?php echo $fMonth."/".$fYear; ?>)</b></u> 
            &nbsp;&nbsp;&nbsp;&nbsp; Ký hiệu : [<i>1: đi làm, P: nghỉ có phép, O: nghỉ không phép, L: nghỉ lễ, 0.5: làm 0,5 công, 1.5: làm việc 1,5 ngày công, 2: tăng ca được tính 2 ngày</i>]
            <input type="hidden" id="danhmuc_id" value="0">
            <div id="grid2" style="width: 1050px;height: 1000px;"></div>
        </td>  
    </tr>
</table>
</body>    

<script language="JavaScript">
    function get_excel() {
        window.open( "ns_qlychamcong.php?report=excel") ;
    } 
</script>